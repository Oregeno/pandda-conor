import os.path
import sys
import logging
import time
from itertools import izip
from string import ascii_uppercase
logger = logging.getLogger(__name__)

import numpy as np

from .builders import HierarchicalBuilder
from .structure import Ligand, Structure
from .volume import Volume
from .helpers import mkdir_p
from .validator import Validator
from .scaler import MapScaler


class Autobuild:
    
    def __init__(self,
                 resolution,
                 ligand,
                 receptor,
                 xmap=None,
                 selection=None,
                 no_scale=True,
                 density_cutoff=0.0,
                 no_build=True,
                 no_local=True,
                 build_stepsize=1,
                 stepsize=1,
                 cardinality=5,
                 threshold=None,
                 intermediate_threshold=0.01,
                 intermediate_cardinality=5,
                 directory='.',
                 debug=False,
                 verbose=False):
        
        self.resolution = resolution
        self.xmap = xmap
        self.receptor = receptor
        self.ligand = ligand

        self.selection = selection
        self.no_scale = no_scale
        self.density_cutoff = density_cutoff
        self.no_build = no_build
        self.no_local = no_local
        self.build_stepsize = build_stepsize
        self.stepsize = stepsize
        self.cardinality = cardinality
        self.threshold = threshold
        self.intermediate_threshold = intermediate_threshold
        self.intermediate_cardinality = intermediate_cardinality
        self.directory = directory
        self.debug = debug
        self.verbose = verbose

        if self.threshold is None:
            if self.resolution < 2.00:
                self.threshold = 0.2
            else:
                self.threshold = 0.3
        
    
    def run(self, xmap=None):
    
        time0 = time.time()

        if xmap is None:
            xmap = self.xmap

        ligand = self.ligand
        receptor = self.receptor.select('e', 'H', '!=')

        # Reset occupancies of ligand
        ligand.altloc.fill('')
        ligand.q.fill(1)
    
        if not self.no_scale:
            scaler = MapScaler(xmap, mask_radius=1, cutoff=self.density_cutoff)
            scaler(receptor.select('record', 'ATOM'))
    
        builder = HierarchicalBuilder(
                ligand, xmap, self.resolution, receptor=receptor,
                build=(not self.no_build), build_stepsize=self.build_stepsize,
                stepsize=self.stepsize, local_search=(not self.no_local),
                cardinality=self.intermediate_cardinality,
                threshold=self.intermediate_threshold,
                directory=self.directory, debug=self.debug
        )
        builder()
        fnames = builder.write_results(base='conformer', cutoff=0)
    
        conformers = builder.get_conformers()
        nconformers = len(conformers)
        if nconformers == 0:
            raise RuntimeError("No conformers were generated or selected. Check whether initial configuration of ligand is severely clashing.")
    
        validator = Validator(xmap, self.resolution)
        # Order conformers based on rscc
        for fname, conformer in izip(fnames, conformers):
            conformer.rscc = validator.rscc(conformer, rmask=1.5)
            conformer.fname = fname
        conformers_sorted = sorted(conformers, key=lambda conformer: conformer.rscc, reverse=True)

        # Remove conformers with significantly lower rscc
        best_rscc = conformers_sorted[0].rscc
        rscc_cutoff = 0.9 * best_rscc
        conformers = [conformer for conformer in conformers_sorted if conformer.rscc >= rscc_cutoff]

        # iteration = 1
        # while True:
        #     # Use builder class to perform MIQP
        #     builder._coor_set = [conformer.coor for conformer in conformers]
        #     builder._convert()
        #     builder._MIQP(threshold=self.threshold, maxfits=self.cardinality)
        #
        #     # Check if adding a conformer increasing the cross-correlation
        #     # sufficiently through the Fisher z transform
        #     filtered_conformers = []
        #     for occ, conformer in izip(builder._occupancies, conformers):
        #         if occ > 0.0001:
        #             conformer.data['q'].fill(occ)
        #             filtered_conformers.append(conformer)
        #     conformers = filtered_conformers
        #     conformers[0].zscore = float('inf')
        #     multiconformer = conformers[0]
        #     multiconformer.data['altloc'].fill('A')
        #     nconformers = 1
        #     filtered_conformers = [conformers[0]]
        #     for conformer in conformers[1:]:
        #         conformer.data['altloc'].fill(ascii_uppercase[nconformers])
        #         new_multiconformer = multiconformer.combine(conformer)
        #         diff = validator.fisher_z_difference(
        #             multiconformer, new_multiconformer, rmask=1.5, simple=True
        #         )
        #         if diff < 0.1:
        #             continue
        #         multiconformer = new_multiconformer
        #         conformer.zscore = diff
        #         filtered_conformers.append(conformer)
        #         nconformers += 1
        #     if len(filtered_conformers) == len(conformers):
        #         conformers = filtered_conformers
        #         break
        #     conformers = filtered_conformers
        #     iteration += 1
        # if nconformers == 1:
        #     multiconformer.data['altloc'].fill('')

        m, s = divmod(time.time() - time0, 60)

        return conformers[0]  # only return best one
