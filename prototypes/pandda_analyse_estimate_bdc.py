import numpy as np
import logging
logger = logging.getLogger(__name__)

import tensorflow as tf
import edward as ed
from edward import models

from qfit_ligand.structure import Structure, Ligand
from qfit_lifand.volume import Volume
from qfit_ligand.transformer import Transformer
from qfit.builders import HierarchicalBuilder
from .validator import Validator

class BDCEstimator:

    def __init__(self):

        self.bdc_prior = None
        self.location_pior = None

        self.ligand_model = None
        self.ground_state_model = None

        self.mean_map = None
        self.bound_map = None

        self.event_info = None

        self.scattering = 'xray'

    def run(self, bdc_prior, ligand_path, ground_state_model_path, mean_map_path, bound_map_path):

        ###################################################
        # load data
        ###################################################

        self.bdc_prior = bdc_prior

        # Load maps
        self.ligand_model = Ligand.fromfile(ligand_path)
        self.ground_state_model = Structure.fromfile(ground_state_model_path).select('e', 'H', '!=')

        # Load models
        self.mean_map = Volume.fromfile(mean_map_path).fill_unit_cell()
        self.bound_map = Volume.fromfile(bound_map_path).fill_unit_cell()

        #######################################
        # Preprocess data
        #######################################

        # Mask Atoms: Select atoms close to event
        event_atoms = receptor[(receptor["x_coord"] - event_x) ^ 2
                               + (receptor["y_coord"] - event_y) ^ 2
                               + (receptor["z_coord"] - event_z) ^ 2 < 100]

        # Make bound state table
        event_waters = event_atoms[event_atoms["residue_name"] == "WAT"]
        bound_state_atoms = event_atoms - event_waters + ligand_atoms

        # Make ground state table
        ground_state_atoms = event_atoms

        # Variates
        density_ground = self.mean_map
        density_bound = self.bound_map

        # TODO CONOR: Make sure this selection makes sense
        # get coordinates of all waters
        ground_model_no_wat = self.ground_state_model.select('resn', 'WAT', '!=')
        ground_model_wat = self.ground_state_model.select('resn', 'WAT', '==')
        # get ids of ones near the event
        event_waters =
        # Select those and combine with empty model
        model_ground_event_waters = ground_model_no_wat.combine(event_waters)

        # Set up map
        model_map = Volume.zeros_like(self.xmap)
        model_map.set_spacegroup("P1")
        # transformer.mask(self.rmask)
        mask = model_map.array > 0
        model_map.array.fill(0)

        # Get densities

        transformer_density_ground_event_waters = Transformer(model_ground_event_waters,
                                                  model_map,
                                                  simple=True,
                                                  scattering=self.scattering)
        density_ground_event_waters = transformer_density_ground_event_waters.density()

        #
        transformer_density_ground_model_no_wat = Transformer(ground_model_no_wat,
                                                  model_map,
                                                  simple=True,
                                                  scattering=self.scattering)
        density_ground_model_no_wat = transformer_density_ground_model_no_wat.density()

        # ligand model density
        transformer_density_ligand_model = Transformer(self.ligand_model,
                                          model_map,
                                          simple=True,
                                          scattering=self.scattering)
        density_ligand_model = transformer_density_ligand_model.density()

        density_bound_model = self.generate_bound_density(model_map, density_ground_model_no_wat)

        # parameters
        omega = models.Normal(loc=self.bdc_prior,
                              scale=0.05)

        location =
        rotation = models.Uniform(low=[0.0, 0.0, 0.0],
                                  high=[np.pi, np.pi, np.pi])
        conformation =


        # Response
        y = models.Normal(loc=omega*self.generate_bound_density(model_map, density_ground_model_no_wat)
                              + (1-omega)*density_ground_event_waters,
                          scale=0.1)

        # Posteriors
        qomega = models.Normal(loc=tf.get_variable("qomega/loc", [1, 2]),
                               scale=tf.nn.softplus(tf.get_variable("qomega/scale", [1, 2])))

        qlocation = models.Normal(loc=tf.get_variable("qomega/loc", [1, 2]),
                                  scale=tf.nn.softplus(tf.get_variable("qomega/scale", [1, 2])))
        qrotation = models.Normal(loc=tf.get_variable("qomega/loc", [1, 2]),
                                  scale=tf.nn.softplus(tf.get_variable("qomega/scale", [1, 2])))
        qconformation = None

        #######################################
        # Model definition
        #######################################

        # Chain level parameters

        for chain_id in event_atoms["chain_id"].unique():
            location = None
            rotation = None

        lig_location = None
        lig_rotation = None

        # residue level parameters
        for residue_id in event_atoms["residue_number"].unique():
            location = None
            rotation = None

        # atom level parameters

        for atom in event_atoms[""]:
            location = None

        for atom in ligand:
            location = None

        # Define response
        y = models.Normal(loc=omega * self.generate_bound_density(model_map, density_ground_model_no_wat)
                              + (1 - omega) * density_ground_event_waters,
                          scale=0.1)

        ###################################################
        # Inference
        ###################################################
        # Define inference
        inference_bdc = ed.VariationalInference({omega: qomega},
                                                data={y:self.bound_map, location: qlocation, rotation: qrotation})
        inference_lig = ed.VariationalInference({location: qlocation, rotation: qrotation},
                                                data={y:self.bound_map, omega: qomega})


        # Sample
        # Start EM
        for _ in range(1000):
            infer_chain.update()
            infer_residue.update()
            infer_atoms.update()

        ###################################################
        # Post-processing
        ###################################################
        # Save results

        # Return results


    def generate_bound_density(self, model_map, density_ground_model_no_wat):

        transformer_density_ligand_model = Transformer(self.ligand_model,
                                                       model_map,
                                                       simple=True,
                                                       scattering=self.scattering)
        density_ligand_model = transformer_density_ligand_model.density()

        density_bound_model = density_ground_model_no_wat \
                              + density_ligand_model

        return density_bound_model



builder = HierarchicalBuilder(
            ligand, xmap, args.resolution, receptor=receptor,
            build=(not args.no_build), build_stepsize=args.build_stepsize,
            stepsize=args.stepsize, local_search=(not args.no_local),
            cardinality=args.intermediate_cardinality,
            threshold=args.intermediate_threshold,
            directory=args.directory, debug=args.debug
    )
    builder()
    fnames = builder.write_results(base='conformer', cutoff=0)

    conformers = builder.get_conformers()
    nconformers = len(conformers)
    if nconformers == 0:
        raise RuntimeError("No conformers were generated or selected. Check whether initial configuration of ligand is severely clashing.")

    validator = Validator(xmap, args.resolution)
    # Order conformers based on rscc
    for fname, conformer in izip(fnames, conformers):
        conformer.rscc = validator.rscc(conformer, rmask=1.5)
        conformer.fname = fname
    conformers_sorted = sorted(conformers, key=lambda conformer: conformer.rscc, reverse=True)
    logger.info("Number of conformers before RSCC filtering: {:d}".format(len(conformers)))
    logger.info("RSCC values:")
    for conformer in conformers_sorted:
        logger.info("{fname}: {rscc:.3f}".format(fname=conformer.fname, rscc=conformer.rscc))
    # Remove conformers with significantly lower rscc
    best_rscc = conformers_sorted[0].rscc
    rscc_cutoff = 0.9 * best_rscc
    conformers = [conformer for conformer in conformers_sorted if conformer.rscc >= rscc_cutoff]
    logger.info("Number of conformers after RSCC filtering: {:d}".format(len(conformers)))
















