import numpy as np
import logging
logger = logging.getLogger(__name__)

import tensorflow as tf
import edward as ed
from edward import models

from biopandas.pdb import PandasPdb


class sampler:

    def __init__(self):

        self.receptor_path
        ligand_path

        ground_map
        mean_map

        event_location


    def run(self):
        #######################################
        # Load data
        #######################################

        receptor = PandasPdb.read_pdb(self.receptor_path)
        ligand = PandasPdb.read_pdb(self.ligand_path)

        #######################################
        # Preprocess data
        #######################################



        # Mask Atoms: Select atoms close to event
        event_atoms = receptor[(receptor["x_coord"] - event_x)^2
                               + (receptor["y_coord"] - event_y)^2
                               + (receptor["z_coord"] - event_z)^2 < 100]

        # Make bound state table
        event_waters = event_atoms[event_atoms["residue_name"] == "WAT"]
        bound_state_atoms = event_atoms - event_waters + ligand_atoms

        # Make ground state table
        ground_state_atoms = event_atoms


        #######################################
        # Model definition
        #######################################

        # Chain level parameters

        for chain_id in event_atoms["chain_id"].unique():
            location = None
            rotation = None

        lig_location = None
        lig_rotation = None

        # residue level parameters
        for residue_id in event_atoms["residue_number"].unique():
            location = None
            rotation = None

        # atom level parameters

        for atom in event_atoms[""]:
            location = None

        for atom in ligand:
            location = None


        # Define response
        y = models.Normal(loc=omega*self.generate_bound_density(model_map, density_ground_model_no_wat)
                              + (1-omega)*density_ground_event_waters,
                          scale=0.1)

        #######################################
        # Sample
        #######################################

        for _ in range(1000):

            infer_chain.update()
            infer_residue.update()
            infer_atoms.update()

        #######################################
        # Output
        #######################################


        return 1