import os
import pandas as pd

import iotbx

import qfit_ligand
from qfit_ligand.structure import Ligand, Structure

import logger
logger = logging.getLogger(__name__)


class Dataset:

    def __init__(self, builder):

        self.event_table = None
        self.events = []
        self.ligand = None
        self.electron_density_map = None
        self.model_input = None
        self.model_hierarchy = None
        self.ligand_input = None
        self.ligand_hierarchy = None

        self.builder = builder

    # TODO CONOR: Check this is the right function
    def load_event_table(self, event_table_path):
        self.event_table = pd.from_csv(event_table_path)

    # TODO CONOR: Work out how these iotbx functions work
    def load_model(self, model_path):
        if self.builder == 'qfit':
            self.model = Structure.fromfile(self.model_input)

        else:
            pdb_input = iotbx.pdb.input(file_name=model_path)
            pdb_hierarchy = pdb_input.construct_hierarchy()

            self.model_input = pdb_input
            self.model_hierarchy = pdb_hierarchy

    def load_ligand(self, ligand_path):
        if self.builder == "qfit_internal" or self.builder == "qfit":
            self.generate_ligand_pdb()
            self.ligand = Ligand.fromfile(ligand_input)

        else:
            pdb_input = iotbx.pdb.input(file_name=ligand_path)
            pdb_hierarchy = pdb_input.construct_hierarchy()

            self.ligand_input = pdb_input
            self.ligand_hierarchy = pdb_hierarchy

    # TODO CONOR: Fix this so it actually works
    def get_event_paths(self):

        # for root, dirs, files in os.walk(self.directory):
        #     for file in files:
        #         if file.endswith('*BDC*.ccp4'):
        #             self.paths.append(file)

        for event in self.event_table.itertuples():
            self.event_paths.append(event.path)

    # TODO CONOR: needs actual statistics
    def get_events_summary_statistics(self):
        """Join summary statistics tables from datasets and do statistics on resulting table"""
        table_list = []
        for event in self.events:
            table_list.append(event.statistics)

        event_statistics = pd.DataFrame(table_list)

        return event_statistics

    # TODO CONOR: Make sure it's deleting everything big
    def unload_all(self):

        del self.events
        del self.electron_density_map

    def generate_ligand_pdb(self):
        mol = Chem.MolFromSmiles()
        mol = Chem.AddHs(mol)
        AllChem.EmbedMolecule(mol)
        # print(Chem.MolToPDB(mol), file=os.open('data/foo.mol', 'w+'))


    # TODO CONOR: incljude resolution information, include more scores, put scores into a dataframe
    def score_models(self):
        if self.builder == 'qfit':
            logger.debug("Already calculated rscc in qfit")

        else:
            validator = qfit_ligand.validator.Validator(xmap=self.event_map,
                                                        resolution=self.resolution)
            for model in self.models:
                rscc = validator.rscc(model)

    def get_model_summary_statistics(self, event, model):
        if self.builder == 'qfit':
            record = {"event": event,
                      "model": model,
                      "rscc": model.rscc}
            self.database.append(record)

    # TODO CONOR: OS.run an actual function?
    def build_models(self):

        if self.builder == "qfit":

            os.run("qfit_ligand {} {} {} {}".format(self.path_to_event_map,
                                                    self.event_map_resolution,
                                                    self.ligand_pdb,
                                                    self.receptor_pdb))



        elif self.builder == "qfit_internal":

            autobuilder = Autobuild(resolution=self.resolution,
                                    ligand=self.ligand,
                                    receptor=self.structure)

            conformers = Autobuild.run(self.event_map)

            for conformer in conformers:
                self.models.append(conformer)

    def localise_ligand_to_event(self, event):
