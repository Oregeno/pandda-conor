import iotbx

from rdkit import Chem
from rdkit.Chem import AllChem

import logging
logger = logging.getLogger(__name__)


class Model:

    def __init__(self, model):



        self.model_input = None
        self.model_hierarchy = None

        if self.builder == 'qfit'
            self.model = model

        else:
            self.model_path = model

    def load_model(self):
        if self.builder == 'qfit':
            logger.debug("already loaded")

        else:
            pdb_input = iotbx.pdb.input(file_name=self.model_path)
            pdb_hierarchy = pdb_input.construct_hierarchy()

            self.model_input = pdb_input
            self.model_hierarchy = pdb_hierarchy

    def unload_model(self):
        pass
