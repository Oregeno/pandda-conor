import time

import numpy as np

import iotbx as iob
import sparse as sp

######################## SETUP #################################

input_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_conor_rerun/processed_datasets" \
             + "./PDK2-x0508/PDK2-x0508-event_1_1-BDC_0.32_map.native.ccp4"

# Import map

test_map = iob.any_file(input_path)

# Strip array

flex_array = test_map.map_data()

# Numpy array

numpy_array = np.array(flex_array)

# Sparsify array

sparse_array = sp.COO(numpy_array)


############################### CCP4 #################################

# Save as ccp4

start_save_cpp4 = time.time()

iotbx.ccp4_map.write_ccp4_map(file_name="map.ccp4",
                              unit_cell=test_map.unit_cell(),
                              space_group=test_map.unit_cell_space_group(),
                              map_data=flex_array)

finish_save_cpp4 = time.time()

# Load as ccp4

start_load_cpp4 = time.time()

ccp4_map = iob.any_file("map.ccp4")

finish_load_cpp4 = time.time()

################################# SPARSE #########################################

# Save as sparse array

start_save_npz = time.time()

sp.save_npz("map.npz", sparse_array)

finish_save_npz = time.time()

# Load as sparse array

start_load_npz = time.time()

sp.load_npz("map.npz")

finish_load_npz = time.time()

############################### PRINT ##############################

# Print timings and array data

print("Sparse array save ", finish_save_npz - start_save_npz)
print("Sparse array load ", finish_load_npz - start_load_npz)
print("CCP4 array save, ", finish_save_cpp4 - start_save_cpp4)
print("CCP4 array load, ", finish_load_cpp4 - start_load_cpp4)