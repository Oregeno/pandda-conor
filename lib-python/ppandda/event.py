import os

import iotbx

import qfit_ligand

from autobuild import Autobuild

import logging
logger = logging.getLogger(__name__)


class Event:

    def __init__(self, path_to_event_map, builder):
        self.path_to_event_map = path_to_event_map
        self.models = []
        self.event_map = None
        self.resolution = None

        self.builder = builder

        self.database = None
        self.database_columns = ["event", "model_id"]

    def load_event_map(self):
        # self.event_map = iotbx.any_file(self.path_to_event_map)
        if self.builder == 'qfit':
            self.event_map = qfit_ligand.Volume.fromfile(self.path_to_event_map).fill_unit_cell()

    def unload_event_map(self):
        pass

    def get_model_summary_statistics(self):
        pass

