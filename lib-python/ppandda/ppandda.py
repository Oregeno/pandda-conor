import os
import pandas as pd
import sqlite3
import configargparse

from sqldatabase import SQLDatabase
from dataset import Dataset
from event import Event
from model import Model

import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger(__name__)


class PPanDDA:

    def __init__(self, directory, builder='qfit'):

        self.directory = directory

        database_path = self.directory + "processing/database/soakDBDataFile.sqlite.db"
        self.database = SQLDatabase(database_path)
        self.pandda_table = self.database["panddaTable"]
        self.main_table = self.database["mainTable"]

        self.datasets = []
        self.builder = builder

    def run(self):
        """Main PPanDDA run function"""

        selected_ds_mask = self.get_valid_dataset_names()

        self.datasets = [Dataset(rec.CrystalName, rec.Resolution) for rec in self.main_table[selected_ds_mask]]

        # Loop over datasets, getting the events and building models into them
        # Should parralelise here? Depends on builder...
        for dataset in self.datasets:

            dataset.load_ligand()
            dataset.load_model()
            dataset.load_event_table()

            dataset.get_event_paths()

            dataset.events = [Event(event_path) for event_path in dataset.event_paths]

            for event in dataset.events:

                event.load_event_map()

                models = dataset.build_models(event)

                event.models = [Model(model) for model in models]

                dataset.score_models(event)

                for model in event.models: dataset.get_model_summary_statistics(event, model)

            dataset.event_statistics = [event.statistics() for event in dataset.events]

        dataset_statistics = [dataset.statistics() for dataset in self.datasets]

        summary_statistics = self.summarise_statistics(dataset_statistics)

        return summary_statistics

    # TODO CONOR: update file ends with condition
    def get_dataset_paths(self):

        # for root, dirs, files in os.walk(self.directory):
        #     for file in files:
        #         if file.endswith('.txt'):
        #             self.paths.append(file)

        for dataset in self.database.itertuples():
            self.paths.append(dataset.path)

        logger.info("Got paths to datasets")

    # TODO CONOR: add more statistics
    def get_summary_statistics(self):
        """Join summary statistics tables from datasets and do statistics on resulting table"""
        table_list = []
        for dataset in self.datasets:
            table_list.append(dataset.statistics)

        dataset_statistics = pd.DataFrame(table_list)

        return dataset_statistics

    # TODO CONOR: change how reading is done
    def get_databases(self):
        database_path = self.directory + "processing/database/soakDBDataFile.sqlite.db"
        conn = sqlite3.connect(database_path)
        self.main_table = pd.read_sql_query("select * from mainTable;", conn)
        self.pandda_table = pd.read_sql_query("select * from panddaTable;", conn)
        logger.info("Got database at {}".format(database_path))




