import sciluigi as sl


##  Define workflow

class ppandda_workflow(sl.WorkflowTask):

    def workflow(self):
        # get an iterable of ppandda outputs from table

        conn = psycopg2.connect(host="172.23.142.43", port="5432", database="test_xchem", user="conor", password="c0n0r")
        cur = conn.cursor()
        row = cur.fetchone()

        # Dispatch qfit tasks over event maps
        qfit_tasks = []

        for event_record in iterable:

            task = self.new_task('qfit_{}'.format(event_record.ID), QFit)
            task.in_event_map = event_record.map_path
            qfit_tasks.append(task)

        # summarise

        statistics = self.new_task('statistics', Statistics)
        statistics.in_models = [task.out_model for task in qfit_tasks]
        statistics.in_maps = [event.map for event in iterable]

        return statistcs


class QFit(sl.Task):

    in_event_map = None

    def run(self):
        sl.ex("qfit {input_map}".format(
            input_map=in_event_map))

    def out_model(self):

        return sciluigi.TargetInfo(self, self.in_foo().path + '.bar.txt')


class Statistics(sl.Task):

    in_models = None
    in_maps = None

    def run(self):


    def out_statistics(self):
        return sciluigi.TargetInfo(self, self.in_foo().path + '.bar.txt')



if __name__ == "__main__":
    sl.run(ppandda_workflow)

