import sys
import time

from iotbx.file_reader import any_file
from cctbx import maptbx

print("Getting mtz file")

start_read = time.time()

f = any_file("/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/PDK2-x0128/PDK2-x0128-pandda-input.mtz",
             force_type="hkl")

finsh_read = time.time()

print("File got in {}".format(finsh_read-start_read))
print(type(f))
print(dir(f))
print(f.file_info())
print(f.show_summary())

start_serve = time.time()

miller = f.file_server.miller_arrays

finish_serve = time.time()

print("File served in {}".format(finish_serve - start_serve))
print(type(miller))
print(dir(miller))
print(miller)
# print(dir(miller[0]))
# print(type(miller[0].as_mtz_dataset()))
# print(dir(miller[0].as_mtz_dataset()))
print(miller[5].show_comprehensive_summary())
start_build = time.time()
print(type(miller[5].fft_map(symmetry_flags=maptbx.use_space_group_symmetry)))
finish_build = time.time()
print("Built map from mtz in {}".format(finish_build-start_build))
print("Size of miller array is {}".format(sys.getsizeof(miller)))



print("getting ccp4 file")

start_read = time.time()

f = any_file(
    "/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/PDK2-x0128/PDK2-x0128-z_map.native.ccp4",
    force_type="ccp4_map")


finsh_read = time.time()

print("Read ccp4 in {}".format(finsh_read - start_read))

start_build = time.time()

ccp4_map = f.file_object

finish_build = time.time()

print("CCP4 built in {}".format(finish_build-start_build))

print("Size of ccp4 map is {}".format(sys.getsizeof(ccp4_map.map_data())))