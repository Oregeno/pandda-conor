import argparse
import os
from shutil import rmtree
import re
from subprocess import PIPE, Popen
import time

import numpy as np
from scipy.optimize import basinhopping, brute, differential_evolution
import pandas as pd
import sqlalchemy

from biopandas.pdb import PandasPdb

import autobuild

from qfit_ligand.validator import Validator
from qfit_ligand.volume import Volume
from qfit_ligand.structure import Structure



# Panddas optoins

pd.options.display.max_columns = 20

pd.options.display.max_colwidth = 50


def parse_args():
    parser = argparse.ArgumentParser()

    # PanDDA run
    parser.add_argument("-r", "--pandda_run", default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Output
    parser.add_argument("-o", "--output", default="harness_test/")

    # Database
    parser.add_argument("-hs", "--host", default="172.23.142.43")
    parser.add_argument("-p", "--port", default="5432")
    parser.add_argument("-d", "--database", default="test_xchem")
    parser.add_argument("-u", "--user", default="conor")
    parser.add_argument("-pw", "--password", default="c0n0r")


    # Interpeter args
    parser.add_argument("-cpy", "--ccp4_python", default="/home/zoh22914/myccp4python")

    # Operation
    parser.add_argument("-b", "--build", default=0)


    args = parser.parse_args()

    return args

def bar():
    print("##############################################################################################")

def get_databases(args):

    engine=sqlalchemy.create_engine("postgresql://{}:{}@{}:{}/{}".format(args.user, args.password, args.host, args.port, args.database))

    pandda_analysis = pd.read_sql_query("SELECT * FROM pandda_analysis", con=engine)

    pandda_run = pd.read_sql_query("SELECT * FROM pandda_run", con=engine)

    pandda_events = pd.read_sql_query("SELECT * FROM pandda_event", con=engine)

    pandda_events_stats = pd.read_sql_query("SELECT * FROM pandda_event_stats", con=engine)

    statistical_maps = pd.read_sql_query("SELECT * FROM pandda_statistical_map", con=engine)

    crystals = pd.read_sql_query("SELECT * FROM crystal", con=engine)

    ligands = pd.read_sql_query("SELECT * FROM proasis_hits", con=engine)

    ligand_stats = pd.read_sql_query("SELECT * FROM ligand_edstats", con=engine)

    data_processing = pd.read_sql_query("SELECT * FROM data_processing", con=engine)

    compounds = pd.read_sql_query("SELECT * FROM compounds", con=engine)

    databases = {"pandda_analyses": pandda_analysis,
                 "pandda_runs": pandda_run ,
                 "pandda_events": pandda_events,
                 "pandda_event_stats": pandda_events_stats,
                 "statistical_maps": statistical_maps,
                 "crystals": crystals,
                 "ligands": ligands,
                 "ligand_stats": ligand_stats,
                 "data_processing": data_processing,
                 "compounds": compounds}

    return databases


if __name__ == "__main__":

    bar()

    # get args
    print("Parsing arguments...")
    args = parse_args()

    # Constants
    print("Defining constants...")
    home = "/home/zoh22914/"
    base_python = "python"
    ccp4_python = home + "myccp4python"
    np2ccp4 = home + "pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py"
    ccp42np = home + "pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py"

    # Set up output dir
    print("Establishing output directory...")
    try:
        os.mkdir(args.output)
    except:
        rmtree(args.output)
        os.mkdir(args.output)

    # get_databases
    print("Setting up databases...")
    databases = get_databases(args)

    # Get run id
    print("Getting analysis id from directory {}...".format(args.pandda_run))
    analysis_id = databases["pandda_analyses"][databases["pandda_analyses"]["pandda_dir"] == args.pandda_run]["id"].iloc[0]
    print("Analysis id is: {}".format(analysis_id))

    # Get run id
    print("Getting run id from directory {}...".format(args.pandda_run))
    run_id = databases["pandda_runs"][databases["pandda_runs"]["pandda_analysis_id"] == analysis_id]["id"].iloc[0]
    print("Run id is: {}".format(run_id))

    # get pandda anlyses table
    print("Getting PanDDA analyses...")
    pandda_analysis_table = pd.read_csv(args.pandda_run + "/analyses/pandda_analyse_events.csv")
    # print("Analysis table colunn is \n{}".format(pandda_analysis_table.iloc[0]))

    # Get ids of events for selected dataset
    print("Getting event ids...")
    event_ids = databases["pandda_events"][databases["pandda_events"]["pandda_run_id"] == run_id]["id"]
    print("Number of events is: {}".format(len(event_ids)))

    # Loop over events in selected dataset
    for index, event_id in event_ids.iteritems():

        bar()

        print("Setting up input data for building event: {}".format(event_id))

        # Get associated crystal
        print("Collecting foriegn keys...")
        crystal_id = databases["pandda_events"][databases["pandda_events"]["id"]==event_id]["crystal_id"].iloc[0]
        event_num = databases["pandda_events"][databases["pandda_events"]["id"]==event_id]["event"].iloc[0]
        crystal_name = databases["crystals"][databases["crystals"]["id"] == crystal_id]["crystal_name"].iloc[0]
        print("\tcrystal id is: {}".format(crystal_id))
        print("\tevent number is: {}".format(event_num))
        print("\tcrystal name is: {}".format(crystal_name))


        # Define output
        print("Defineing output")
        output = args.output + databases["crystals"][databases["crystals"]["id"]==crystal_id]["crystal_name"].iloc[0] + "/"
        print("\tOutput path is : {}".format(output))

        # Make output directory
        os.mkdir(output)


        # Get mtz for transforming
        print("Getting mtz path...")
        mtz_path = databases["pandda_events"][databases["pandda_events"]["id"] == event_id]["pandda_input_mtz"].iloc[0]
        print("\tMTZ path is: {}".format(mtz_path))

        # prepare 2MFo-DFc maps
        print("FFTing 2mFo-DFc maps...")
        os.popen("phenix.mtz2map {} ".format(mtz_path)
                 + "directory={} ".format(output))

        # Get template map path
        target_map_path = output + os.path.splitext(os.path.basename(mtz_path))[0] + "_2mFo-DFc.ccp4"
        statistical_map_path = args.pandda_run + "/processed_datasets/" + crystal_name + "/" + crystal_name + "-ground-state-mean-map.native.ccp4"


        # get numpy maps
        target_map_path = output + os.path.splitext(os.path.basename(mtz_path))[0] + "_2mFo-DFc.ccp4"
        print("Loading target map from: {}".format(target_map_path))
        numpy_map = autobuild.load_map(target_map_path, output+"target_map.npy", ccp4_python, ccp42np)
        print("Loading mean map from: {}".format(statistical_map_path))
        numpy_mean_map = autobuild.load_map(statistical_map_path, output + "mean_map.npy", ccp4_python, ccp42np)

        # Get analysis record
        print("Loading analysis record from table...")
        analysis_record = pandda_analysis_table[(pandda_analysis_table["dtag"]==crystal_name)
                                         & (pandda_analysis_table["event_idx"]==event_num)]

        # get centroid
        print("Getting centroid...")
        centroid = analysis_record.as_matrix(columns=["x", "y", "z"])
        print("\tEvent centroid is: {}".format(centroid))

        # Load xmap
        print("Loading volume xmap from {}...".format(statistical_map_path))
        volume_xmap = Volume.fromfile(str(statistical_map_path))
        print("\tVolume xmap shape is: {}".format(volume_xmap.array.shape))

        # get bdc
        print("Getting bdc...")
        databases["pandda_event_stats"]["event_id_int"] = databases["pandda_event_stats"]["event_id"].astype("int64")
        # bdc = databases["pandda_event_stats"][databases["pandda_event_stats"]["event_id_int"] == event_id]["one_minus_bdc"].iloc[0]
        bdc = analysis_record["1-BDC"].iloc[0]
        print("\tBDC is : {}".format(bdc))

        # build bdc map
        print("Making bdc map...")
        bdc_map = autobuild.generate_bdc_map(bdc,
                                             numpy_map,
                                             numpy_mean_map)

        # Get ligand smiles
        print("Getting ligand sdf...")
        print(databases["ligands"]["crystal_name_id"])
        compound_id = databases["crystals"][databases["crystals"]["id"] == crystal_id]["compound_id"].iloc[0]
        ligand_smiles = databases["compounds"][databases["compounds"]["id"] == compound_id]["smiles"].iloc[0]
        print("\tLigand smiles is: {}".format(ligand_smiles))

        # Get receptor
        print("Getting receptor path...")
        receptor = databases["pandda_events"][databases["pandda_events"]["id"] == event_id]["pandda_input_pdb"].iloc[0]
        print("\tReceptor path is: {}".format(receptor))

        exit()


        # Build!

        print("Building!")

        if args.build == 1:

            models = autobuild.build(output=output,
                                     centroid=centroid,
                                     bdc_map=bdc_map,
                                     ligand_smiles=ligand_smiles,
                                     receptor=receptor,
                                     volume_xmap=volume_xmap,
                                     template_map_path=target_map_path)


        bar()


    # # Refine initial models
    #
    # for event in events:
    #
    #     # Filter to within 20% of best
    #
    #     best = 1
    #
    #     for model in best:
    #
    #         # Set up refinemnet
    #         os.Popen("phenix.ready_set {ENSEMBLE_PDB}",
    #                  shell=True)
    #
    #         # Refine
    #         os.Popen("~/myccp4python "
    #                  + "~/pandda-conor/lib-python/giant/jiffies/quick_refine.py "
    #                  + "input.pdb={ENSEMBLE_PDB} "
    #                  + "input.mtz={MTZ} "
    #                  + "input.cif={ENSEMBLE_CIF} "
    #                  + "input.params={REFMAC_PARAMS} "
    #                  + "options.program={program}".format(),
    #                  shell=True)
    #

