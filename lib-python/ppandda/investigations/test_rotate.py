import numpy as np


def rotate(sample_by_feature, x, y, z):

    mean_x = np.mean(sample_by_feature[:, 0])
    mean_y = np.mean(sample_by_feature[:, 1])
    mean_z = np.mean(sample_by_feature[:, 2])

    de_meaned_x = sample_by_feature[:, 0] - mean_x
    de_meaned_y = sample_by_feature[:, 1] - mean_y
    de_meaned_z = sample_by_feature[:, 2] - mean_z

    sample_by_feature[:, 0] = de_meaned_x
    sample_by_feature[:, 1] = de_meaned_y
    sample_by_feature[:, 2] = de_meaned_z

    sx = np.sin(x)
    cx = np.cos(x)
    sy = np.sin(y)
    cy = np.cos(y)
    sz = np.sin(z)
    cz = np.cos(z)

    rx = [[1, 0, 0],
           [0, cx, sx],
           [0, -sx, cx]]
    ry = [[cy, 0, -sy],
           [0, 1, 0],
           [sy, 0, cy]]
    rz = [[cz, sz, 0],
           [-sz, cz, 0],
           [0, 0, 1]]

    rotated_x = np.matmul(rx, sample_by_feature.T)
    rotated_y = np.matmul(ry, rotated_x)
    rotated_z = np.matmul(rz, rotated_y)

    rotated_matrix = rotated_z.T

    rotated_matrix[:, 0] = rotated_matrix[:, 0] + mean_x
    rotated_matrix[:, 1] = rotated_matrix[:, 1] + mean_y
    rotated_matrix[:, 2] = rotated_matrix[:, 2] + mean_z


    return rotated_matrix



unit_x = np.array([[1, 0, 0],
                   [-1, 0, 0]])

unit_y = np.array([[0, 1, 0],
                   [0, -1, 0]])

unit_z = np.array([[0, 0, 1],
                   [0, 0, -1]])

print("x rotations")
print(rotate(unit_x, 0, 0, np.pi))
print(rotate(unit_x, 0, np.pi, 0))
print(rotate(unit_x, np.pi, 0, 0))


print("y rotations")
print(rotate(unit_y, 0, 0, np.pi))
print(rotate(unit_y, 0, np.pi, 0))
print(rotate(unit_y, np.pi, 0, 0))

print("Z rotations")
print(rotate(unit_z, 0, 0, np.pi))
print(rotate(unit_z, 0, np.pi, 0))
print(rotate(unit_z, np.pi, 0, 0))
