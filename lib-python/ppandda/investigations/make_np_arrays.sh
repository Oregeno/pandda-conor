

for dataset in *; do

    echo "$dataset"

    cd $dataset

    for map in *input.mtz; do
        echo "$map"
        phenix.mtz2map mtz_file="$map" d_min=3 grid_resolution_factor=0.166666666666
        ~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py -m ""$dataset"-pandda-input_2mFo-DFc.ccp4"
        cd -
    done

done