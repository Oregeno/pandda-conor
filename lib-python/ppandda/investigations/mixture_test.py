from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from sklearn.mixture import BayesianGaussianMixture

import argparse
import time
import numpy as np

import plots

import ccp4_io_utils as io




def parse_args():
    parser = argparse.ArgumentParser()

    # Input
    parser.add_argument("-d", "--map_directory_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Settings
    parser.add_argument("-n", "--num_components",
                        default=2)

    # Output

    args = parser.parse_args()

    return args



if __name__ == "__main__":

    args = parse_args()

    datasets = io.load_datasets_parralel(args.map_directory_path,
                                file_class="-pandda-input.mtz",
                                max_datasets=100)

    print("Genmerating map list...")
    maps = [datasets[crystal]["map"] for crystal in datasets]
    print("Length of maps list is {}".format(len(maps)))
    print(maps[0].shape)


    print("Generating sample by feature array...")
    sample_by_components = np.vstack(maps)
    # print("Sample by feature array is of shape {}".format(sample_by_components.shape))
    # print("Performing pca...")
    # pca_start = time.time()
    # pca = PCA(n_components=int(args.num_components), whiten=True)
    # components = pca.fit_transform(sample_by_components)
    # pca_finish = time.time()
    # print("\tCompleted PCA in {}".format(pca_finish-pca_start))

    plots.save_scatter(sample_by_components[:,0], sample_by_components[:,0], "/home/zoh22914/test_figures/dist.png")


    print("Mixture modelling with 1 component...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = GaussianMixture(n_components=1, covariance_type="diag")
        mixture_model.fit(sample_by_components[:,i].reshape(-1,1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish-mixture_start))

    print("Mixture modelling with 2 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = GaussianMixture(n_components=2, covariance_type="diag")
        mixture_model.fit(sample_by_components[:,i].reshape(-1,1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish-mixture_start))

    print("Mixture modelling with 3 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = GaussianMixture(n_components=3, covariance_type="diag")
        mixture_model.fit(sample_by_components[:,i].reshape(-1,1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish-mixture_start))

    print("Variational Mixture modelling with 1 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = BayesianGaussianMixture(n_components=1, covariance_type="diag")
        labels = mixture_model.fit_predict(sample_by_components[:, i].reshape(-1, 1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish - mixture_start))




    print("Variational Mixture modelling with 2 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = BayesianGaussianMixture(n_components=2, covariance_type="diag")
        labels = mixture_model.fit_predict(sample_by_components[:, i].reshape(-1, 1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish - mixture_start))
        print("\tWeights are {}".format(mixture_model.weights_))

        if i==0:
            plots.save_coloured_scatter(sample_by_components[:, i], sample_by_components[:, i], labels,
                                    "/home/zoh22914/test_figures/mixture_components.png")


    print("Variational Mixture modelling with 3 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = BayesianGaussianMixture(n_components=3, covariance_type="diag")
        mixture_model.fit(sample_by_components[:, i].reshape(-1, 1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish - mixture_start))
        print("\tWeights are {}".format(mixture_model.weights_))


    print("Variational Mixture modelling with 4 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = BayesianGaussianMixture(n_components=4, covariance_type="diag")
        mixture_model.fit(sample_by_components[:, i].reshape(-1, 1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish - mixture_start))
        print("\tWeights are {}".format(mixture_model.weights_))


    print("Variational Mixture modelling with 5 components...")
    for i in range(10):
        mixture_start = time.time()
        mixture_model = BayesianGaussianMixture(n_components=5, covariance_type="diag")
        mixture_model.fit(sample_by_components[:, i].reshape(-1, 1))
        mixture_finish = time.time()
        print("\tEstimated mixture components in {}".format(mixture_finish - mixture_start))
        print("\tWeights are {}".format(mixture_model.weights_))

    #
    #
    # # Plot
    # combinations = list(itertools.combinations(range(int(args.num_components)), 2))
    # num_combinations = len(combinations)
    #
    # fig, ax = plt.subplots(1, num_combinations)
    # fig.set_size_inches(45, 15)
    #
    # for j, c in enumerate(combinations):
    #
    #     ax[j].scatter(components[:, c[0]], components[:, c[1]])
    #     print(components.shape)
    #
    #     for i, txt in enumerate(datasets):
    #         ax[j].annotate(txt[0], (components[i, c[0]], components[i, c[1]]))
    #
    #     ax[j].set_xlabel(c)
    #
    # fig.savefig("components_{}_x0122.png".format(args.num_components))
    #
    # for i in range(int(args.num_components)):
    #
    #     print(dir(pca))
    #
    #     component = pca.components_[i]
    #
    #     # array = np.zeros(int(args.num_components))
    #     #
    #     # array[i] = 1000
    #     #
    #     # print(array)
    #     #
    #     # component = pca.inverse_transform(array.reshape(1, -1))
    #     #
    #     # print(component.shape)
    #     #
    #     # print("20th component is", component[20])
    #
    #     component_reshaped = component.reshape((225, 225, 180))
    #
    #     print("saving to ica_{}.npy".format(i))
    #
    #     np.save("ica_{}.npy".format(i), component_reshaped)
    #
    #     os.popen("~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py -a ica_{}.npy -m processed_datasets/PDK2-x0001/PDK2-x0001-pandda-input_2mFo-DFc.ccp4".format(i))
    #
    #
    # for i in range(int(args.num_components)):
    #
    #     print(dir(pca))
    #
    #     component = pca.components_[i]
    #
    #     # array = np.zeros(int(args.num_components))
    #     #
    #     # array[i] = 1000
    #     #
    #     # print(array)
    #     #
    #     # component = pca.inverse_transform(array)
    #
    #     print("20th component is", component[20])
    #
    #     component_reshaped = component.reshape((225, 225, 180))
    #
    #     print("saving to ica_neg{}.npy".format(i))
    #
    #     np.save("ica_neg_{}.npy".format(i), -component_reshaped)
    #
    #     os.popen("~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py -a ica_neg_{}.npy -m processed_datasets/PDK2-x0001/PDK2-x0001-pandda-input_2mFo-DFc.ccp4".format(i))
    #
    #
    #
    #
