import argparse
import numpy as np

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import statsmodels.stats.diagnostic as stmdls


def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("-mmp", "--mean_map_path")
    parser.add_argument("-emp", "--event_map_path")
    parser.add_argument("-tmp", "--target_map_path")
    parser.add_argument("-n", "--num")

    args = parser.parse_args()

    return args


def get_stats(array, axs, i):

    print("#####################(100, 100, 100)", array[100, 100, 100])

    flattented_array = array.flatten()

    print("Size before truncation", flattented_array.shape)

    non_zero_array = flattented_array[np.abs(flattented_array) > 0.001]

    #non_zero_array_unscaled = flattented_array
    # non_zero_array = (non_zero_array_unscaled*np.std(flattented_array))/np.std(non_zero_array_unscaled)
    #non_zero_array = flattented_array



    axs[i].hist(non_zero_array, bins=50)

    print("Sample values after truncation", non_zero_array[0:10])
    print("Size after truncation", non_zero_array.shape)

    print("Mean", np.mean(non_zero_array))
    print("Deviation", np.std(non_zero_array))

    lil_ks_statistic, lil_pval = stmdls.lilliefors(non_zero_array)

    print("raw pval", lil_pval)

    return lil_pval



if __name__ == "__main__":

    # Parse args
    args = parse_args()

    # load maps
    mean_map = np.load(args.mean_map_path)
    event_map = np.load(args.event_map_path)
    target_map = np.load(args.target_map_path)
    print("loaded maps")

    # Generate statistics
    print("Generating Statistics")

    fig, axs = plt.subplots(1, int(args.num)+3, sharey=True, tight_layout=True)

    fig.set_size_inches(45, 15)

    mean_map_stats = get_stats(mean_map, axs, 0)
    event_map_stats = get_stats(event_map, axs, 1)
    target_map_stats = get_stats(target_map, axs, 2)

    bdc_maps = [[b, (target_map - b*mean_map)] for b in np.linspace(0, 1, args.num)]

    for bdc_map in bdc_maps:
        np.save("{}.npy".format(bdc_map[0]), bdc_map[1])

    bdc_map_stats = [[bdc_map[0], get_stats(bdc_map[1], axs, 3+i)] for i, bdc_map in enumerate(bdc_maps)]

    plt.savefig("hist.png")

    # print stats
    print("Mean map Lillifors value: {}".format(mean_map_stats))
    print("Event map Lillifors value: {}".format(event_map_stats))
    print("Target map Lillifors value: {}".format(target_map_stats))

    for bdc_map_stat in bdc_map_stats:
        print("BDC: {}, Lillifors value: {}".format(bdc_map_stat[0], bdc_map_stat[1]))




