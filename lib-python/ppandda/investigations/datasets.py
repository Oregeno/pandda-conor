import argparse
import time

import numpy as np

import pandas as pd

import itertools
import array_split

import pathlib as p
from scipy.stats import mode

import subprocess

import os



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d",
                        "--directory",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")
    parser.add_argument("-o",
                        "--output",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/np_maps")

    args = parser.parse_args()

    return args


class Dataset:

    def __init__(self, directory, num_samples, column):

        self.directory = directory
        self.num_samples = num_samples
        self.column = column
        self.shape = None


        self.df = 1

        self.get_paths()
        if num_samples != "all":
            self.df = self.df.head(num_samples)

    def get_crystal_paths(self):

        print("Finding datasets...")

        processed_datasets = p.Path(self.directory) / "processed_datasets"

        crystals = processed_datasets.glob("*")

        datasets = [{"crystal": crystal.name,
                     "path": crystal}
                    for crystal in crystals if crystal.is_dir()]

        df = pd.DataFrame(datasets)

        self.df = df

        print("\t found {} datasets".format(len(datasets)))

        return self.df

    def generate_np_maps_from_ccp4s(self, map_name="-pandda-input_2mFo-DFc.ccp4", overwrite=False):

        print("Generating numpy maps...")

        for crystal_path in self.df["path"]:

            map_path = crystal_path / crystal_path.name + map_name
            array_path = crystal_path / crystal_path.name + map_name + ".npy"
            print("\tHandling map {}...".format(map_path))

            if os.exists(map_path):
                print("\t\tSkipping map, already exists...")
                pass
            else:
                print("\t\tGenerating numpy array from map...")
                gen_map_start = time.time()
                p = subprocess.Popen("ccp_python ccp4_to_np -m {} -o {} -v 1".format(map_path,
                                                                                     array_path),
                                     shell=True)
                gen_map_finish = time.time()
                print("Generated map in {}".format(gen_map_finish - gen_map_start))

    def load_np_maps(self, keep_mode=True):




    def get_paths(self):

        print("Finding datasets...")

        processed_datasets = p.Path(self.directory) / "processed_datasets"

        crystals = processed_datasets.glob("*")

        datasets = [{"crystal": crystal.name,
                     "pdb_path": processed_datasets / crystal.name / "{}-pandda-input.pdb".format(crystal.name),
                     "mtz_path": processed_datasets / crystal.name / "{}-pandda-input.mtz".format(crystal.name)}
                    for crystal in crystals if crystal.is_dir()]

        df = pd.DataFrame(datasets)

        self.df = self.df.append(df, ignore_index=True, sort=True)

        print("\t found {} datasets".format(len(datasets)))

        return self.df

    def load_and_transform_mtzs(self, column="FWT"):
        print("Loading datasets")

        loader = lambda record: load_transform(record, column)

        load_start = time.time()

        maps = easy_mp.pool_map(iterable=self.df["mtz_path"],
                                fixed_func=loader,
                                chunksize=10)

        load_finish = time.time()

        print("\tLoaded datasets in {}".format(load_finish - load_start))

        print(maps[0])

        datasets = {record[0]: record[1] for record in maps}
        for triple in maps:
            datasets[triple[0]]["map"] = triple[2]

        modal_shape = mode([datasets[crystal]["map"].shape for crystal in datasets]).mode
        print("\tThe modal shape is {}".format(modal_shape))

        datasets = {crystal: {"map": datasets[crystal]["map"]} for crystal in datasets if
                    datasets[crystal]["map"].shape == modal_shape}
        print("\tAfter filtering {} maps are left".format(len(datasets)))


    def load_mtzs(self, column="FWT"):
        print("Loading datasets")

        loader = lambda record: load_transform(record, column)

        load_start = time.time()

        maps = easy_mp.pool_map(iterable=self.df["mtz_path"],
                                fixed_func=loader,
                                chunksize=10)

        load_finish = time.time()

        print("\tLoaded datasets in {}".format(load_finish - load_start))

        print(maps[0])

        datasets = {record[0]: record[1] for record in maps}
        for triple in maps:
            datasets[triple[0]]["map"] = triple[2]

        modal_shape = mode([datasets[crystal]["map"].shape for crystal in datasets]).mode
        print("\tThe modal shape is {}".format(modal_shape))

        datasets = {crystal: {"map": datasets[crystal]["map"]} for crystal in datasets if
                    datasets[crystal]["map"].shape == modal_shape}
        print("\tAfter filtering {} maps are left".format(len(datasets)))


    def load_mtzs(self):

        print("Loading MTZs...")

        mtz_load_start = time.time()
        mtzs = [any_reflection_file(str(mtz_path)) for mtz_path in self.df["mtz_path"]]
        mtz_load_finish = time.time()

        print("\tLoaded {} mtzs in {}".format(len(mtzs), mtz_load_finish-mtz_load_start))

        self.df["mtz"] = pd.Series(mtzs)


    def iterload_maps(self, x, y, z):

        for chunk in itertools.product(range(x), range(y), range(z)):
            print("Retrieving chunk: {} {} {}".format(chunk[0], chunk[1], chunk[2]))

            loader = lambda record: load_map(record, self.column, x, y, z, chunk)

            load_chunk_start = time.time()
            map_chunks = easy_mp.pool_map(processes=10,
                                          iterable=[mtz.as_miller_arrays() for mtz in self.df["mtz"]],
                                    fixed_func=loader,
                                    chunksize=10)
            times = [map_chunk[1] for map_chunk in map_chunks]
            print(times)
            map_chunks = [map_chunk[0] for map_chunk in map_chunks]
            load_chunk_finish = time.time()
            print("\tGot chunk in {}".format(load_chunk_finish - load_chunk_start))
            print("\tNumber of chunks returned is: {}".format(len(map_chunks)))
            print("\tChunks are of sizes:")
            print([map_chunk.size for map_chunk in map_chunks])

            yield chunk, map_chunks




