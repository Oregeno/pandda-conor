import argparse
import time

import numpy as np

import pandas as pd

import itertools
import array_split

import pathlib as p
from scipy.stats import mode
from cctbx import maptbx
from iotbx.reflection_file_reader import any_reflection_file
import libtbx.easy_mp as easy_mp


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d",
                        "--directory",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")
    parser.add_argument("-o",
                        "--output",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/np_maps")

    args = parser.parse_args()

    return args


class Dataset:

    def __init__(self, directory, num_samples, column):

        self.directory = directory
        self.num_samples = num_samples
        self.column = column

        columns = ["crystal", "pdb_path", "mtz_path"]

        self.df = pd.DataFrame(columns=columns)

        self.get_paths()
        if num_samples != "all":
            self.df = self.df.head(num_samples)




    def get_paths(self):

        print("Finding datasets...")

        processed_datasets = p.Path(self.directory) / "processed_datasets"

        crystals = processed_datasets.glob("*")

        datasets = [{"crystal": crystal.name,
                     "pdb_path": processed_datasets / crystal.name / "{}-pandda-input.pdb".format(crystal.name),
                     "mtz_path": processed_datasets / crystal.name / "{}-pandda-input.mtz".format(crystal.name)}
                    for crystal in crystals if crystal.is_dir()]

        df = pd.DataFrame(datasets)

        self.df = self.df.append(df, ignore_index=True, sort=True)

        print("\t found {} datasets".format(len(datasets)))

        return self.df

    def load_and_transform_mtzs(self, column="FWT"):
        print("Loading datasets")

        loader = lambda record: load_transform(record, column)

        load_start = time.time()

        maps = easy_mp.pool_map(iterable=self.df["mtz_path"],
                                fixed_func=loader,
                                chunksize=10)

        load_finish = time.time()

        print("\tLoaded datasets in {}".format(load_finish - load_start))

        print(maps[0])

        datasets = {record[0]: record[1] for record in maps}
        for triple in maps:
            datasets[triple[0]]["map"] = triple[2]

        modal_shape = mode([datasets[crystal]["map"].shape for crystal in datasets]).mode
        print("\tThe modal shape is {}".format(modal_shape))

        datasets = {crystal: {"map": datasets[crystal]["map"]} for crystal in datasets if
                    datasets[crystal]["map"].shape == modal_shape}
        print("\tAfter filtering {} maps are left".format(len(datasets)))


    def load_mtzs(self, column="FWT"):
        print("Loading datasets")

        loader = lambda record: load_transform(record, column)

        load_start = time.time()

        maps = easy_mp.pool_map(iterable=self.df["mtz_path"],
                                fixed_func=loader,
                                chunksize=10)

        load_finish = time.time()

        print("\tLoaded datasets in {}".format(load_finish - load_start))

        print(maps[0])

        datasets = {record[0]: record[1] for record in maps}
        for triple in maps:
            datasets[triple[0]]["map"] = triple[2]

        modal_shape = mode([datasets[crystal]["map"].shape for crystal in datasets]).mode
        print("\tThe modal shape is {}".format(modal_shape))

        datasets = {crystal: {"map": datasets[crystal]["map"]} for crystal in datasets if
                    datasets[crystal]["map"].shape == modal_shape}
        print("\tAfter filtering {} maps are left".format(len(datasets)))


    def load_mtzs(self):

        print("Loading MTZs...")

        mtz_load_start = time.time()
        mtzs = [any_reflection_file(str(mtz_path)) for mtz_path in self.df["mtz_path"]]
        mtz_load_finish = time.time()

        print("\tLoaded {} mtzs in {}".format(len(mtzs), mtz_load_finish-mtz_load_start))

        self.df["mtz"] = pd.Series(mtzs)


    def iterload_maps(self, x, y, z):

        for chunk in itertools.product(range(x), range(y), range(z)):
            print("Retrieving chunk: {} {} {}".format(chunk[0], chunk[1], chunk[2]))

            loader = lambda record: load_map(record, self.column, x, y, z, chunk)

            load_chunk_start = time.time()
            map_chunks = easy_mp.pool_map(processes=10,
                                          iterable=[mtz.as_miller_arrays() for mtz in self.df["mtz"]],
                                    fixed_func=loader,
                                    chunksize=10)
            times = [map_chunk[1] for map_chunk in map_chunks]
            print(times)
            map_chunks = [map_chunk[0] for map_chunk in map_chunks]
            load_chunk_finish = time.time()
            print("\tGot chunk in {}".format(load_chunk_finish - load_chunk_start))
            print("\tNumber of chunks returned is: {}".format(len(map_chunks)))
            print("\tChunks are of sizes:")
            print([map_chunk.size for map_chunk in map_chunks])

            yield chunk, map_chunks

    def parralel_save(self, dir):

        loader = lambda record: save_map(record, self.column, dir)

        print("saving maps...")
        load_chunk_start = time.time()
        easy_mp.pool_map(processes=10,
                                      iterable=[[mtz.as_miller_arrays(), crystal] for mtz, crystal in zip(self.df["mtz"], self.df["crystal"])],
                                fixed_func=loader,
                                chunksize=10)

        load_chunk_finish = time.time()
        print("\tSaved maps in {}".format(load_chunk_finish - load_chunk_start))




def save_map(miller, column, dir):

    for miller_array in miller[0]:

        if (column == miller_array.info().labels[0]):
            ed_map = miller_array.fft_map(symmetry_flags=maptbx.use_space_group_symmetry,
                                          resolution_factor=0.5 / 3.0,
                                          d_min=3.0).apply_sigma_scaling().real_map().as_numpy_array()


            np.savez_compressed(dir + "/" + miller[1], ed_map)


def load_map(miller, column, x, y, z, chunk):

    load_start = time.time()

    for miller_array in miller:

        if (column == miller_array.info().labels[0]):

            ed_map = miller_array.fft_map(symmetry_flags=maptbx.use_space_group_symmetry,
                                          resolution_factor=0.5 / 3.0,
                                          d_min=3.0).apply_sigma_scaling().real_map().as_numpy_array()

            split = array_split.shape_split(ed_map.shape, axis=[x, y, z])

            chunk = ed_map[split[chunk[0], chunk[1], chunk[2]]]

            load_finish=time.time()

            return chunk, load_finish-load_start

    return None







def load_transform(item, map_type=None):

    crystal = item[0]
    record = item[1]

    # record["mtz"] = any_reflection_file(str(record["mtz_path"]))
    # record["miller"] = record["mtz"].as_miller_arrays()
    mtz = any_reflection_file(str(record["mtz_path"]))
    miller = mtz.as_miller_arrays()
    for miller_array in miller:

        if (map_type == miller_array.info().labels[0]):
            # print("FFTing...")
            start_fft = time.time()

            ed_map = miller_array.fft_map(symmetry_flags=maptbx.use_space_group_symmetry,
                                                            resolution_factor=0.5 / 3.0,
                                                            d_min=3.0).apply_sigma_scaling().real_map().as_numpy_array().flatten()
            finish_fft = time.time()
            # print("FFT'd in {}".format(finish_fft - start_fft))
            # print(datasets[crystal]["map"].real_map().as_numpy_array().shape)

            return (crystal, record, ed_map)

    return (crystal, record, None)

def load_datasets_parralel(map_directory_path, file_class="-pandda-input.mtz", map_type="FWT", exclude=None, include=None,
                  max_datasets=500, mtz=1):
    print("Finding datasets...")

    processed_datasets = p.Path(map_directory_path) / "processed_datasets"

    paths = processed_datasets.glob("**/*" + file_class)

    datasets = {}
    for i, path in enumerate(paths):
        if i > max_datasets: break
        datasets[path.parent.name] = {"mtz_path": path}

    print("found {} Datasets. FFTing...".format(len(datasets)))

    if mtz == 1:

        print("Loading datasets")

        loader = lambda record: load_transform(record, map_type)

        load_start = time.time()

        maps = easy_mp.pool_map(iterable=datasets.items(),
                                fixed_func=loader,
                                chunksize=10)

        load_finish = time.time()

        print("\tLoaded datasets in {}".format(load_finish-load_start))

        print(maps[0])

        datasets = {record[0]: record[1] for record in maps}
        for triple in maps:
            datasets[triple[0]]["map"] = triple[2]

        modal_shape = mode([datasets[crystal]["map"].shape for crystal in datasets]).mode
        print("\tThe modal shape is {}".format(modal_shape))

        datasets = {crystal: {"map": datasets[crystal]["map"]} for crystal in datasets if datasets[crystal]["map"].shape == modal_shape}
        print("\tAfter filtering {} maps are left".format(len(datasets)))

    else:
        print("Not Loading MTZs")


    return datasets



def load_datasets(map_directory_path, file_class="-pandda-input.mtz", map_type="FWT", exclude=None, include=None,
                  max_datasets=500):
    print("Finding datasets...")

    processed_datasets = p.Path(map_directory_path) / "processed_datasets"

    paths = processed_datasets.glob("**/*" + file_class)

    datasets = {}
    for i, path in enumerate(paths):
        if i > max_datasets: break
        datasets[path.parent.name] = {"mtz_path": path}

    print("found {} Datasets. FFTing...".format(len(datasets)))

    print("Loading datasets")
    for i, crystal in enumerate(datasets):
        # print("Getting map for {}".format(crystal))

        # print("\tLoading dataset from {}".format(str(datasets[crystal]["mtz_path"])))
        datasets[crystal]["mtz"] = any_reflection_file(str(datasets[crystal]["mtz_path"]))
        datasets[crystal]["miller"] = datasets[crystal]["mtz"].as_miller_arrays()


        for miller_array in datasets[crystal]["miller"]:

            if (map_type == miller_array.info().labels[0]):
                # print("FFTing...")
                start_fft = time.time()

                datasets[crystal]["map"] = miller_array.fft_map(symmetry_flags=maptbx.use_space_group_symmetry,
                                                                resolution_factor=0.5/3.0,
                                                                d_min=3.0).apply_sigma_scaling().real_map().as_numpy_array().flatten()
                finish_fft = time.time()
                print("FFT'd in {}".format(finish_fft-start_fft))
                # print(datasets[crystal]["map"].real_map().as_numpy_array().shape)

    modal_shape = mode([datasets[crystal]["map"].shape for crystal in datasets]).mode
    print("\tThe modal shape is {}".format(modal_shape))

    datasets = {crystal: {"mtz": datasets[crystal]["mtz"], "map": datasets[crystal]["map"]} for crystal in datasets if datasets[crystal]["map"].shape == modal_shape}
    print("\tAfter filtering {} maps are left".format(len(datasets)))


    return datasets


if __name__ == "__main__":
    print("Parsing args...")
    args = parse_args()

    print("Loading datasets...")
    datasets = Dataset(args.directory, num_samples="all", column="FWT")

    print("Loading mtzs...")
    datasets.load_mtzs()

    # print("Saving map chunks...")
    # for ijk, map_chunks in datasets.iterload_maps(5, 5, 5):
    #
    #     shape_array = np.array([chunk.shape for chunk in map_chunks])
    #     modal_shape = mode(shape_array).mode
    #     filtered_map_chunks = [map_chunk for map_chunk in map_chunks if np.array_equal(np.array(map_chunk.shape).reshape(-1,),
    #                                                                                    np.array(modal_shape).reshape(-1,))]
    #     print("\tAfter filtering on shape {} chunks remain".format(len(filtered_map_chunks)))
    #     print([map_chunk.shape for map_chunk in filtered_map_chunks])
    #     sample_by_feature = np.stack(filtered_map_chunks)
    #
    #     save_start = time.time()
    #     np.savez_compressed(args.directory + "/" + "chunks/" + "{}_{}_{}".format(ijk[0], ijk[1], ijk[2]), sample_by_feature)
    #     save_finish = time.time()
    #     print("\tSaved in {}".format(save_finish-save_start))

    datasets.parralel_save(args.output)