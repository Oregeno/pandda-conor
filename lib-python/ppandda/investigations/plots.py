import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import itertools

import numpy as np

from matplotlib import colors

cmap = colors.ListedColormap(['k','b','y','g','r'])

color_iter = itertools.cycle(['navy', 'c', 'cornflowerblue', 'gold',
                              'darkorange'])

def save_scatter(x, y, title):

    print("Generating plot: {}...".format(title))

    fig, ax = plt.subplots()
    fig.set_size_inches(45, 15)


    ax.scatter(x, y)

    fig.savefig(title)


    # for j, c in enumerate(combinations):
    #
    #     ax[j].scatter(components[:, c[0]], components[:, c[1]])
    #     print(components.shape)
    #
    #     for i, txt in enumerate(datasets):
    #         ax[j].annotate(txt[0], (components[i, c[0]], components[i, c[1]]))
    #
    #     ax[j].set_xlabel(c)
    #
    # fig.savefig("components_{}_x0122.png".format(args.num_components))

def save_coloured_scatter(x, y, colours, title):

    print("Generating plot: {}...".format(title))

    fig, ax = plt.subplots()
    fig.set_size_inches(15, 15)

    for unique_colour in np.unique(colours):

        ax.scatter(x[colours == unique_colour],
                   y[colours == unique_colour],
                   # c=colours[colours == unique_colour],
                   cmap=cmap,
                   label="{}".format(unique_colour))

    ax.legend(loc="upper left")

    fig.savefig(title)

