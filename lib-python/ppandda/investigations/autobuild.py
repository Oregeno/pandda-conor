import argparse
import os
from shutil import rmtree
import re
from subprocess import PIPE, Popen
import time

import numpy as np
from scipy.optimize import basinhopping, brute, differential_evolution
import pandas as pd
import sqlalchemy

from biopandas.pdb import PandasPdb

from rdkit import Chem
from rdkit.Chem import AllChem

from sklearn.cluster import DBSCAN
from skimage.measure import label


from qfit_ligand.validator import Validator
from qfit_ligand.volume import Volume
from qfit_ligand.structure import Structure


# Panddas optoins

pd.options.display.max_columns = 20

pd.options.display.max_colwidth = 50


def parse_args():
    parser = argparse.ArgumentParser()

    # PanDDA run
    parser.add_argument("-r", "--pandda_run", default="/dls/science/groups/i04-1/conor_dev/autobuild")

    # Output
    parser.add_argument("-o", "--output", default="autobuild_labeling_NUDT7A-x0436/")

    # Database
    parser.add_argument("-hs", "--host", default="172.23.142.43")
    parser.add_argument("-p", "--port", default="5432")
    parser.add_argument("-d", "--database", default="test_xchem")
    parser.add_argument("-u", "--user", default="conor")
    parser.add_argument("-pw", "--password", default="c0n0r")

    # QFit parameters
    parser.add_argument("-s", "--angular_step", default=6, type=int)
    parser.add_argument("-b", "--degrees_freedom", default=2, type=int)

    # Interpeter args
    parser.add_argument("-cpy", "--ccp4_python", default="/home/zoh22914/myccp4python")


    args = parser.parse_args()

    return args


def to_sample_by_feature(dense):

    it = np.nditer(dense, flags=['multi_index'])

    rows = []

    while not it.finished:

        if it[0] < 0.99:
            it.iternext()
            continue

        row = [it.multi_index[0], it.multi_index[1], it.multi_index[2], it[0]]
        rows.append(row)
        it.iternext()

    sample_by_feature = np.array(rows)

    return sample_by_feature


def to_dense(sample_by_feature, dimensions):

    dense = np.zeros(dimensions)

    for sample in sample_by_feature:

        dense[int(sample[0,0]), int(sample[0,1]), int(sample[0,2])] = sample[0,3]

    return dense



def rotate(sample_by_feature, x, y, z):

    mean_x = np.mean(sample_by_feature[:, 0])
    mean_y = np.mean(sample_by_feature[:, 1])
    mean_z = np.mean(sample_by_feature[:, 2])

    de_meaned_x = sample_by_feature[:, 0] - mean_x
    de_meaned_y = sample_by_feature[:, 1] - mean_y
    de_meaned_z = sample_by_feature[:, 2] - mean_z

    sample_by_feature[:, 0] = de_meaned_x
    sample_by_feature[:, 1] = de_meaned_y
    sample_by_feature[:, 2] = de_meaned_z

    sx = np.sin(x)
    cx = np.cos(x)
    sy = np.sin(y)
    cy = np.cos(y)
    sz = np.sin(z)
    cz = np.cos(z)

    rx = [[1, 0, 0],
           [0, cx, sx],
           [0, -sx, cx]]
    ry = [[cy, 0, -sy],
           [0, 1, 0],
           [sy, 0, cy]]
    rz = [[cz, sz, 0],
           [-sz, cz, 0],
           [0, 0, 1]]

    rotated_x = np.matmul(rx, sample_by_feature.T)
    rotated_y = np.matmul(ry, rotated_x)
    rotated_z = np.matmul(rz, rotated_y)

    rotated_matrix = rotated_z.T

    rotated_matrix[:, 0] = rotated_matrix[:, 0] + mean_x
    rotated_matrix[:, 1] = rotated_matrix[:, 1] + mean_y
    rotated_matrix[:, 2] = rotated_matrix[:, 2] + mean_z


    return rotated_matrix


def recentre(ligand, centroid):

    ligand_mean = [ligand.df['HETATM']["x_coord"].mean(),
                   ligand.df['HETATM']["y_coord"].mean(),
                   ligand.df['HETATM']["z_coord"].mean()]

    x_mean_diff = ligand_mean[0] - centroid[0]
    y_mean_diff = ligand_mean[1] - centroid[1]
    z_mean_diff = ligand_mean[2] - centroid[2]
    print("vector is ({},{},{})".format(x_mean_diff, y_mean_diff, z_mean_diff))

    centered_x = ligand.df['HETATM']["x_coord"] - x_mean_diff
    centered_y = ligand.df['HETATM']["y_coord"] - y_mean_diff
    centered_z = ligand.df['HETATM']["z_coord"] - z_mean_diff

    ligand.df['HETATM']["x_coord"] = centered_x
    ligand.df['HETATM']["y_coord"] = centered_y
    ligand.df['HETATM']["z_coord"] = centered_z

    return ligand, ligand_mean


def load_map(map_path, output_path, ccp4_python, ccp42np):
    os.popen("{} {} -m {} -o {} -v {}".format(ccp4_python, ccp42np, map_path, output_path, 1))

    numpy_map = np.load(output_path)

    return numpy_map

def save_map(target_map, template, output, ccp4_python, np2ccp4):

    np.save(output, target_map)

    os.popen("{} {} -a {} -m {}".format(ccp4_python, np2ccp4, output, template))


def generate_bdc_map(bdc, target_map, mean_map, template=None, output=None, python=None, np2ccp4=None):

    bdc_map = (target_map - bdc*mean_map)/(1-bdc)

    # np.save(output + "bdc.npy", bdc_map)

    # os.popen("{} {} -a {} -m {}".format(python, np2ccp4, output + "bdc.npy", template))

    return bdc_map


def translate(sample_by_feature, x, y, z):

    sample_by_feature[:, 0] = sample_by_feature[:, 0] + x
    sample_by_feature[:, 1] = sample_by_feature[:, 1] + y
    sample_by_feature[:, 2] = sample_by_feature[:, 2] + z

    return sample_by_feature


def gen_embedding(x, embedding_coords):
    # Extract
    sample_by_feature = embedding_coords.as_matrix(columns=["x_coord", "y_coord", "z_coord"])

    # Rotate
    rot = [x[3], x[4], x[5]]
    rotated = rotate(sample_by_feature, rot[0], rot[1], rot[2])
    # print("Rotated x: ({}), y: ({}), z: ({})".format(rot[0], rot[1], rot[2]))

    # Translate
    trans = [x[0], x[1], x[2]]
    translated = translate(rotated, trans[0], trans[1], trans[2])
    # print("Translated x: ({}), y: ({}), z: ({})".format(trans[0], trans[1], trans[2]))

    # Update
    embedding_coords["x_coord"] = translated[:, 0]
    embedding_coords["y_coord"] = translated[:, 1]
    embedding_coords["z_coord"] = translated[:, 2]

    return embedding_coords



def get_rscc(embedding, structure_dir, xmap, resolution):
    # Output  model
    pandas_embedding_path = structure_dir + "pandas_embedding.pdb"
    embedding.to_pdb(pandas_embedding_path)

    # Calculate RSCC
    structure_ligand = Structure.fromfile(pandas_embedding_path)

    structure_ligand.b = structure_ligand.b + 20.0

    validator = Validator(xmap, float(resolution))
    rscc = validator.rscc(structure_ligand)

    print("RSCC is {}".format(rscc))

    return rscc





def get_mask(embedding, volume):

    alpha, beta, gamma = np.deg2rad(volume.angles)
    a, b, c = volume.lattice_parameters
    cos_gamma = np.cos(gamma)
    sin_gamma = np.sin(gamma)
    cos_alpha = np.cos(alpha)
    cos_beta = np.cos(beta)
    omega = np.sqrt(
        1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
        2 * cos_alpha * cos_beta * cos_gamma
    )


    cartesian_to_lattice = np.asarray([
        [1, -cos_gamma / sin_gamma,
         (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
        [0, 1 / sin_gamma,
         (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
        [0, 0,
         sin_gamma / omega],
    ])

    sample_by_feature = embedding.df["HETATM"].as_matrix(columns=["x_coord", "y_coord", "z_coord"])
    #
    # tiled = np.tile(volume.origin, (20, 1)).T
    #
    # adjusted_coords = sample_by_feature.T - tiled
    #
    # lattice_coords = np.matmul(cartesian_to_lattice.T, adjusted_coords)

    mask = np.zeros(volume.array.shape)

    # lattice_coords = lattice_coords/0.5
    # lattice_coords = lattice_coords.T - volume.offset
    # print(volume.offset)
    # print(volume.origin)

    for coords in sample_by_feature:
        # print(coords, coords.shape)
        shifted = coords - volume.origin
        tilted = np.matmul(cartesian_to_lattice, shifted)
        stretched = tilted/0.5

        # print(stretched)
        # x = int(stretched[0]) -2
        # y = int(stretched[1]) -1
        # z = int(stretched[2]) + 10

        x = int(round(stretched[0])) +2
        y = int(round(stretched[1])) -2
        z = int(round(stretched[2])) +2

        step = 2

        mask[x-step:x+step, y-step:y+step, z-step:z+step] = 1

    # print(cartesian_to_lattice)

    print("Masked: {} voxels".format(np.sum(mask)))
    print("Volume array voxels past cutoff: {} voxels".format(np.sum((volume.array > 0.9))))

    mask_score = np.sum(mask[volume.array > 0.9])/np.sum(mask)

    print("mask score is: {}".format(mask_score))

    # return mask_score, mask

    return mask


def get_skeleton_score(embedding_coords, volume, volume_xmap):

    alpha, beta, gamma = np.deg2rad(volume.angles)
    a, b, c = volume.lattice_parameters
    cos_gamma = np.cos(gamma)
    sin_gamma = np.sin(gamma)
    cos_alpha = np.cos(alpha)
    cos_beta = np.cos(beta)
    omega = np.sqrt(
        1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
        2 * cos_alpha * cos_beta * cos_gamma
    )


    cartesian_to_lattice = np.asarray([
        [1, -cos_gamma / sin_gamma,
         (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
        [0, 1 / sin_gamma,
         (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
        [0, 0,
         sin_gamma / omega],
    ])

    sample_by_feature = embedding_coords.as_matrix(columns=["x_coord", "y_coord", "z_coord"])

    shifted = sample_by_feature - np.array(volume.origin)

    tilted = np.matmul(cartesian_to_lattice, shifted.T)

    stretched = tilted/np.array(volume_xmap.voxelspacing).reshape(3,1)

    indicies = np.asarray(stretched, dtype=np.int32)




    indicies = np.mod(indicies, np.array(volume_xmap.array.shape).reshape(3,1))


    mask_score = np.sum(volume.array[indicies[0, :], indicies[1, :], indicies[2, :]])/indicies.shape[1]


    return mask_score


def score_conformer(x, embedding, receptor, centorid, xmap, structure_dir, coordinates):

    embedding_coords = embedding.df["HETATM"].copy()
    # print(type(embedding_coords))

    new_embedding_coords = gen_embedding(x, embedding_coords)

    # clash = check_clash(embedding, receptor, centorid)
    clash = 0

    # rscc = get_rscc(embedding, structure_dir, xmap)

    score = get_skeleton_score(new_embedding_coords , xmap)

    # print("New ligand mean is ({}, {}, {})".format(embedding.df["HETATM"]["x_coord"].mean(),
    #                                                embedding.df["HETATM"]["y_coord"].mean(),
    #                                                embedding.df["HETATM"]["z_coord"].mean()))

    # embedding.df["HETATM"]["x_coord"] = coordinates[0]
    # embedding.df["HETATM"]["y_coord"] = coordinates[1]
    # embedding.df["HETATM"]["z_coord"] = coordinates[2]
    # final_score = (1 + clash) - score
    if clash > 0:
        final_score = 2 - score
    else:
        final_score = 1-score

    # print("Conformer score is {}".format(final_score))

    return final_score


def find_ligand_cluster(array, point, size_cutoff, initial_cutoff=1.0, final_cutoff=2.0, steps=20):

    cutoffs = np.linspace(initial_cutoff, final_cutoff, steps)

    valid_mask = 0

    print("Array shape is {}".format(array.shape))
    print("point is at {}".format(point))
    print("Size cutoff is {}".format(size_cutoff))

    for cutoff in cutoffs:
        print("Trying cutoff of {}".format(cutoff))

        cutoff_mask = np.zeros(array.shape)

        cutoff_mask[array > cutoff] = 1

        if cutoff_mask[point[0], point[1], point[2]] == 0:

            if valid_mask == 0:
                print("No valid mask found, condider a lower initial cutoff")
                return None
            else:
                print("Point no longer past cutoff, terminating procedure, examine results manually")
                return mask

        # label
        labels = label(cutoff_mask, connectivity=3)

        # find label of point
        point_label = labels[point[0], point[1], point[2]]

        # mask points with same label
        mask = np.zeros(labels.shape)
        mask[labels == point_label] = 1

        # count points
        count = np.sum(mask)
        print("Number of non-zero points in mask is {}".format(count))

        # Check if less than size, if so break

        if count <= 1:
            raise Exception("Mask too small, probably an error")

        if count <= size_cutoff:
            print("Optimum cutoff is {}".format(cutoff))
            return mask

        valid_mask = 1

    raise Exception("Unable to find a mask under cutoff")


class Transformer:

    def __int__(self, volume_xmap):

        self.origin = volume_xmap.origin

        self.voxel_spacing = volume_xmap.voxelspacing

        self.shape = volume_xmap.array.shape

        alpha, beta, gamma = np.deg2rad(volume_xmap.angles)
        a, b, c = volume_xmap.lattice_parameters
        cos_gamma = np.cos(gamma)
        sin_gamma = np.sin(gamma)
        cos_alpha = np.cos(alpha)
        cos_beta = np.cos(beta)
        omega = np.sqrt(
            1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
            2 * cos_alpha * cos_beta * cos_gamma
        )

        self.cartesian_to_lattice = np.asarray([
            [1, -cos_gamma / sin_gamma,
             (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
            [0, 1 / sin_gamma,
             (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
            [0, 0,
             sin_gamma / omega],
        ])

        self.lattice_to_cartesian = np.asarray([
            [1, cos_gamma, cos_beta],
            [0, sin_gamma, (cos_alpha - cos_beta * cos_gamma) / sin_gamma],
            [0, 0, omega / sin_gamma],
        ])

    def cartesian_to_lattice(self, coords):


        shifted = np.array(coords) - self.origin
        tilted = np.matmul(self.cartesian_to_lattice, shifted)
        stretched = tilted / np.array(self.voxelspacing).reshape(1,3)
        print(stretched.shape)

        stretched_modulo = np.mod(stretched, self.shape)

        return stretched_modulo


def build(output,
          centroid,
          bdc_map,
          receptor,
          volume_xmap,
          template_map_path,
          ligand_smiles=None,
          ligand_sdf_path=None):


    # Establish python vars
    home = "/home/zoh22914/"
    base_python = "python"
    ccp4_python = home + "myccp4python"
    np2ccp4 = home + "pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py"
    ccp42np = home + "pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py"

    qfit_dir = output + "qfit/"

    structure_dir = output + "structures/"

    map_dir = output + "maps/"


    receptor_pdb = PandasPdb().read_pdb(receptor)
    receptor_pdb.to_pdb(structure_dir + "input_pdb.pdb")

    if ligand_smiles is not None:
        ligand = Chem.MolFromSmiles(ligand_smiles)
    if ligand_sdf_path is not None:
        ligand = Chem.SDMolSupplier(ligand_sdf_path)

    AllChem.EmbedMultipleConfs(ligand,
                                            clearConfs=True,
                                            numConfs=1000,
                                            pruneRmsThresh=1.5)

    volume_xmap.array = bdc_map

    # CLUSTER

    transformer = Transformer(xmap=volume_xmap)

    lattive_event_coords = transformer.caretsian_to_lattice(centroid)

    approximate_ligand_volume = 30*ligand.GetNumAtoms()

    # TODO: !!!!!!!!!WILL NOT WORK WHEN CLUSTER CENTROID NOT IN VOLUME!!!!!!!!

    centorid_lattice_coords = np.asarray(lattive_event_coords, dtype=np.int32).reshape(3)

    print("Centroid lattice coords are {}".format(centorid_lattice_coords))

    print("Value of map at event centroid is {}".format(volume_xmap.array[centorid_lattice_coords[0],
                                                                          centorid_lattice_coords[1],
                                                                          centorid_lattice_coords[2]]))

    mask = find_ligand_cluster(volume_xmap.array,
                               centorid_lattice_coords,
                               approximate_ligand_volume,
                               initial_cutoff=1.0,
                               final_cutoff=2.0)



    non_zero_indexes = np.nonzero(mask)

    cluster_coords = [np.mean(non_zero_indexes[0]),
                      np.mean(non_zero_indexes[1]),
                      np.mean(non_zero_indexes[2])]

    print("lattice_coordinates of closest cluster is {}".format(cluster_coords))

    lattice_distance = cluster_coords - lattive_event_coords
    print("lattice distance between clusters is {}".format(lattice_distance))

    lattice_distance_unstretched = np.array(lattice_distance) * np.array(volume_xmap.voxelspacing)
    print("Lattice distance unstetched is {}".format(lattice_distance_unstretched))
    lattice_distance_untilted = np.matmul(transformer.lattice_to_cartesian, lattice_distance_unstretched.T )
    print("Corresponding cartesian distance is {}".format(lattice_distance_untilted))


    lattice_distance_untilted = lattice_distance_untilted.reshape(3,)

    volume_xmap.array = mask

    embeddings = {}

    for embedding_id in embeddings:
        # Optimise
        AllChem.MMFFOptimizeMolecule(ligand, confId=embedding_id)

        # Save to pdb
        Chem.MolToPDBFile(ligand, "embedding.pdb", confId=embedding_id)

        # Open
        pandas_embedding = PandasPdb().read_pdb("embedding.pdb")
        # embedding_copy = pandas_embedding.copy()

        # recentre
        pandas_embedding, ligand_mean = recentre(pandas_embedding, np.array(centroid) + lattice_distance_untilted)
        coordinates = [pandas_embedding.df['HETATM']["x_coord"].copy(),
                       pandas_embedding.df['HETATM']["y_coord"].copy(),
                       pandas_embedding.df['HETATM']["z_coord"].copy()]


        func = lambda x: score_conformer(x, pandas_embedding, receptor_pdb, centroid, volume_xmap, structure_dir,
                                         coordinates)


        bounds = [(-5, 5),
                  (-5, 5),
                  (-5, 5),
                  (0, 6.3),
                  (0, 6.3),
                  (0, 6.3)]


        result = differential_evolution(func, bounds, maxiter=150, disp=True)

        conformer = gen_embedding(result.x, pandas_embedding.df["HETATM"])

        pandas_embedding.df["HETATM"] = conformer

        print("Final conformer centre at ({}, {}, {})".format(conformer["x_coord"].mean(),
                                                    conformer["y_coord"].mean(),
                                                    conformer["z_coord"].mean()))

        conf_embedding_path = structure_dir + "{}_{}.pdb".format(embedding_id, 1-result.fun)
        pandas_embedding.to_pdb(conf_embedding_path)

        mask_map = get_mask(pandas_embedding, volume_xmap)

        np.save(map_dir + "xmap.npy", mask_map)

        process = Popen("{} {} -a {} -m {}".format(ccp4_python, np2ccp4, map_dir + "xmap.npy", template_map),
                        shell=True,
                        stdout=PIPE)


        embeddings[embedding_id] = {"embedding_path": map_dir + "xmap.npy.ccp4",
                                    "skeleton_score": 1-result.fun}

    return embeddings

