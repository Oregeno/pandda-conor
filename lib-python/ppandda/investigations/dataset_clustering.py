from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN


# import MDAnalysis
# import MDAnalysis.analysis.encore as encore
# from MDAnalysis import Merge
# from MDAnalysis.topology import guessers as guessers
#
# from MDAnalysis.coordinates.memory import MemoryReader

import argparse
import time
import numpy as np

import plots

import ccp4_io_utils as io

# import hdbscan

from scipy.stats import mode



def parse_args():
    parser = argparse.ArgumentParser()

    # Input
    parser.add_argument("-d", "--map_directory_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Settings
    parser.add_argument("-n", "--num_components",
                        default=2)

    # Output

    args = parser.parse_args()

    return args

#
# def cluster_pdbs_MDAnalysis(pdb_paths):
#
#     print("Loading MDAnalysis Universes...")
#     load_start = time.time()
#     universes = [MDAnalysis.Universe(str(pdb_path)).select_atoms("name CA")
#                  for i, pdb_path in enumerate(pdb_paths) if i < 50]
#     load_finish = time.time()
#     print("\tLoaded MDAnalysis Universes in {}".format(load_finish - load_start))
#
#     # protein = universes[0].select_atoms("protein")
#     merge_start = time.time()
#     combined = Merge(universes[1])
#     merge_finish = time.time()
#     print("\tMerged trajectories in {}".format(merge_finish-merge_start))
#     # print("\tTrajectory length is {}".format(len(combined.trajectory)))
#
#     print("Building topology...")
#     top_start = time.time()
#     topology = guessers.guess_bonds(combined.atoms,
#                                     combined.select_atoms("protein").positions,
#                                     vdwradii={"CL":1.6})
#     top_finish = time.time()
#     print("\tBuilt topology in {}".format(top_finish - top_start))
#     print(topology)
#
#
#
#     print("Generating psuedo-trajectory...")
#     coord_array = np.stack([pdb.atoms.positions for pdb in universes])
#
#     combined.load_new(coord_array, format=MemoryReader, order="fac")
#     print("\tNew trajectory of length: {}".format(len(combined.trajectory)))
#
#     print("Clustering PDBs")
#     cluster_start = time.time()
#     clustering = encore.cluster(combined,
#                                 selection="name CA")
#     cluster_finish = time.time()
#     print("\tClustered PDBs in {}".format(cluster_finish-cluster_start))
#
#     return clustering


def cluster_maps(datasets, num_components=2, iter=True, x=10, y=10, z=10, samples="all"):

    output = "/home/zoh22914/pandda-conor/lib-python/ppandda/investigations/map_cluster_plots/"

    reduced_maps = np.zeros((datasets.num_samples, x, y, z))

    # Load in chunk
    for ijk, map_chunks in datasets.iterload_maps(x,y,z):

        # Get modal chunk
        shape_array = np.array([chunk.shape for chunk in map_chunks])
        modal_shape = mode(shape_array).mode
        filtered_map_chunks = [map_chunk for map_chunk in map_chunks if map_chunk.shape == modal_shape]
        print("\tAfter filtering on shape {} chunks remain".format(len(filtered_map_chunks)))
        print([map_chunk.shape for map_chunk in filtered_map_chunks])


        # Perform PCA
        sample_by_feature = np.vstack(map_chunks)
        pca = PCA(n_components=num_components)
        print("Performing PCA on {} components!".format(num_components))
        pca_start = time.time()
        components = pca.fit_transform(sample_by_feature)
        pca_finish = time.time()
        print("\tFinished PCA in {}".format(pca_finish-pca_start))

        # clusterer = DBSAN.HDBSCAN(min_cluster_size=5,
        #                             allow_single_cluster=True).fit(components)

        clusterer = DBSCAN(eps=100, min_samples=5).fit(components)

        plots.save_coloured_scatter(components[:, 0], components[:, 1], clusterer.labels_,
                                    output + "{}_{}_{}.png".format(ijk[0], ijk[1], ijk[2]))

        exit()



        reduced_maps[:, ijk[0], ijk[1], ijk[2]] == clusterer.labels_





if __name__ == "__main__":

    args = parse_args()

    datasets = io.Dataset(args.map_directory_path, num_samples=50, column="FWT")

    # Load mtz
    datasets.load_mtzs()

    # Cluster maps
    map_clusters = cluster_maps(datasets)

    #
    for map_cluster in map_clusters:
        pdb_clusters = cluster_pdbs_biopandas(map_cluster)
        central_pdb_cluster = exclude_outliers(pdb_clusters)
        centroid = get_centroid(map_cluster, central_pdb_cluster)





    #
    #
    # # Plot
    # combinations = list(itertools.combinations(range(int(args.num_components)), 2))
    # num_combinations = len(combinations)
    #
    # fig, ax = plt.subplots(1, num_combinations)
    # fig.set_size_inches(45, 15)
    #
    # for j, c in enumerate(combinations):
    #
    #     ax[j].scatter(components[:, c[0]], components[:, c[1]])
    #     print(components.shape)
    #
    #     for i, txt in enumerate(datasets):
    #         ax[j].annotate(txt[0], (components[i, c[0]], components[i, c[1]]))
    #
    #     ax[j].set_xlabel(c)
    #
    # fig.savefig("components_{}_x0122.png".format(args.num_components))
    #
    # for i in range(int(args.num_components)):
    #
    #     print(dir(pca))
    #
    #     component = pca.components_[i]
    #
    #     # array = np.zeros(int(args.num_components))
    #     #
    #     # array[i] = 1000
    #     #
    #     # print(array)
    #     #
    #     # component = pca.inverse_transform(array.reshape(1, -1))
    #     #
    #     # print(component.shape)
    #     #
    #     # print("20th component is", component[20])
    #
    #     component_reshaped = component.reshape((225, 225, 180))
    #
    #     print("saving to ica_{}.npy".format(i))
    #
    #     np.save("ica_{}.npy".format(i), component_reshaped)
    #
    #     os.popen("~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py -a ica_{}.npy -m processed_datasets/PDK2-x0001/PDK2-x0001-pandda-input_2mFo-DFc.ccp4".format(i))
    #
    #
    # for i in range(int(args.num_components)):
    #
    #     print(dir(pca))
    #
    #     component = pca.components_[i]
    #
    #     # array = np.zeros(int(args.num_components))
    #     #
    #     # array[i] = 1000
    #     #
    #     # print(array)
    #     #
    #     # component = pca.inverse_transform(array)
    #
    #     print("20th component is", component[20])
    #
    #     component_reshaped = component.reshape((225, 225, 180))
    #
    #     print("saving to ica_neg{}.npy".format(i))
    #
    #     np.save("ica_neg_{}.npy".format(i), -component_reshaped)
    #
    #     os.popen("~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py -a ica_neg_{}.npy -m processed_datasets/PDK2-x0001/PDK2-x0001-pandda-input_2mFo-DFc.ccp4".format(i))
    #
    #
    #
    #
