import argparse
import numpy as np
import os

from iotbx.file_reader import any_file


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--map_path")
    parser.add_argument("-o", "--output", default="")
    parser.add_argument("-v", "--overide_output", default=0)

    args = parser.parse_args()

    return args


if __name__ == "__main__":

    # Parse args
    args = parse_args()

    # load map
    ccp4_map = any_file(args.map_path).file_object

    # Extract
    flex_array = ccp4_map.map_data()

    print("Flex array is of shape {}".format(flex_array.accessor().all()))

    # Convert
    #numpy_array = np.asarray(flex_array)
    numpy_array = flex_array.as_numpy_array()

    # Save
    if args.overide_output == "1":
        np.save(args.output, numpy_array)
    else:
        np.save(args.output + os.path.basename(args.map_path) + ".npy", numpy_array)

    # Feedback
    print("Successfully wrote array of shape {} to {}".format( numpy_array.shape, args.map_path + ".npy"))


