import argparse

import pandas as pd

from biopandas.pdb import PandasPdb

pd.options.display.max_columns = 30

pd.options.display.max_colwidth = 10


def parse_args():
    parser = argparse.ArgumentParser()

    # PanDDA run
    parser.add_argument("-r", "--receptor")
    parser.add_argument("-l", "--ligand")
    parser.add_argument("-o", "--output")


    args = parser.parse_args()

    return args


def trim_clashes(receptor, ligand):

    # receptor_waters = receptor.df["HETATM"][receptor.df["HETATM"]["residue_name"] == "WAT"]

    for atom in ligand.df["HETATM"].itertuples():

        distances = receptor.distance_df(receptor.df["HETATM"],
                                         xyz=(atom.x_coord, atom.y_coord, atom.z_coord))


        receptor.df["HETATM"] = receptor.df["HETATM"][distances > 1.0]

    return receptor


def sequential(series):

    print("SEQUENTIALISING!")

    initial = series.iloc[0]

    series = pd.Series(series.index) + initial

    return series


if __name__ == "__main__":

    args = parse_args()

    receptor = PandasPdb().read_pdb(args.receptor)
    ligand = PandasPdb().read_pdb(args.ligand)

    receptor = trim_clashes(receptor, ligand)

    ligand.df["HETATM"]["b_factor"] = 20.0

    ligand.df["HETATM"]["chain_id"] = "Z"
    ligand.df["HETATM"]["residue_name"] = "LIG"

    receptor.df["HETATM"] = pd.concat([receptor.df["HETATM"], ligand.df["HETATM"]], ignore_index=True, sort=False)

    receptor.df["HETATM"]["line_idx"] = sequential(receptor.df["HETATM"]["line_idx"])

    receptor.df["HETATM"]["atom_number"] = sequential(receptor.df["HETATM"]["atom_number"])

    print(receptor.df["ATOM"].columns)
    print(receptor.df["HETATM"].columns)

    receptor.df["HETATM"]["b_factor"] = pd.to_numeric(receptor.df["HETATM"]["b_factor"], downcast="integer").round(decimals=2)

    receptor.df["ATOM"]["b_factor"] = pd.to_numeric(receptor.df["ATOM"]["b_factor"], downcast="integer").round(decimals=2)

    print(receptor.df["ATOM"]["b_factor"].head())
    print(receptor.df["HETATM"]["b_factor"].head())
    print(receptor.df["ATOM"].head())
    # exit()

    print(receptor.df["ATOM"]["b_factor"].iloc[0])
    print(type(receptor.df["ATOM"]["b_factor"].iloc[0]))


    final_line = receptor.df["HETATM"]["line_idx"].iloc[-1]
    print("Final line idx is {}".format(final_line))

    receptor.df["OTHERS"]["line_idx"].iloc[-1] = str(int(final_line) + 1)

    receptor.impute_element(inplace=True)

    receptor.to_pdb(args.output)

