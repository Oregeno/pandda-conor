import numpy as np

from skimage.measure import label

# Suggested size_cutoff ~9-10* number of atoms in ligand

def find_ligand_cluster(array, point, size_cutoff, initial_cutoff=1.3, final_cutoff=2.2, steps=10):

    cutoffs = np.linspace(initial_cutoff, final_cutoff, steps)

    for cutoff in cutoffs:
        print("Trying cutoff of {}".format(cutoff))

        cutoff_mask = np.zeros(array.shape)

        cutoff_mask[array > cutoff] = 1

        # label
        labels = label(cutoff_mask, connectivity=3)

        # find label of point
        point_label = labels[point]

        # mask points with same label
        mask = np.zeros(labels.shape)
        mask[labels == point_label] = 1

        # count points
        count = np.sum(mask)

        # Check if less than size, if so break

        if count <= 1:
            raise Exception("Mask too small, probably an error")

        if count <= size_cutoff:
            print("Optimum cutoff is {}".format(cutoff))
            return mask

    raise Exception("Unable to find a mask under cutoff")

if __name__ == "__main__":

    test_array = [[[2, 2, 2], [0, 1, 0], [2, 2, 2]],
                  [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
                  [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]

    test_array = np.array(test_array)

    # list is last index-> first index i.e. lists are along z, lists of lists along y, lists of lists of lists along x

    mask = find_ligand_cluster(test_array, (0,0,0), 5, initial_cutoff=0.5, final_cutoff=1.5)

    print(mask)