from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA

from sklearn.manifold import Isomap
from sklearn.cluster import DBSCAN
from sklearn.ensemble import IsolationForest
import sklearn.svm as svm

import dataset_py3

# import MDAnalysis
# import MDAnalysis.analysis.encore as encore
# from MDAnalysis import Merge
# from MDAnalysis.topology import guessers as guessers
#
# from MDAnalysis.coordinates.memory import MemoryReader

import argparse
import time
import numpy as np

import plots

# import ccp4_io_utils as io

import array_split

# import hdbscan

from scipy.stats import mode





def parse_args():
    parser = argparse.ArgumentParser()

    # Input
    parser.add_argument("-d", "--map_directory_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Settings
    parser.add_argument("-n", "--num_components",
                        default=2)

    # Output

    args = parser.parse_args()

    return args

#
# def cluster_pdbs_MDAnalysis(pdb_paths):
#
#     print("Loading MDAnalysis Universes...")
#     load_start = time.time()
#     universes = [MDAnalysis.Universe(str(pdb_path)).select_atoms("name CA")
#                  for i, pdb_path in enumerate(pdb_paths) if i < 50]
#     load_finish = time.time()
#     print("\tLoaded MDAnalysis Universes in {}".format(load_finish - load_start))
#
#     # protein = universes[0].select_atoms("protein")
#     merge_start = time.time()
#     combined = Merge(universes[1])
#     merge_finish = time.time()
#     print("\tMerged trajectories in {}".format(merge_finish-merge_start))
#     # print("\tTrajectory length is {}".format(len(combined.trajectory)))
#
#     print("Building topology...")
#     top_start = time.time()
#     topology = guessers.guess_bonds(combined.atoms,
#                                     combined.select_atoms("protein").positions,
#                                     vdwradii={"CL":1.6})
#     top_finish = time.time()
#     print("\tBuilt topology in {}".format(top_finish - top_start))
#     print(topology)
#
#
#
#     print("Generating psuedo-trajectory...")
#     coord_array = np.stack([pdb.atoms.positions for pdb in universes])
#
#     combined.load_new(coord_array, format=MemoryReader, order="fac")
#     print("\tNew trajectory of length: {}".format(len(combined.trajectory)))
#
#     print("Clustering PDBs")
#     cluster_start = time.time()
#     clustering = encore.cluster(combined,
#                                 selection="name CA")
#     cluster_finish = time.time()
#     print("\tClustered PDBs in {}".format(cluster_finish-cluster_start))
#
#     return clustering


def cluster_maps(datasets, num_components=2, iter=True, x=10, y=10, z=10, samples="all"):

    output = "/home/zoh22914/pandda-conor/lib-python/ppandda/investigations/map_cluster_plots/"

    reduced_maps = np.zeros((datasets.num_samples, x, y, z))

    # Load in chunk
    for ijk, map_chunks in datasets.iterload_maps(x,y,z):

        # Get modal chunk
        shape_array = np.array([chunk.shape for chunk in map_chunks])
        modal_shape = mode(shape_array).mode
        filtered_map_chunks = [map_chunk for map_chunk in map_chunks if map_chunk.shape == modal_shape]
        print("\tAfter filtering on shape {} chunks remain".format(len(filtered_map_chunks)))
        print([map_chunk.shape for map_chunk in filtered_map_chunks])


        # Perform PCA
        sample_by_feature = np.vstack(map_chunks)

        pca = PCA(n_components=num_components)
        print("Performing PCA on {} components!".format(num_components))
        pca_start = time.time()
        components = pca.fit_transform(sample_by_feature)
        pca_finish = time.time()
        print("\tFinished PCA in {}".format(pca_finish-pca_start))

        # clusterer = DBSAN.HDBSCAN(min_cluster_size=5,
        #                             allow_single_cluster=True).fit(components)

        clusterer = DBSCAN(eps=100, min_samples=5).fit(components)

        plots.save_coloured_scatter(components[:, 0], components[:, 1], clusterer.labels_,
                                    output + "pca_{}_{}_{}.png".format(ijk[0], ijk[1], ijk[2]))

        iso = Isomap(n_components=num_components)
        print("Performing PCA on {} components!".format(num_components))
        pca_start = time.time()
        components = iso.fit_transform(sample_by_feature)
        pca_finish = time.time()
        print("\tFinished isomap in {}".format(pca_finish - pca_start))

        # clusterer = DBSAN.HDBSCAN(min_cluster_size=5,
        #                             allow_single_cluster=True).fit(components)

        clusterer = DBSCAN(eps=100, min_samples=5).fit(components)

        plots.save_coloured_scatter(components[:, 0], components[:, 1], clusterer.labels_,
                                    output + "iso_{}_{}_{}.png".format(ijk[0], ijk[1], ijk[2]))




        exit()



        reduced_maps[:, ijk[0], ijk[1], ijk[2]] == clusterer.labels_

class MapClusterer:

    def __init__(self):

        self.labels_ = None

    def itercluster(self, dataset, num_components=3, x=10, y=10, z=10):


        tables = [(dataset.maps[crystal], apo) for crystal, apo in zip(dataset.df["crystal"], dataset.df["status"])]

        table_shape_mode = mode([array[0].shape for array in tables]).mode

        print("\tBefore truncation there are {} dataasets. Array shape mode is {}. Truncating...".format(len(tables), str(np.array(mode))))

        tables = [table for table in tables if np.array_equal(np.array(table[0].shape).reshape(-1,), np.array(table_shape_mode).reshape(-1,))]

        print("\t\tAfter truncating {} datasets remain.".format(len(tables)))
        print([table[1] for table in tables])

        partitioning = array_split.shape_split(tables[0][0].shape,
                                               axis=[x, y, z])

        print("Setting up iteration...")

        for idx, chunk_indicies in np.ndenumerate(partitioning):

            output = "/home/zoh22914/pandda-conor/lib-python/ppandda/investigations/map_cluster_plots/"


            # print(chunk)
            # print(tables[0][chunk[0], chunk[1], chunk[2]])

            print("\tWorking on chunk {}".format(idx))

            load_data_start = time.time()
            map_chunks_list = [ed_map[0][chunk_indicies[0],
                                      chunk_indicies[1],
                                      chunk_indicies[2]].flatten().reshape(1,-1) for ed_map in tables]
            load_data_finish = time.time()
            print("\tLoaded data in {}".format(load_data_finish-load_data_start))

            sample_by_feature = np.vstack(map_chunks_list)
            print("\tShape of sample by feature array is  {}".format(sample_by_feature.shape))

            print("\tPerforming PCA on {} components!".format(num_components))
            pca_start = time.time()
            pca = PCA(n_components=num_components)
            components = pca.fit_transform(sample_by_feature)
            pca_finish = time.time()
            print("\t\tFinished PCA in {}".format(pca_finish - pca_start))
            #
            # print(components.shape)
            # print(components[0,:])




            iso_start = time.time()
            iso = IsolationForest(behaviour="new",
                                  # max_samples=1000,
                                  contamination=0.03)
            outlier = iso.fit(components).predict(components)
            iso_finish = time.time()
            print("\tPerformed outleir detection in {}".format(iso_finish- iso_start))


            plots.save_coloured_scatter(components[:, 0], components[:, 1], outlier,#  [table[1] for table in tables],
                                        output + "chunk_{}_{}_{}_pca.png".format(idx[0], idx[1], idx[2]))


            print("\tPerforming PCA WITHOUT OUTLIERS on {} components!".format(num_components))
            pca_start = time.time()
            pca = PCA(n_components=num_components)
            components = pca.fit(sample_by_feature[outlier == 1]).transform(sample_by_feature[outlier == 1])
            pca_finish = time.time()
            print("\t\tFinished PCA in {}".format(pca_finish - pca_start))


            # detector = svm.OneClassSVM(nu=0.05, kernel="rbf", gamma="auto")
            #
            # outlier = detector.fit(components).predict(components)


            statuses = np.array([table[1] for table in tables])

            clusterer = DBSCAN(eps=6, min_samples=5).fit(components)


            plots.save_coloured_scatter(components[:, 0], components[:, 1],  statuses[outlier == 1],
                                        output + "chunk_{}_{}_{}_pca_no_outliers.png".format(idx[0], idx[1], idx[2]))

            # print("Performing PCA on {} components!".format(num_components))
            # pca_start = time.time()
            # iso = Isomap(n_components=num_components)
            # components = iso.fit_transform(sample_by_feature)
            # pca_finish = time.time()
            # print("\tFinished isomap in {}".format(pca_finish - pca_start))
            #
            # print(components.shape)
            # print(components[0, :])
            #
            # clusterer = DBSCAN(eps=6, min_samples=5).fit(components)
            #
            # output = "/home/zoh22914/pandda-conor/lib-python/ppandda/investigations/map_cluster_plots/"
            #
            # plots.save_coloured_scatter(components[:, 0], components[:, 1], clusterer.labels_,
            #                             output + "chunk_{}_{}_{}_isomap.png".format(idx[0], idx[1], idx[2]))
            #
            # print("Performing ICA on {} components!".format(num_components))
            # pca_start = time.time()
            # ica = FastICA(n_components=num_components)
            # components = ica.fit_transform(sample_by_feature)
            # pca_finish = time.time()
            # print("\tFinished ica in {}".format(pca_finish - pca_start))
            #
            # print(components.shape)
            # print(components[0, :])
            #
            # clusterer = DBSCAN(eps=6, min_samples=5).fit(components)
            #
            # output = "/home/zoh22914/pandda-conor/lib-python/ppandda/investigations/map_cluster_plots/"
            #
            # plots.save_coloured_scatter(components[:, 0], components[:, 1], [table[1] for table in tables],
            #                             output + "chunk_{}_{}_{}_ica.png".format(idx[0], idx[1], idx[2]))







if __name__ == "__main__":

    args = parse_args()

    dataset = dataset_py3.Dataset(directory=args.map_directory_path,num_samples="all")
    # dataset.generate_ccp4_maps_from_mtzs(, overwrite=False)
    # dataset.generate_np_maps_from_ccp4s(overwrite=False)
    #
    # dataset.load_np_maps(mmap=True,
    #                      exclude_different_sizes=True)

    dataset.get_status()

    # exit()

    dataset.generate_hdf5(args.map_directory_path + "/map_store.hdf5")

    dataset.load_hdf5(args.map_directory_path + "/map_store.hdf5")

    print(dataset.maps.keys())

    map_cluster = MapClusterer().itercluster(dataset)

    clustering = map_cluster.labels_

    # for ijk, map_chunks in dataset.iterload():
    #
    #     map_cluster = MapClusterer().cluster(map_chunks)
    #
    #     clustering = map_cluster.labels_


    #
    #
    #
    # # Load mtz
    # datasets.load_mtzs()
    #
    # # Cluster maps
    # map_clusters = cluster_maps(datasets)
    #
    # #
    # for map_cluster in map_clusters:
    #     pdb_clusters = cluster_pdbs_biopandas(map_cluster)
    #     central_pdb_cluster = exclude_outliers(pdb_clusters)
    #     centroid = get_centroid(map_cluster, central_pdb_cluster)
    #
