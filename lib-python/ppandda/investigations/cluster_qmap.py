from sklearn.cluster import DBSCAN
import glob
import argparse
import itertools
import os

import numpy as np

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--map_path")

    args = parser.parse_args()

    return args


def to_sample_by_feature(dense):

    it = np.nditer(dense, flags=['multi_index'])

    rows = []

    while not it.finished:

        if it[0] < 0.99:
            it.iternext()
            continue

        row = [it.multi_index[0], it.multi_index[1], it.multi_index[2], it[0]]
        rows.append(row)
        it.iternext()

    sample_by_feature = np.array(rows)

    return sample_by_feature

def to_dense(sample_by_feature, dimensions):

    dense = np.zeros(dimensions)

    for sample in sample_by_feature:

        dense[int(sample[0,0]), int(sample[0,1]), int(sample[0,2])] = sample[0,3]

    return dense

if __name__ == "__main__":

    args = parse_args()

    # Load datasets
    home = "/home/zoh22914/"
    base_python = "python"
    python_interpreter = home + "myccp4python"
    np2ccp4 = home + "pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py"
    ccp42np = home + "pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py"
    reference_map = args.map_path

    os.popen("{} {} -m {}".format(python_interpreter, ccp42np, reference_map))

    array = np.load("{}.npy".format(reference_map))

    print("Loaded array of shape {}".format(array.shape))

    # Convert to sample by feature
    sample_by_feature = to_sample_by_feature(array)
    print("Converted to sample by feature array of shape {}".format(sample_by_feature.shape))

    # Perform Clustering
    print("Clustering")
    clustering = DBSCAN(eps=3, min_samples=80).fit(sample_by_feature)

    # Generate clusters
    cluster_nums = np.unique(clustering.labels_)
    print("Clusters are {}".format(cluster_nums))


    # Plot distribution of cluster sizes
    cluster_sizes = []

    for i, cluster_num in enumerate(cluster_nums):
        sub_array = clustering.labels_[clustering.labels_ == cluster_num]
        cluster_sizes.append(len(sub_array))

    print("Cluster sizes are {}".format(cluster_sizes))
    fig, ax = plt.subplots()
    fig.set_size_inches(45, 15)
    ax.hist(cluster_sizes[1:], bins=20)
    fig.savefig("DBSCAN_cluster_sizes.png")

    sorted_size_args = np.argsort(cluster_sizes)

    largest_indicies = sorted_size_args[len(sorted_size_args)-10:]

    print("The indicies of the largest clusters are {}".format(largest_indicies))

    largest_clusters = cluster_nums[largest_indicies]
    largest_cluster_sizes = np.array(cluster_sizes)[largest_indicies]

    print("Top five clusters are {}".format(largest_clusters))
    print("They have sizes {}".format(largest_cluster_sizes))


    # Output masks
    for i, cluster_num in enumerate(largest_clusters):

        print("Cluster num is {}".format(cluster_num))

        if cluster_num == -1: continue

        mask = np.zeros(array.shape)
        indexes = np.argwhere(clustering.labels_ == cluster_num)

        print("Cluster size is {}".format(len(indexes)))

        reduced_sample_by_feature = sample_by_feature[indexes]

        mask = to_dense(reduced_sample_by_feature, array.shape)

        print("Number of non zero elements is {}".format(np.count_nonzero(mask)))

        np.save("cluster_{}.npy".format(cluster_num), mask)
        os.popen("{} {} -a cluster_{}.npy -m {}".format(python_interpreter, np2ccp4, cluster_num, args.map_path))
