import argparse
import os
from shutil import rmtree
import re
from subprocess import PIPE, Popen
import time

import numpy as np
from scipy.optimize import basinhopping, brute, differential_evolution
import pandas as pd
import sqlalchemy

from biopandas.pdb import PandasPdb

from rdkit import Chem
from rdkit.Chem import AllChem

from sklearn.cluster import DBSCAN

from qfit_ligand.validator import Validator
from qfit_ligand.volume import Volume
from qfit_ligand.structure import Structure


# Panddas optoins

pd.options.display.max_columns = 20

pd.options.display.max_colwidth = 50


def parse_args():
    parser = argparse.ArgumentParser()

    # PanDDA run
    parser.add_argument("-r", "--pandda_run", default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Output
    parser.add_argument("-o", "--output", default="mask2/")

    # Database
    parser.add_argument("-hs", "--host", default="172.23.142.43")
    parser.add_argument("-p", "--port", default="5432")
    parser.add_argument("-d", "--database", default="test_xchem")
    parser.add_argument("-u", "--user", default="conor")
    parser.add_argument("-pw", "--password", default="c0n0r")

    # QFit parameters
    parser.add_argument("-s", "--angular_step", default=6, type=int)
    parser.add_argument("-b", "--degrees_freedom", default=2, type=int)

    # Interpeter args
    parser.add_argument("-cpy", "--ccp4_python", default="/home/zoh22914/myccp4python")


    args = parser.parse_args()

    return args


def to_sample_by_feature(dense):

    it = np.nditer(dense, flags=['multi_index'])

    rows = []

    while not it.finished:

        if it[0] < 0.99:
            it.iternext()
            continue

        row = [it.multi_index[0], it.multi_index[1], it.multi_index[2], it[0]]
        rows.append(row)
        it.iternext()

    sample_by_feature = np.array(rows)

    return sample_by_feature


def to_dense(sample_by_feature, dimensions):

    dense = np.zeros(dimensions)

    for sample in sample_by_feature:

        dense[int(sample[0,0]), int(sample[0,1]), int(sample[0,2])] = sample[0,3]

    return dense



def rotate(sample_by_feature, x, y, z):

    mean_x = np.mean(sample_by_feature[:, 0])
    mean_y = np.mean(sample_by_feature[:, 1])
    mean_z = np.mean(sample_by_feature[:, 2])

    de_meaned_x = sample_by_feature[:, 0] - mean_x
    de_meaned_y = sample_by_feature[:, 1] - mean_y
    de_meaned_z = sample_by_feature[:, 2] - mean_z

    sample_by_feature[:, 0] = de_meaned_x
    sample_by_feature[:, 1] = de_meaned_y
    sample_by_feature[:, 2] = de_meaned_z

    sx = np.sin(x)
    cx = np.cos(x)
    sy = np.sin(y)
    cy = np.cos(y)
    sz = np.sin(z)
    cz = np.cos(z)

    rx = [[1, 0, 0],
           [0, cx, sx],
           [0, -sx, cx]]
    ry = [[cy, 0, -sy],
           [0, 1, 0],
           [sy, 0, cy]]
    rz = [[cz, sz, 0],
           [-sz, cz, 0],
           [0, 0, 1]]

    rotated_x = np.matmul(rx, sample_by_feature.T)
    rotated_y = np.matmul(ry, rotated_x)
    rotated_z = np.matmul(rz, rotated_y)

    rotated_matrix = rotated_z.T

    rotated_matrix[:, 0] = rotated_matrix[:, 0] + mean_x
    rotated_matrix[:, 1] = rotated_matrix[:, 1] + mean_y
    rotated_matrix[:, 2] = rotated_matrix[:, 2] + mean_z


    return rotated_matrix


def recentre(ligand, centroid):

    ligand_mean = [ligand.df['HETATM']["x_coord"].mean(),
                   ligand.df['HETATM']["y_coord"].mean(),
                   ligand.df['HETATM']["z_coord"].mean()]

    x_mean_diff = ligand_mean[0] - centroid[0]
    y_mean_diff = ligand_mean[1] - centroid[1]
    z_mean_diff = ligand_mean[2] - centroid[2]
    print("vector is ({},{},{})".format(x_mean_diff, y_mean_diff, z_mean_diff))

    centered_x = ligand.df['HETATM']["x_coord"] - x_mean_diff
    centered_y = ligand.df['HETATM']["y_coord"] - y_mean_diff
    centered_z = ligand.df['HETATM']["z_coord"] - z_mean_diff

    ligand.df['HETATM']["x_coord"] = centered_x
    ligand.df['HETATM']["y_coord"] = centered_y
    ligand.df['HETATM']["z_coord"] = centered_z

    return ligand, ligand_mean


def load_map(map_path, output_path):
    os.popen("{} {} -m {} -o {}".format(ccp4_python, ccp42np, target_map_path, map_dir))

    numpy_map = np.load(target_map_path + ".npy")

    return numpy_map

def save_map(target_map, template, output, python):

    np.save(output, target_map)

    os.popen("{} {} -a {} -m {}".format(python, np2ccp4, output, template))


def generate_bdc_map(bdc, target_map, mean_map, template, output, python):

    bdc_map = (target_map - bdc*mean_map)/(1-bdc)

    np.save(bdc_map_path, bdc_map)

    os.popen("{} {} -a {} -m {}".format(python, np2ccp4, output, template))

    return bdc_map


def translate(sample_by_feature, x, y, z):

    sample_by_feature[:, 0] = sample_by_feature[:, 0] + x
    sample_by_feature[:, 1] = sample_by_feature[:, 1] + y
    sample_by_feature[:, 2] = sample_by_feature[:, 2] + z

    return sample_by_feature


def gen_embedding(x, embedding_coords):
    # Extract
    sample_by_feature = embedding_coords.as_matrix(columns=["x_coord", "y_coord", "z_coord"])

    # Rotate
    rot = [x[3], x[4], x[5]]
    rotated = rotate(sample_by_feature, rot[0], rot[1], rot[2])
    # print("Rotated x: ({}), y: ({}), z: ({})".format(rot[0], rot[1], rot[2]))

    # Translate
    trans = [x[0], x[1], x[2]]
    translated = translate(rotated, trans[0], trans[1], trans[2])
    # print("Translated x: ({}), y: ({}), z: ({})".format(trans[0], trans[1], trans[2]))

    # Update
    embedding_coords["x_coord"] = translated[:, 0]
    embedding_coords["y_coord"] = translated[:, 1]
    embedding_coords["z_coord"] = translated[:, 2]

    return embedding_coords


def check_clash(embedding, receptor, centorid):
    # print(embedding)
    # print(receptor)
    # print(centorid)
    distances = receptor.distance(xyz=centorid, records='ATOM')
    all_within_7A = receptor.df['ATOM'][distances < 10.0]

    clashing = 0

    for atom in embedding.df["HETATM"].itertuples():

        distances = receptor.distance_df(all_within_7A,
                                         xyz=(atom.x_coord, atom.y_coord, atom.z_coord))

        # print(distances)

        if distances[distances < 1].any():
            # print(distances[distances < 0.9])
            # print(atom.x_coord, atom.y_coord, atom.z_coord)
            # print("CLASHING!")
            clashing = clashing + 1

    print("Num CLashes: {}".format(clashing))

    return clashing


def get_rscc(embedding, structure_dir, xmap):
    # Output  model
    pandas_embedding_path = structure_dir + "pandas_embedding.pdb"
    embedding.to_pdb(pandas_embedding_path)

    # Calculate RSCC
    structure_ligand = Structure.fromfile(pandas_embedding_path)

    structure_ligand.b = structure_ligand.b + 20.0

    validator = Validator(xmap, float(resolution))
    rscc = validator.rscc(structure_ligand)

    print("RSCC is {}".format(rscc))

    return rscc


def get_mask_score(embedding_coords, volume):

    alpha, beta, gamma = np.deg2rad(volume.angles)
    a, b, c = volume.lattice_parameters
    cos_gamma = np.cos(gamma)
    sin_gamma = np.sin(gamma)
    cos_alpha = np.cos(alpha)
    cos_beta = np.cos(beta)
    omega = np.sqrt(
        1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
        2 * cos_alpha * cos_beta * cos_gamma
    )


    cartesian_to_lattice = np.asarray([
        [1, -cos_gamma / sin_gamma,
         (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
        [0, 1 / sin_gamma,
         (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
        [0, 0,
         sin_gamma / omega],
    ])

    sample_by_feature = embedding_coords.as_matrix(columns=["x_coord", "y_coord", "z_coord"])
    #
    # tiled = np.tile(volume.origin, (20, 1)).T
    #
    # adjusted_coords = sample_by_feature.T - tiled
    #
    # lattice_coords = np.matmul(cartesian_to_lattice.T, adjusted_coords)

    mask = np.zeros(volume.array.shape)

    # lattice_coords = lattice_coords/0.5
    # lattice_coords = lattice_coords.T - volume.offset
    # print(volume.offset)
    # print(volume.origin)

    for coords in sample_by_feature:
        # print(coords, coords.shape)
        shifted = coords - volume.origin
        tilted = np.matmul(cartesian_to_lattice, shifted)
        stretched = tilted/0.5

        # print(stretched)
        # x = int(stretched[0]) -2
        # y = int(stretched[1]) -1
        # z = int(stretched[2]) + 10

        x = int(round(stretched[0])) +2
        y = int(round(stretched[1])) -2
        z = int(round(stretched[2])) +2

        step = 1

        if step == 0:

            mask[x, y, z] = 1

        else:

            mask[x - step:x + step, y - step:y + step, z - step:z + step] = 1

    # print(cartesian_to_lattice)

    # print("Masked: {} voxels".format(np.sum(mask)))
    # print("Volume array voxels past cutoff: {} voxels".format(np.sum((volume.array > 1.5))))

    mask_score = np.sum(mask[volume.array > 0.9])/np.sum(mask)

    # print("mask score is: {}".format(mask_score))

    # return mask_score, mask

    return mask_score



def get_mask(embedding, volume):

    alpha, beta, gamma = np.deg2rad(volume.angles)
    a, b, c = volume.lattice_parameters
    cos_gamma = np.cos(gamma)
    sin_gamma = np.sin(gamma)
    cos_alpha = np.cos(alpha)
    cos_beta = np.cos(beta)
    omega = np.sqrt(
        1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
        2 * cos_alpha * cos_beta * cos_gamma
    )


    cartesian_to_lattice = np.asarray([
        [1, -cos_gamma / sin_gamma,
         (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
        [0, 1 / sin_gamma,
         (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
        [0, 0,
         sin_gamma / omega],
    ])

    sample_by_feature = embedding.df["HETATM"].as_matrix(columns=["x_coord", "y_coord", "z_coord"])
    #
    # tiled = np.tile(volume.origin, (20, 1)).T
    #
    # adjusted_coords = sample_by_feature.T - tiled
    #
    # lattice_coords = np.matmul(cartesian_to_lattice.T, adjusted_coords)

    mask = np.zeros(volume.array.shape)

    # lattice_coords = lattice_coords/0.5
    # lattice_coords = lattice_coords.T - volume.offset
    # print(volume.offset)
    # print(volume.origin)

    for coords in sample_by_feature:
        # print(coords, coords.shape)
        shifted = coords - volume.origin
        tilted = np.matmul(cartesian_to_lattice, shifted)
        stretched = tilted/0.5

        # print(stretched)
        # x = int(stretched[0]) -2
        # y = int(stretched[1]) -1
        # z = int(stretched[2]) + 10

        x = int(round(stretched[0])) +2
        y = int(round(stretched[1])) -2
        z = int(round(stretched[2])) +2

        step = 2

        mask[x-step:x+step, y-step:y+step, z-step:z+step] = 1

    # print(cartesian_to_lattice)

    print("Masked: {} voxels".format(np.sum(mask)))
    print("Volume array voxels past cutoff: {} voxels".format(np.sum((volume.array > 0.9))))

    mask_score = np.sum(mask[volume.array > 0.9])/np.sum(mask)

    print("mask score is: {}".format(mask_score))

    # return mask_score, mask

    return mask


def score_conformer(x, embedding, receptor, centorid, xmap, structure_dir, coordinates):

    embedding_coords = embedding.df["HETATM"].copy()
    # print(type(embedding_coords))

    new_embedding_coords = gen_embedding(x, embedding_coords)

    # clash = check_clash(embedding, receptor, centorid)
    clash = 0

    # rscc = get_rscc(embedding, structure_dir, xmap)

    score = get_mask_score(new_embedding_coords , xmap)

    # print("New ligand mean is ({}, {}, {})".format(embedding.df["HETATM"]["x_coord"].mean(),
    #                                                embedding.df["HETATM"]["y_coord"].mean(),
    #                                                embedding.df["HETATM"]["z_coord"].mean()))

    # embedding.df["HETATM"]["x_coord"] = coordinates[0]
    # embedding.df["HETATM"]["y_coord"] = coordinates[1]
    # embedding.df["HETATM"]["z_coord"] = coordinates[2]
    # final_score = (1 + clash) - score
    if clash > 0:
        final_score = 2 - score
    else:
        final_score = 1-score

    # print("Conformer score is {}".format(final_score))

    return final_score






if __name__ == "__main__":

    args = parse_args()

    # Establish python vars
    home = "/home/zoh22914/"
    base_python = "python"
    ccp4_python = home + "myccp4python"
    np2ccp4 = home + "pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py"
    ccp42np = home + "pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py"
    output = args.pandda_run + "/" + args.output

    # Prepare output dir

    def prepare_output(output):

        try:
            os.mkdir(output)

        except:
            rmtree(output)
            os.mkdir(output)

        qfit_dir = output + "qfit/"
        os.mkdir(qfit_dir)


        structure_dir = output + "structures/"
        os.mkdir(structure_dir)

        map_dir = output + "maps/"
        os.mkdir(map_dir)

        return qfit_dir, structure_dir, map_dir

    qfit_dir, structure_dir, map_dir = prepare_output(output)


    # Connect to database

    engine=sqlalchemy.create_engine("postgresql://{}:{}@{}:{}/{}".format(args.user, args.password, args.host, args.port, args.database))

    pandda_analysis = pd.read_sql_query("SELECT * FROM pandda_analysis", con=engine)

    pandda_run = pd.read_sql_query("SELECT * FROM pandda_run", con=engine)

    pandda_events = pd.read_sql_query("SELECT * FROM pandda_event", con=engine)

    pandda_events_stats = pd.read_sql_query("SELECT * FROM pandda_event_stats", con=engine)

    statistical_maps = pd.read_sql_query("SELECT * FROM pandda_statistical_map", con=engine)

    crystals = pd.read_sql_query("SELECT * FROM crystal", con=engine)

    ligands = pd.read_sql_query("SELECT * FROM proasis_hits", con=engine)

    ligand_stats = pd.read_sql_query("SELECT * FROM ligand_edstats", con=engine)

    data_processing = pd.read_sql_query("SELECT * FROM data_processing", con=engine)

    # Query database

    # TODO: Un-hard-code this all
    analysis_id = pandda_analysis[pandda_analysis["pandda_dir"] == args.pandda_run]["id"].iloc[0]
    print("Pandda analysis id is {}".format(analysis_id))

    run_id = pandda_run[pandda_run["pandda_analysis_id"] == analysis_id]["id"].iloc[-1]
    print("PanDDA run id is {}".format(run_id))

    crystal_id = crystals[crystals["crystal_name"] == "PDK2-x0513"]["id"].iloc[0]
    print("Crystal id is {}".format(crystal_id))


    statistical_map = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/PDK2-x0128-ground-state-mean-map.native.ccp4"

    print("PanDDA statistical map path is {}".format(statistical_map))

    print(pandda_events[pandda_events["crystal_id"] == crystal_id])

    # pandda_event_id = pandda_events[(pandda_events["crystal_id"] == crystal_id)
    #                                 & (pandda_events["pandda_run_id"] == run_id)
    #                                 & (pandda_events["event"] == 1)]["id"].iloc[0]
    # print("pandda_event id is {}".format(pandda_event_id))


    # TODO : Un-hard code this
    # statistical_map_path = run_id["pandda_dir"].loc[0]
    # print(statistical_map_path)

    # Load maps
    # mtz_path = pandda_events[pandda_events["id"] == pandda_event_id]["pandda_input_mtz"].iloc[0]
    mtz_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/PDK2-x0128-pandda-input.mtz"
    print("MTZ path is {}. Converting to ccp4.".format(mtz_path))
    mean_map_path = statistical_map
    os.popen("phenix.mtz2map {} directory={} grid_resolution_factor=0.166666".format(mtz_path, map_dir))

    target_map_path = map_dir + os.path.splitext(os.path.basename(mtz_path))[0] + "_2mFo-DFc.ccp4"
    numpy_map = load_map(target_map_path, map_dir)
    print("Loaded 2mFo-DFc arrap of shape {}".format(numpy_map.shape))

    numpy_mean_map = load_map(statistical_map, map_dir)
    print("Loaded mean array of shape {}".format(numpy_mean_map.shape))

    # TODO: Get event stats working so this can be used
    # Generate BDC map - pipeline isn't working
    # print(pandda_events_stats)
    # bdc = pandda_events_stats[pandda_events_stats["event_id"] == pandda_event_id]["one_minus_bdc"].iloc[0]
    bdc = 0.26
    print("BDC factor is {}".format(bdc))

    bdc_map_path = map_dir + "bdc_map.npy"
    bdc_map = generate_bdc_map(bdc, numpy_map, numpy_mean_map, statistical_map, bdc_map_path, ccp4_python)
    print("Saved bdc map")

    # Get centroid
    # centorid = [pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_x"].iloc[0],
    #             pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_y"].iloc[0],
    #             pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_z"].iloc[0]]
    centorid = [32.0, -36.0, 12.0]
    print("cetroid is ({},{},{})".format(centorid[0], centorid[1], centorid[2]))

    # Load PDBS
    # ligand_pdb_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/ligand_files/VER-00160616_with_H.pdb"
    ligand_smiles_path = "COc1ccc(cc1)N(C)C(=O)c1ccc(O)cc1O"
    # ligand_cif_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/ligand_files/VER-00160616.cif"

    print("ligand pdb path is {}".format( ligand_smiles_path))
    # ligand = Chem.MolFromPDBFile(ligand_pdb_path)
    ligand = Chem.MolFromSmiles(ligand_smiles_path)

    # Sample
    # resolution = data_processing[data_processing["crystal_name_id"] == crystal_id]["res_high"].iloc[0]
    resolution = 3.02
    # receptor = pandda_events[pandda_events["id"] == pandda_event_id]["pandda_input_pdb"].iloc[0]
    receptor = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/PDK2-x0128-pandda-input.pdb"
    receptor_pdb = PandasPdb().read_pdb(receptor)


    print("Dataset resolution is {}".format(resolution))
    print("Path to receptor is {}".format(receptor))

    embeddings = AllChem.EmbedMultipleConfs(ligand,
                                            clearConfs=True,
                                            numConfs=100,
                                            pruneRmsThresh=1)

    print("Found ({}) distinct embeddings".format(len(embeddings)))

    volume_xmap = Volume.fromfile(statistical_map)

    volume_xmap.array = bdc_map

    def cluster_mean(i, sample_by_feature, labels):
        cluster = sample_by_feature[labels == i]
        print(cluster)
        x_mean = np.mean(cluster[:, 0])
        y_mean = np.mean(cluster[:, 1])
        z_mean = np.mean(cluster[:, 2])

        # mean = np.array([x_mean, y_mean, z_mean])



        return [x_mean, y_mean, z_mean]

    # CLUSTER
    print("Clustering")
    print(volume_xmap.array.shape)
    masked_array = np.zeros(volume_xmap.array.shape)
    masked_array[volume_xmap.array > 2] = 1
    print(masked_array.shape)
    sample_by_feature = to_sample_by_feature(masked_array)
    print(sample_by_feature.shape)
    clustering = DBSCAN(eps=1.5, min_samples=9).fit(sample_by_feature)
    cluster_info = [(i, len(sample_by_feature[clustering.labels_ == i]), cluster_mean(i, sample_by_feature, clustering.labels_)) for i in np.unique(clustering.labels_)]
    print(cluster_info)


    alpha, beta, gamma = np.deg2rad(volume_xmap.angles)
    a, b, c = volume_xmap.lattice_parameters
    cos_gamma = np.cos(gamma)
    sin_gamma = np.sin(gamma)
    cos_alpha = np.cos(alpha)
    cos_beta = np.cos(beta)
    omega = np.sqrt(
        1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
        2 * cos_alpha * cos_beta * cos_gamma
    )

    cartesian_to_lattice = np.asarray([
        [1, -cos_gamma / sin_gamma,
         (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
        [0, 1 / sin_gamma,
         (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
        [0, 0,
         sin_gamma / omega],
    ])

    lattice_to_cartesian = np.asarray([
        [1, cos_gamma, cos_beta],
        [0, sin_gamma, (cos_alpha - cos_beta * cos_gamma) / sin_gamma],
        [0, 0, omega / sin_gamma],
    ])


    shifted = np.array(centorid) - volume_xmap.origin
    tilted = np.matmul(cartesian_to_lattice, shifted)
    stretched = tilted / 0.5
    stretched_modulo = np.mod(stretched, volume_xmap.array.shape)

    print("Lattice coordinates of event are {}".format(stretched_modulo))


    distances = [np.linalg.norm(np.array(cluster[2]) - stretched_modulo) for cluster in cluster_info]

    closest = np.argmin(distances)

    closest_cluster = cluster_info[closest]

    print(closest_cluster)

    indexes = np.argwhere(clustering.labels_ == closest_cluster[0])

    closest_cluster_array = to_dense(sample_by_feature[indexes], masked_array.shape)

    print("lattice_coordinates of closest cluster is {}".format(closest_cluster[2]))

    lattice_distance = np.array(closest_cluster[2]) - stretched_modulo
    print("lattice distance between clusters is {}".format(lattice_distance))

    lattice_distance_unstretched = np.array(lattice_distance) * 0.5
    lattice_distance_untilted = np.matmul(lattice_to_cartesian, lattice_distance_unstretched )
    print("Corresponding cartesian distance is {}".format(lattice_distance_untilted))

    # print(centorid)
    # print(volume_xmap.array.shape)
    # print(cluster[2])
    # print(-(0.5)*np.array(volume_xmap.array.shape))
    # #
    # # # cluster_modded = -(0.5)*np.array(volume_xmap.array.shape) + np.mod(np.array(cluster[2]), volume_xmap.array.shape)
    # # cluster_modded = cluster[2]
    # #
    # # cluster_centre_unstretched = np.array(cluster_modded)*0.5
    # # cluster_centre_untilted = np.matmul(lattice_to_cartesian, cluster_centre_unstretched)
    # # cluster_centre_unshifted = cluster_centre_untilted + volume_xmap.origin
    # #
    # # print(cluster_centre_unshifted)
    # #
    # # exit()





    new_array = np.zeros(closest_cluster_array.shape)

    non_zero = np.nonzero(closest_cluster_array)

    step = 2

    for i in range(len(non_zero[0])):
        new_array[non_zero[0][i]-step:non_zero[0][i]+step,
        non_zero[1][i] - step:non_zero[1][i] + step,
        non_zero[2][i] - step:non_zero[2][i] + step] = 1

    save_map(new_array, statistical_map, map_dir + "cluster.npy", ccp4_python)

    volume_xmap.array = new_array

    rscc_current_max = 0

    for embedding_id in embeddings:
        print("working on embedding: {}".format(embedding_id))
        # Optimise
        print("Optimising conformer geometry")
        AllChem.MMFFOptimizeMolecule(ligand, confId=embedding_id)

        # Save to pdb
        Chem.MolToPDBFile(ligand, "embedding.pdb", confId=embedding_id)

        # Open
        print("Reading embedding")
        pandas_embedding = PandasPdb().read_pdb("embedding.pdb")
        # embedding_copy = pandas_embedding.copy()

        # recentre
        pandas_embedding, ligand_mean = recentre(pandas_embedding, np.array(centorid) + lattice_distance_untilted)
        coordinates = [pandas_embedding.df['HETATM']["x_coord"].copy(),
                       pandas_embedding.df['HETATM']["y_coord"].copy(),
                       pandas_embedding.df['HETATM']["z_coord"].copy()]

        print("Inital guess for mean is ({}, {}, {})".format(coordinates[0].mean(), coordinates[1].mean(), coordinates[2].mean()))


        initial_guess = np.array([0, 0, 0, 0, 0, 0])

        func = lambda x: score_conformer(x, pandas_embedding, receptor_pdb, centorid, volume_xmap, structure_dir,
                                         coordinates)


        bounds = [(-2, 2),
                  (-2, 2),
                  (-2, 2),
                  (0, 6.3),
                  (0, 6.3),
                  (0, 6.3)]

        start = time.time()

        result = differential_evolution(func, bounds, maxiter=50, disp=True)
        # result = basinhopping(func, initial_guess, stepsize=0.35, T=1.0, niter=300)


        finish = time.time()

        print(result)
        print("Completed in {} seconds".format(finish-start))

        conformer = gen_embedding(result.x, pandas_embedding.df["HETATM"])

        pandas_embedding.df["HETATM"] = conformer

        print("Final conformer centre at ({}, {}, {})".format(conformer["x_coord"].mean(),
                                                    conformer["y_coord"].mean(),
                                                    conformer["z_coord"].mean()))

        conf_embedding_path = structure_dir + "{}_{}.pdb".format(embedding_id, 1-result.fun)
        pandas_embedding.to_pdb(conf_embedding_path)

        mask_map = get_mask(pandas_embedding, volume_xmap)

        np.save(map_dir + "xmap.npy", mask_map)

        process = Popen("{} {} -a {} -m {}".format(ccp4_python, np2ccp4, map_dir + "xmap.npy", statistical_map),
                        shell=True,
                        stdout=PIPE)

        # Reset




