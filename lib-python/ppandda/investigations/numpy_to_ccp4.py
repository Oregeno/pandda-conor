import argparse
import numpy as np

from iotbx.ccp4_map import write_ccp4_map
from iotbx.file_reader import any_file
from scitbx.array_family import flex
from cctbx.sgtbx import space_group_info
import scitbx

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--array_path")
    parser.add_argument("-m", "--map_path")

    args = parser.parse_args()

    return args


if __name__ == "__main__":

    # Parse args
    args = parse_args()

    # Load array
    np_array = np.load(args.array_path)

    print("The numpy array is of shape ", np_array.shape)

    # load map
    ccp4_map = any_file(args.map_path).file_object

    # convert
    flex_array = flex.double(np_array).as_double()

    print("Flex array is of shape {}".format(flex_array.accessor().all()))
    print(type(flex_array))
    print(type(ccp4_map.map_data()))
    print(ccp4_map.show_summary())

    # Save
    write_ccp4_map(file_name=args.array_path + ".ccp4",
                   unit_cell=ccp4_map.unit_cell(),
                   space_group=space_group_info(number=ccp4_map.space_group_number).group(),
                   map_data=flex_array,
                   #map_data=ccp4_map.map_data(),
                   labels=scitbx.array_family.flex.std_string(['Output map']))

    # Feedback
    print("Successfully wrote array of shape {} to {}".format(np_array.shape, args.array_path + ".ccp4"))


