import pandas as pd
import sqlalchemy

pd.options.display.max_columns = 20

pd.options.display.max_colwidth = 150

pd.options.display.width = 2000

def bar():
    print("##############################################################################################")

# Database
host ="172.23.142.43"
port ="5432"
database ="test_xchem"
user ="conor"
password ="c0n0r"

engine = sqlalchemy.create_engine(
    "postgresql://{}:{}@{}:{}/{}".format(user, password, host, port, database))

pandda_analyses = pd.read_sql_query("SELECT * FROM pandda_analysis", con=engine)

pandda_runs = pd.read_sql_query("SELECT * FROM pandda_run", con=engine)

pandda_events = pd.read_sql_query("SELECT * FROM pandda_event", con=engine)

pandda_events_stats = pd.read_sql_query("SELECT * FROM pandda_event_stats", con=engine)

statistical_maps = pd.read_sql_query("SELECT * FROM pandda_statistical_map", con=engine)

crystals = pd.read_sql_query("SELECT * FROM crystal", con=engine)

ligands = pd.read_sql_query("SELECT * FROM proasis_hits", con=engine)

ligand_stats = pd.read_sql_query("SELECT * FROM ligand_edstats", con=engine)

data_processing = pd.read_sql_query("SELECT * FROM data_processing", con=engine)

proasis_pandda = pd.read_sql_query("SELECT * FROM proasis_pandda", con=engine)


pandda_run = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso"

pandda_analysis_record = pandda_analyses.iloc[0]
print("Pandda analysis record is\n {}\n\n".format(pandda_analysis_record))

bar()

pandda_run_record = pandda_runs.iloc[0]
print("Pandda run record is\n {}\n\n".format(pandda_run_record))

bar()

pandda_event_record = pandda_events.iloc[0]
print("Pandda event record is\n {}\n\n".format(pandda_event_record))

bar()

pandda_event_stats_record = pandda_events_stats.iloc[0]
print("Pandda event stats record is\n {}\n\n".format(pandda_event_stats_record))

bar()

# statistical_maps_record = statistical_maps.iloc[0]
# print("Pandda statistical maps record is\n {}\n\n".format(statistical_maps_record))

bar()

crystals_record = crystals.iloc[0]
print("Pandda crystals record is\n {}\n\n".format(crystals_record))

bar()

ligands_record = ligands.iloc[0]
print("Pandda ligands record is\n {}\n\n".format(ligands_record))

bar()

# ligand_stats_record = ligand_stats.iloc[0]
# print("Pandda ligand stats record is\n {}\n\n".format(ligand_stats_record))

bar()

data_processing_record = data_processing.iloc[0]
print("Pandda data processing record is {}\n\n".format(data_processing_record))

bar()

proasis_pandda_record = proasis_pandda.iloc[0]
print("Pandda proasis record is {}\n\n".format(proasis_pandda_record))

