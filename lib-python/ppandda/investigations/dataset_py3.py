import argparse
import time

import numpy as np

import pandas as pd

import itertools
import array_split

import pathlib as p
from scipy.stats import mode


import subprocess

import os

import h5py

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d",
                        "--directory",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")
    parser.add_argument("-o",
                        "--output",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/np_maps")

    args = parser.parse_args()

    return args


class Dataset:

    def __init__(self, directory, num_samples):

        self.directory = directory
        self.num_samples = num_samples
        # self.column = column
        self.shape = None

        self.df = 1

        self.get_crystal_paths()

        if num_samples != "all":
            self.df = self.df.head(num_samples)
            print("\tAfter filtering {} datasets remain".format(len(self.df)))

        self.maps = None

    def get_crystal_paths(self):

        print("Finding datasets...")

        processed_datasets = p.Path(self.directory) / "processed_datasets"

        crystals = processed_datasets.glob("*")

        datasets = [{"crystal": crystal.name,
                     "path": crystal}
                    for crystal in crystals if crystal.is_dir()]

        df = pd.DataFrame(datasets)

        self.df = df

        print("\t found {} datasets".format(len(datasets)))

        return self.df

    def get_status(self):


        status = []

        for path in self.df["path"]:

            if len(list((path / "modelled_structures").glob("*"))) != 0:

                status.append(2)

            elif len(list((path / "ligand_files").glob("*"))) != 0:

                status.append(1)

            else:
                status.append(0)

        # print("Finding apo datasets...")
        #
        # apo = [1 if len(list((path / "ligand_files").glob("*"))) == 0 else 0 for path in self.df["path"]]
        #
        # print("\t{} out of {} datasets are apo".format(len([ap for ap in apo if ap == 1]), len(apo)))
        #
        # print("finding modelled data sets...")
        #
        # modelled =

        self.df["status"] = pd.Series(status)

        # print(self.df.head(200))
        #
        # exit()

    def generate_np_maps_from_ccp4s(self, map_name="-pandda-input_2mFo-DFc.ccp4", overwrite=False):

        print("Generating numpy maps...")

        for crystal_path in self.df["path"]:

            map_path = crystal_path / (str(crystal_path.name) + map_name)
            array_path = crystal_path / (str(crystal_path.name) + map_name + ".npy")
            print("\tHandling map {}...".format(map_path))

            if map_path.exists():
                print("\t\tSkipping map, already exists...")
                pass
            else:
                print("\t\tGenerating numpy array from map...")
                gen_map_start = time.time()
                p = subprocess.Popen("ccp_python ccp4_to_np -m {} -o {} -v 1".format(map_path,
                                                                                     array_path),
                                     shell=True)
                gen_map_finish = time.time()
                print("Generated map in {}".format(gen_map_finish - gen_map_start))

    def generate_hdf5(self, hdf5_file, map_name="-pandda-input_2mFo-DFc.ccp4"):

        f = h5py.File(hdf5_file, "a")

        print("Generating hdf5 maps...")
        gen_map_start = time.time()

        for crystal_path in self.df["path"]:

            array_path = crystal_path / (str(crystal_path.name) + map_name + ".npy")
            print("\tHandling map {}...".format(array_path))
            if crystal_path.name in f:
                print("\t\tAlready added dataset. Skipping.")
            else:
                f.create_dataset("{}".format(crystal_path.name), data=np.load(array_path), chunks=True)

        gen_map_finish = time.time()
        print("Generated store in {}".format(gen_map_finish - gen_map_start))

    def load_hdf5(self, hdf5_file):

        self.maps = h5py.File(hdf5_file, 'r')

    def load_np_maps(self, map_name="-pandda-input_2mFo-DFc.ccp4", mmap=True, keep_mode=True, exclude_different_sizes=True):

        print("Loading maps...")

        mmap_start = time.time()
        if mmap==True:

            for crystal_path in self.df["path"]:
                array_path = crystal_path / (str(crystal_path.name) + map_name + ".npy")

                maps = [np.load(str(crystal_path / (str(crystal_path.name) + map_name + ".npy")), mmap_mode="r")
                        for crystal_path in self.df["path"]]
        mmap_finish = time.time()
        print("\tLoaded maps in {}".format(mmap_finish-mmap_start))


        # modal_shape = mode([mmap.shape for mmap in maps]).mode

