from sklearn.decomposition import MiniBatchSparsePCA
import glob
import argparse
import itertools
import os

import numpy as np

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--map_path")
    parser.add_argument("-d", "--map_directory_path")
    parser.add_argument("-n", "--num_components")

    args = parser.parse_args()

    return args


if __name__ == "__main__":

    args = parse_args()

    contrast_set = ["x0001", "x0003", "x0004", "x0005", "x0006",
        "x0007", "x0008", "x0010", "x0011", "x0012",
        "x0013", "x0014", "x0015", "x0016", "x0017",
        "x0018", "x0019", "x0020", "x0021", "x0022",
        "x0023", "x0024", "x0025", "x0026", "x0027",
        #"x0028",
        "x0029", "x0030", "x0031", "x0032",
        "x0033", "x0034", "x0035",
                    #"x0036",
        "x0037",
        "x0038", "x0039", "x0040", "x0041", "x0042",
        "x0043", "x0044", "x0046", "x0047", "x0048",
        "x0049",
                    #"x0050",
        "x0051", "x0052", "x0053",
        "x0055", "x0056", "x0057", "x0058", "x0061",
        "x0062", "x0063", "x0064", "x0065", "x0066"]

    # Load datasets

    print("Loading taget dataset", args.map_path)
    datasets = []
    target_dataset_unflattened = np.load(args.map_path)
    print(target_dataset_unflattened.shape)
    target_dataset = target_dataset_unflattened.flatten()
    datasets.append([args.map_path, target_dataset])

    print("Searching for datasets along", args.map_directory_path+"/*/*.npy")
    directories = glob.glob(args.map_directory_path+"/*")
    dataset_paths = []
    for directory in directories:
        npy = glob.glob(directory+"/*.npy")
        dataset_paths.append(npy)

    # print(dataset_paths)

    for dataset_path in dataset_paths:
        # print("Looking for contrast match for ", dataset_path)

        if not dataset_path: continue

        for contrast_name in contrast_set:
            # print(contrast_name, dataset_path)
            if contrast_name in dataset_path[0]:
                print("Loading data for ", contrast_name, "from ", dataset_path)

                array = np.load(dataset_path[0])
                if array.shape[0] != 225:
                    print("Array is wrong shape!")
                    continue
                else:
                    datasets.append([contrast_name, array.flatten()])


    # Perform PCA
    datasets_arrays = [dataset[1] for dataset in datasets]
    sample_by_components = np.vstack(datasets_arrays)
    print(sample_by_components.shape)
    pca = MiniBatchSparsePCA(n_components=int(args.num_components))
    print("Performing MB_sparse_pca on {} components!".format(args.num_components))
    components = pca.fit_transform(sample_by_components)

    print(components)

    # Plot
    combinations = list(itertools.combinations(range(int(args.num_components)), 2))
    num_combinations = len(combinations)

    fig, ax = plt.subplots(1, num_combinations)
    fig.set_size_inches(45, 15)

    for j, c in enumerate(combinations):

        ax[j].scatter(components[:, c[0]], components[:, c[1]])
        print(components.shape)

        for i, txt in enumerate(datasets):
            ax[j].annotate(txt[0], (components[i, c[0]], components[i, c[1]]))

        ax[j].set_xlabel(c)

    fig.savefig("components_{}_x0122.png".format(args.num_components))

    for i in range(int(args.num_components)):

        print(dir(pca))

        component = pca.components_[i]

        print("20th component is", component[20])

        component_reshaped = component.reshape((225, 225, 180))

        print("saving to mb_sparse_pca_{}.npy".format(i))

        np.save("mb_sparse_pca_{}.npy".format(i), component_reshaped)

        os.popen("~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py -a mb_sparse_pca_{}.npy -m processed_datasets/PDK2-x0001/PDK2-x0001-pandda-input_2mFo-DFc.ccp4".format(i))


    for i in range(int(args.num_components)):

        print(dir(pca))

        component = pca.components_[i]

        print("20th component is", component[20])

        component_reshaped = component.reshape((225, 225, 180))

        print("saving to ica_neg{}.npy".format(i))

        np.save("mb_sparse_pca_neg_{}.npy".format(i), -component_reshaped)

        os.popen("~/myccp4python ~/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py -a mb_sparse_pca_neg_{}.npy -m processed_datasets/PDK2-x0001/PDK2-x0001-pandda-input_2mFo-DFc.ccp4".format(i))




