import argparse
import os
from shutil import rmtree
import re
from subprocess import PIPE, Popen

import numpy as np
from scipy.optimize import basinhopping
import pandas as pd
import sqlalchemy

from biopandas.pdb import PandasPdb

from rdkit import Chem
from rdkit.Chem import AllChem

from qfit_ligand.validator import Validator
from qfit_ligand.volume import Volume
from qfit_ligand.structure import Structure


# Panddas optoins

pd.options.display.max_columns = 20

pd.options.display.max_colwidth = 50


def parse_args():
    parser = argparse.ArgumentParser()

    # PanDDA run
    parser.add_argument("-r", "--pandda_run", default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Output
    parser.add_argument("-o", "--output", default="basin0/")

    # Database
    parser.add_argument("-hs", "--host", default="172.23.142.43")
    parser.add_argument("-p", "--port", default="5432")
    parser.add_argument("-d", "--database", default="test_xchem")
    parser.add_argument("-u", "--user", default="conor")
    parser.add_argument("-pw", "--password", default="c0n0r")

    # QFit parameters
    parser.add_argument("-s", "--angular_step", default=6, type=int)
    parser.add_argument("-b", "--degrees_freedom", default=2, type=int)

    # Interpeter args
    parser.add_argument("-cpy", "--ccp4_python", default="/home/zoh22914/myccp4python")


    args = parser.parse_args()

    return args


def rotate(sample_by_feature, x, y, z):

    mean_x = np.mean(sample_by_feature[:, 0])
    mean_y = np.mean(sample_by_feature[:, 1])
    mean_z = np.mean(sample_by_feature[:, 2])

    de_meaned_x = sample_by_feature[:, 0] - mean_x
    de_meaned_y = sample_by_feature[:, 1] - mean_y
    de_meaned_z = sample_by_feature[:, 2] - mean_z

    sample_by_feature[:, 0] = de_meaned_x
    sample_by_feature[:, 1] = de_meaned_y
    sample_by_feature[:, 2] = de_meaned_z

    sx = np.sin(x)
    cx = np.cos(x)
    sy = np.sin(y)
    cy = np.cos(y)
    sz = np.sin(z)
    cz = np.cos(z)

    rx = [[1, 0, 0],
           [0, cx, sx],
           [0, -sx, cx]]
    ry = [[cy, 0, -sy],
           [0, 1, 0],
           [sy, 0, cy]]
    rz = [[cz, sz, 0],
           [-sz, cz, 0],
           [0, 0, 1]]

    rotated_x = np.matmul(rx, sample_by_feature.T)
    rotated_y = np.matmul(ry, rotated_x)
    rotated_z = np.matmul(rx, rotated_y)

    rotated_matrix = rotated_z.T

    rotated_matrix[:, 0] = rotated_matrix[:, 0] + mean_x
    rotated_matrix[:, 1] = rotated_matrix[:, 1] + mean_y
    rotated_matrix[:, 2] = rotated_matrix[:, 2] + mean_z

    return rotated_matrix


def recentre(ligand, centroid):

    ligand_mean = [ligand.df['HETATM']["x_coord"].mean(),
                   ligand.df['HETATM']["y_coord"].mean(),
                   ligand.df['HETATM']["z_coord"].mean()]

    x_mean_diff = ligand_mean[0] - centroid[0]
    y_mean_diff = ligand_mean[1] - centroid[1]
    z_mean_diff = ligand_mean[2] - centroid[2]
    print("vector is ({},{},{})".format(x_mean_diff, y_mean_diff, z_mean_diff))

    centered_x = ligand.df['HETATM']["x_coord"] - x_mean_diff
    centered_y = ligand.df['HETATM']["y_coord"] - y_mean_diff
    centered_z = ligand.df['HETATM']["z_coord"] - z_mean_diff

    ligand.df['HETATM']["x_coord"] = centered_x
    ligand.df['HETATM']["y_coord"] = centered_y
    ligand.df['HETATM']["z_coord"] = centered_z

    return ligand, ligand_mean


def load_map(map_path, output_path):
    os.popen("{} {} -m {} -o {}".format(ccp4_python, ccp42np, target_map_path, map_dir))

    numpy_map = np.load(target_map_path + ".npy")

    return numpy_map


def generate_bdc_map(bdc, target_map, mean_map, template, output, python):

    bdc_map = (target_map - bdc*mean_map)/(1-bdc)

    np.save(bdc_map_path, bdc_map)

    os.popen("{} {} -a {} -m {}".format(python, np2ccp4, output, template))


def translate(sample_by_feature, x, y, z):

    sample_by_feature[:, 0] = sample_by_feature[:, 0] + x
    sample_by_feature[:, 1] = sample_by_feature[:, 1] + y
    sample_by_feature[:, 2] = sample_by_feature[:, 2] + z

    return sample_by_feature


def gen_embedding(x, embedding):
    # Extract
    sample_by_feature = embedding.df["HETATM"].as_matrix(columns=["x_coord", "y_coord", "z_coord"])

    # Rotate
    rot = [x[3], x[4], x[5]]
    rotated = rotate(sample_by_feature, rot[0], rot[1], rot[2])
    print("Rotated x: ({}), y: ({}), z: ({})".format(rot[0], rot[1], rot[2]))

    # Translate
    trans = [x[0], x[1], x[2]]
    translated = translate(rotated, trans[0], trans[1], trans[2])
    print("Translated x: ({}), y: ({}), z: ({})".format(trans[0], trans[1], trans[2]))

    # Update
    embedding.df["HETATM"]["x_coord"] = translated[:, 0]
    embedding.df["HETATM"]["y_coord"] = translated[:, 1]
    embedding.df["HETATM"]["z_coord"] = translated[:, 2]

    return embedding


def check_clash(embedding, receptor, centorid):
    distances = receptor.distance(xyz=centorid, records='ATOM')
    all_within_7A = receptor.df['ATOM'][distances < 12.0]

    clashing = 0

    for atom in embedding.df["HETATM"].itertuples():

        if clashing == 1:
            continue

        distances = receptor.distance_df(all_within_7A,
                                         xyz=(atom.x_coord, atom.y_coord, atom.z_coord))

        if distances[distances < 0.9].any():
            print(distances[distances < 0.9])
            print(atom.x_coord, atom.y_coord, atom.z_coord)
            print("CLASHING!")
            clashing = clashing + 1

    return clashing


def get_rscc(embedding, structure_dir, xmap):
    # Output  model
    pandas_embedding_path = structure_dir + "pandas_embedding.pdb"
    embedding.to_pdb(pandas_embedding_path)

    # Calculate RSCC
    structure_ligand = Structure.fromfile(pandas_embedding_path)

    structure_ligand.b = structure_ligand.b + 20.0

    validator = Validator(xmap, float(resolution))
    rscc = validator.rscc(structure_ligand)

    print("RSCC is {}".format(rscc))

    return rscc


def score_conformer(x, embedding, receptor, centorid, xmap, structure_dir, coordinates):

    copy_embedding = embedding.copy()

    new_embedding = gen_embedding(x, copy_embedding)

    clash = check_clash(new_embedding, receptor, centorid)

    rscc = get_rscc(new_embedding, structure_dir, xmap)

    print("New ligand mean is ({}, {}, {})".format(new_embedding.df["HETATM"]["x_coord"].mean(),
                                                   new_embedding.df["HETATM"]["y_coord"].mean(),
                                                   new_embedding.df["HETATM"]["z_coord"].mean()))

    score = (1 + clash) - rscc
    print("Conformer score is {}".format(score))

    return score






if __name__ == "__main__":

    args = parse_args()

    # Establish python vars
    home = "/home/zoh22914/"
    base_python = "python"
    ccp4_python = home + "myccp4python"
    np2ccp4 = home + "pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py"
    ccp42np = home + "pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py"
    output = args.pandda_run + "/" + args.output

    # Prepare output dir

    def prepare_output(output):

        try:
            os.mkdir(output)

        except:
            rmtree(output)
            os.mkdir(output)

        qfit_dir = output + "qfit/"
        os.mkdir(qfit_dir)


        structure_dir = output + "structures/"
        os.mkdir(structure_dir)

        map_dir = output + "maps/"
        os.mkdir(map_dir)

        return qfit_dir, structure_dir, map_dir

    qfit_dir, structure_dir, map_dir = prepare_output(output)


    # Connect to database

    engine=sqlalchemy.create_engine("postgresql://{}:{}@{}:{}/{}".format(args.user, args.password, args.host, args.port, args.database))

    pandda_analysis = pd.read_sql_query("SELECT * FROM pandda_analysis", con=engine)

    pandda_run = pd.read_sql_query("SELECT * FROM pandda_run", con=engine)

    pandda_events = pd.read_sql_query("SELECT * FROM pandda_event", con=engine)

    pandda_events_stats = pd.read_sql_query("SELECT * FROM pandda_event_stats", con=engine)

    statistical_maps = pd.read_sql_query("SELECT * FROM pandda_statistical_map", con=engine)

    crystals = pd.read_sql_query("SELECT * FROM crystal", con=engine)

    ligands = pd.read_sql_query("SELECT * FROM proasis_hits", con=engine)

    ligand_stats = pd.read_sql_query("SELECT * FROM ligand_edstats", con=engine)

    data_processing = pd.read_sql_query("SELECT * FROM data_processing", con=engine)

    # Query database

    # TODO: Un-hard-code this all
    analysis_id = pandda_analysis[pandda_analysis["pandda_dir"] == args.pandda_run]["id"].iloc[0]
    print("Pandda analysis id is {}".format(analysis_id))

    run_id = pandda_run[pandda_run["pandda_analysis_id"] == analysis_id]["id"].iloc[-1]
    print("PanDDA run id is {}".format(run_id))

    crystal_id = crystals[crystals["crystal_name"] == "PDK2-x0513"]["id"].iloc[0]
    print("Crystal id is {}".format(crystal_id))


    statistical_map = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/PDK2-x0128-ground-state-mean-map.native.ccp4"

    print("PanDDA statistical map path is {}".format(statistical_map))


    pandda_event_id = pandda_events[(pandda_events["crystal_id"] == crystal_id)
                                    & (pandda_events["pandda_run_id"] == run_id)
                                    & (pandda_events["event"] == 1)]["id"].iloc[0]
    print("pandda_event id is {}".format(pandda_event_id))


    # TODO : Un-hard code this
    # statistical_map_path = run_id["pandda_dir"].loc[0]
    # print(statistical_map_path)

    # Load maps
    mtz_path = pandda_events[pandda_events["id"] == pandda_event_id]["pandda_input_mtz"].iloc[0]
    print("MTZ path is {}. Converting to ccp4.".format(mtz_path))
    mean_map_path = statistical_map
    os.popen("phenix.mtz2map {} directory={} grid_resolution_factor=0.166666".format(mtz_path, map_dir))

    target_map_path = map_dir + os.path.splitext(os.path.basename(mtz_path))[0] + "_2mFo-DFc.ccp4"
    numpy_map = load_map(target_map_path, map_dir)
    print("Loaded 2mFo-DFc arrap of shape {}".format(numpy_map.shape))

    numpy_mean_map = load_map(statistical_map, map_dir)
    print("Loaded mean array of shape {}".format(numpy_mean_map.shape))

    # TODO: Get event stats working so this can be used
    # Generate BDC map - pipeline isn't working
    # print(pandda_events_stats)
    # bdc = pandda_events_stats[pandda_events_stats["event_id"] == pandda_event_id]["one_minus_bdc"].iloc[0]
    bdc = 0.26
    print("BDC factor is {}".format(bdc))

    bdc_map_path = map_dir + "bdc_map.npy"
    bdc_map = generate_bdc_map(bdc, numpy_map, numpy_mean_map, statistical_map, bdc_map_path, ccp4_python)
    print("Saved bdc map")

    # Get centroid
    centorid = [pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_x"].iloc[0],
                pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_y"].iloc[0],
                pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_z"].iloc[0]]
    print("cetroid is ({},{},{})".format(centorid[0], centorid[1], centorid[2]))

    # Load PDBS
    # ligand_pdb_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/ligand_files/VER-00160616_with_H.pdb"
    #ligand_smiles_path = "COc1ccc(cc1)N(C)C(=O)c1ccc(O)cc1O"
    # ligand_cif_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/ligand_files/VER-00160616.cif"
    ligand_smiles_path = "CN(C(=O)c1ccc(O)cc1O)c1ccc(cc1)C(=O)N(C)c1ccccc1C"

    print("ligand pdb path is {}".format( ligand_smiles_path))
    # ligand = Chem.MolFromPDBFile(ligand_pdb_path)
    ligand = Chem.MolFromSmiles(ligand_smiles_path)

    # Sample
    resolution = data_processing[data_processing["crystal_name_id"] == crystal_id]["res_high"].iloc[0]
    receptor = pandda_events[pandda_events["id"] == pandda_event_id]["pandda_input_pdb"].iloc[0]
    receptor_pdb = PandasPdb().read_pdb(receptor)

    print("Dataset resolution is {}".format(resolution))
    print("Path to receptor is {}".format(receptor))

    embeddings = AllChem.EmbedMultipleConfs(ligand,
                                            clearConfs=True,
                                            numConfs=100,
                                            pruneRmsThresh=1)

    print("Found ({}) distinct embeddings".format(len(embeddings)))

    volume_xmap = Volume.fromfile(bdc_map_path + ".ccp4")

    rscc_current_max = 0

    for embedding_id in embeddings:
        print("working on embedding: {}".format(embedding_id))
        # Optimise
        print("Optimising conformer geometry")
        # AllChem.MMFFOptimizeMolecule(ligand, confId=embedding)

        # Save to pdb
        Chem.MolToPDBFile(ligand, "embedding.pdb", confId=embedding_id)

        # Open
        print("Reading embedding")
        pandas_embedding = PandasPdb().read_pdb("embedding.pdb")

        # recentre
        pandas_embedding, ligand_mean = recentre(pandas_embedding, centorid)
        coordinates = [pandas_embedding.df['HETATM']["x_coord"].copy(),
                       pandas_embedding.df['HETATM']["y_coord"].copy(),
                       pandas_embedding.df['HETATM']["z_coord"].copy()]


        initial_guess = np.array([0, 0, 0, 0, 0, 0])

        func = lambda x: score_conformer(x, pandas_embedding, receptor_pdb, centorid, volume_xmap, structure_dir,
                                         coordinates)

        result = basinhopping(func, initial_guess)

        print(result)

        conformer = gen_embedding(result.x, pandas_embedding)

        conf_embedding_path = structure_dir + "{}_{}.pdb".format(embedding_id, 1-result.fun)
        conformer.to_pdb(conf_embedding_path)

        # Reset




