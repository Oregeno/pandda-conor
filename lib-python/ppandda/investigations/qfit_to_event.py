import argparse
import os
from shutil import rmtree
import re
from subprocess import PIPE, Popen

import numpy as np
import pandas as pd
import sqlalchemy

from biopandas.pdb import PandasPdb

import psycopg2

# Panddas optoins

pd.options.display.max_columns = 20

pd.options.display.max_colwidth = 50

def parse_args():
    parser = argparse.ArgumentParser()

    # PanDDA run
    parser.add_argument("-r", "--pandda_run", default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso")

    # Output
    parser.add_argument("-o", "--output", default="autofit3/")

    # Database
    parser.add_argument("-hs", "--host", default="172.23.142.43")
    parser.add_argument("-p", "--port", default="5432")
    parser.add_argument("-d", "--database", default="test_xchem")
    parser.add_argument("-u", "--user", default="conor")
    parser.add_argument("-pw", "--password", default="c0n0r")

    # QFit parameters
    parser.add_argument("-s", "--angular_step", default=6, type=int)
    parser.add_argument("-b", "--degrees_freedom", default=2, type=int)

    # Interpeter args
    parser.add_argument("-cpy", "--ccp4_python", default="/home/zoh22914/myccp4python")


    args = parser.parse_args()

    return args

def rotate(sample_by_feature, x, y, z):

    mean_x = np.mean(sample_by_feature[:, 0])
    mean_y = np.mean(sample_by_feature[:, 1])
    mean_z = np.mean(sample_by_feature[:, 2])

    de_meaned_x = sample_by_feature[:, 0] - mean_x
    de_meaned_y = sample_by_feature[:, 1] - mean_y
    de_meaned_z = sample_by_feature[:, 2] - mean_z

    sample_by_feature[:, 0] = de_meaned_x
    sample_by_feature[:, 1] = de_meaned_y
    sample_by_feature[:, 2] = de_meaned_z

    sx = np.sin(x)
    cx = np.cos(x)
    sy = np.sin(y)
    cy = np.cos(y)
    sz = np.sin(z)
    cz = np.cos(z)

    rx = [[1, 0, 0],
           [0, cx, sx],
           [0, -sx, cx]]
    ry = [[cy, 0, -sy],
           [0, 1, 0],
           [sy, 0, cy]]
    rz = [[cz, sz, 0],
           [-sz, cz, 0],
           [0, 0, 1]]

    rotated_x = np.matmul(rx, sample_by_feature.T)
    rotated_y = np.matmul(ry, rotated_x)
    rotated_z = np.matmul(rx, rotated_y)

    rotated_matrix = rotated_z.T

    rotated_matrix[:, 0] = rotated_matrix[:, 0] + mean_x
    rotated_matrix[:, 1] = rotated_matrix[:, 1] + mean_y
    rotated_matrix[:, 2] = rotated_matrix[:, 2] + mean_z

    return rotated_matrix


if __name__ == "__main__":

    args = parse_args()

    # Establish python vars
    home = "/home/zoh22914/"
    base_python = "python"
    ccp4_python = home + "myccp4python"
    np2ccp4 = home + "pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py"
    ccp42np = home + "pandda-conor/lib-python/ppandda/investigations/ccp4_to_np.py"
    output = args.pandda_run + "/" + args.output

    # Prepare output dir
    try:
        os.mkdir(output)
    except:
        rmtree(output)
        os.mkdir(output)

    qfit_dir = output + "qfit/"
    os.mkdir(qfit_dir)


    structure_dir = output + "structures/"
    os.mkdir(structure_dir)

    map_dir = output + "maps/"
    os.mkdir(map_dir)


    # Connect to database

    # conn = psycopg2.connect(host=args.host,
    #                         port=args.port,
    #                         database=args.database,
    #                         user=args.user,
    #                         password=args.password)
    #
    # cur = conn.cursor()
    #
    # cur.execute("SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'")
    #
    # # tables = pd.DataFrame(cur.fetchall())
    # tables = pd.io.sql.read_sql("SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'", conn)
    #
    # print(tables)
    # cur.execute("SELECT * FROM pandda_event")
    # pandda_events = pd.DataFrame(cur.fetchall())

    engine=sqlalchemy.create_engine("postgresql://{}:{}@{}:{}/{}".format(args.user, args.password, args.host, args.port, args.database))

    pandda_analysis = pd.read_sql_query("SELECT * FROM pandda_analysis", con=engine)

    pandda_run = pd.read_sql_query("SELECT * FROM pandda_run", con=engine)

    pandda_events = pd.read_sql_query("SELECT * FROM pandda_event", con=engine)

    pandda_events_stats = pd.read_sql_query("SELECT * FROM pandda_event_stats", con=engine)

    statistical_maps = pd.read_sql_query("SELECT * FROM pandda_statistical_map", con=engine)

    crystals = pd.read_sql_query("SELECT * FROM crystal", con=engine)

    ligands = pd.read_sql_query("SELECT * FROM proasis_hits", con=engine)

    ligand_stats = pd.read_sql_query("SELECT * FROM ligand_edstats", con=engine)

    data_processing = pd.read_sql_query("SELECT * FROM data_processing", con=engine)

    # Query database

    # TODO: Un-hard-code this all
    analysis_id = pandda_analysis[pandda_analysis["pandda_dir"] == args.pandda_run]["id"].iloc[0]
    print("Pandda analysis id is {}".format(analysis_id))

    run_id = pandda_run[pandda_run["pandda_analysis_id"] == analysis_id]["id"].iloc[-1]
    print("PanDDA run id is {}".format(run_id))

    crystal_id = crystals[crystals["crystal_name"] == "PDK2-x0128"]["id"].iloc[0]
    print("Crystal id is {}".format(crystal_id))


    statistical_map = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/PDK2-x0128-ground-state-mean-map.native.ccp4"

    print("PanDDA statistical map path is {}".format(statistical_map))


    pandda_event_id = pandda_events[(pandda_events["crystal_id"] == crystal_id)
                                    & (pandda_events["pandda_run_id"] == run_id)
                                    & (pandda_events["event"] == 2)]["id"].iloc[0]
    print("pandda_event id is {}".format(pandda_event_id))

    # ligand_id =

    # TODO : Un-hard code this
    # statistical_map_path = run_id["pandda_dir"].loc[0]
    # print(statistical_map_path)

    # Load maps
    mtz_path = pandda_events[pandda_events["id"] == pandda_event_id]["pandda_input_mtz"].iloc[0]
    print("MTZ path is {}. Converting to ccp4.".format(mtz_path))
    mean_map_path = statistical_map

    os.popen("phenix.mtz2map {} directory={} grid_resolution_factor=0.166666".format(mtz_path, map_dir))

    target_map_path = map_dir + os.path.splitext(os.path.basename(mtz_path))[0] + "_2mFo-DFc.ccp4"

    os.popen("{} {} -m {} -o {}".format(ccp4_python, ccp42np, target_map_path, map_dir))

    numpy_map = np.load(target_map_path + ".npy")
    print("Loaded 2mFo-DFc arrap of shape {}".format(numpy_map.shape))

    os.popen("{} {} -m {} -o {}".format(ccp4_python, ccp42np, statistical_map, map_dir))

    numpy_mean_map_path = map_dir + os.path.basename(statistical_map) + ".npy"

    numpy_mean_map = np.load(numpy_mean_map_path)
    print("Loaded mean array of shape {}".format(numpy_mean_map.shape))

    # TODO: Get event stats working so this can be used
    # Generate BDC map - pipeline isn't working
    # print(pandda_events_stats)
    # bdc = pandda_events_stats[pandda_events_stats["event_id"] == pandda_event_id]["one_minus_bdc"].iloc[0]
    bdc = 0.26
    print("BDC factor is {}".format(bdc))

    bdc_map = (numpy_map - bdc*numpy_mean_map)/(1-bdc)
    bdc_map_path = map_dir + "bdc_map.npy"

    np.save(bdc_map_path, bdc_map)

    os.popen("{} {} -a {} -m {}".format(ccp4_python, np2ccp4, bdc_map_path, statistical_map))

    print("Save bdc map of size {}".format(bdc_map.shape))

    # Load PDBS

    # ligand_pdb_path = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/processed_datasets/PDK2-x0128/ligand_files/VER-00160616.pdb"
    ligand_pdb_path  = "/dls/labxchem/data/2018/lb19758-9/processing/analysis/panddas_run12_all_onlydmso/autofit2/qfit/conformer_3.pdb"


    print("ligand pdb path is {}".format( ligand_pdb_path))

    ligand = PandasPdb().read_pdb(ligand_pdb_path)

    #print(ligand.df["HETATM"])

    ligand_x_mean = ligand.df['HETATM']["x_coord"].mean()
    ligand_y_mean = ligand.df['HETATM']["y_coord"].mean()
    ligand_z_mean = ligand.df['HETATM']["z_coord"].mean()

    #print(ligand.df['HETATM']["x_coord"])

    centorid_x = pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_x"].iloc[0]
    centorid_y = pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_y"].iloc[0]
    centorid_z = pandda_events[pandda_events["id"] == pandda_event_id]["event_centroid_z"].iloc[0]
    print("cetroid is ({},{},{})".format(centorid_x, centorid_y, centorid_z))

    x_mean_diff = ligand_x_mean - centorid_x
    y_mean_diff = ligand_y_mean - centorid_y
    z_mean_diff = ligand_z_mean - centorid_z
    print("vector is ({},{},{})".format(x_mean_diff, y_mean_diff, z_mean_diff))

    centered_x = ligand.df['HETATM']["x_coord"] - x_mean_diff
    centered_y = ligand.df['HETATM']["y_coord"] - y_mean_diff
    centered_z = ligand.df['HETATM']["z_coord"] - z_mean_diff

    ligand.df['HETATM']["x_coord"] = centered_x
    ligand.df['HETATM']["y_coord"] = centered_y
    ligand.df['HETATM']["z_coord"] = centered_z

    print(ligand.df['HETATM']["x_coord"].iloc[0])

    # Output joint model
    ligand_path = path=structure_dir + "ligand.pdb"
    ligand.to_pdb(ligand_path)

    # Qfit

    resolution = data_processing[data_processing["crystal_name_id"] == crystal_id]["res_high"].iloc[0]
    receptor = pandda_events[pandda_events["id"] == pandda_event_id]["pandda_input_pdb"].iloc[0]

    print("Dataset resolution is {}".format(resolution))
    print("Path to receptor is {}".format(receptor))

    while True:
        print("QFit-ing with -s {} and -b {}".format(args.angular_step, args.degrees_freedom))

        process = Popen("qfit_ligand {} {} {} -r {} -s {} -b {} -d {}".format(bdc_map_path + ".ccp4",
                                                                       resolution,
                                                                       ligand_path,
                                                                       receptor,
                                                                       args.angular_step,
                                                                       args.degrees_freedom,
                                                                       qfit_dir),
                           shell=True,
                           stdout=PIPE,
                           stderr=PIPE)

        stdout, stderr = process.communicate()

        print("Process returned: {}".format(stderr))


        # Open log

        if stderr == "":

            file = open(qfit_dir + "qfit_ligand.log", "r")

            text = file.read()

            matches = re.findall("INFO:qfit_ligand.qfit_ligand:((\/[^\/\:]*)*): (.*)", text)

            print(matches)

            print("Best match is at ({}) with RSCC {}".format(matches[0][0], matches[0][-1]))

            break

        else:
            print("Initial clash, trying with new ligand")


            sample_by_feature = ligand.df["HETATM"].as_matrix(columns=["x_coord", "y_coord", "z_coord"])

            rot_x = np.random.rand()*2*np.pi
            rot_y = np.random.rand()*2*np.pi
            rot_z = np.random.rand()*2*np.pi

            rotated = rotate(sample_by_feature, rot_x, rot_y, rot_z)

            trans_x = np.random.normal(scale=1)
            trans_y = np.random.normal(scale=1)
            trans_z = np.random.normal(scale=1)

            rotated[:, 0] = rotated[:, 0] + trans_x
            rotated[:, 1] = rotated[:, 1] + trans_y
            rotated[:, 2] = rotated[:, 2] + trans_z

            ligand.df["HETATM"]["x_coord"] = rotated[:, 0]
            ligand.df["HETATM"]["y_coord"] = rotated[:, 1]
            ligand.df["HETATM"]["z_coord"] = rotated[:, 2]

            # ligand.df['HETATM']["x_coord"] = centered_x + np.random.normal(scale=2)
            # ligand.df['HETATM']["y_coord"] = centered_y + np.random.normal(scale=2)
            # ligand.df['HETATM']["z_coord"] = centered_z + np.random.normal(scale=2)

            mean_x = ligand.df['HETATM']["x_coord"].mean()
            mean_y = ligand.df['HETATM']["y_coord"].mean()
            mean_z = ligand.df['HETATM']["z_coord"].mean()

            print("Roated x: ({}), y: ({}), z: ({})".format(rot_x, rot_y, rot_z))
            print("Translated x: ({}), y: ({}), z: ({})".format(trans_x, trans_y, trans_z))

            # print("new ligand mean is ({}, {}, {})".format(mean_x, mean_y, mean_z))

            # Output joint model
            ligand_path = path = structure_dir + "ligand.pdb"
            ligand.to_pdb(ligand_path)




    # Output RSCC

