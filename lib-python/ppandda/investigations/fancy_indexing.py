import numpy as np

test_array = [[[1, 0, 0], [0, 0, 0], [0, 0, 0]],
              [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
              [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]


sample_by_feature = [[0, 0, 0],
                     [1, 1, 1],
                     [0, 1, 0]]

test_array = np.array(test_array)
sample_by_feature = np.array(sample_by_feature)

print("Fancy indexing result")
print(test_array[sample_by_feature[:,0], sample_by_feature[:,1], sample_by_feature[:,2]])

print("non_fancy result")

print(test_array[0, 0, 0])
print(test_array[1, 1, 1])

angles = [90, 90, 120]
lengths = []
origin = np.array([0, 0, 0])

alpha, beta, gamma = np.deg2rad(angles)
a, b, c = [109.347000122, 109.347000122, 83.7610015869]

cos_gamma = np.cos(gamma)
sin_gamma = np.sin(gamma)
cos_alpha = np.cos(alpha)
cos_beta = np.cos(beta)
omega = np.sqrt(
    1 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma +
    2 * cos_alpha * cos_beta * cos_gamma
)

cartesian_to_lattice = np.asarray([
    [1, -cos_gamma / sin_gamma,
     (cos_alpha * cos_gamma - cos_beta) / (omega * sin_gamma)],
    [0, 1 / sin_gamma,
     (cos_beta * cos_gamma - cos_alpha) / (omega * sin_gamma)],
    [0, 0,
     sin_gamma / omega],
])

print(cartesian_to_lattice)

sample_by_feature = np.array([1, 1, 1])
#
# print("Sample by feature shape is {}".format(sample_by_feature.shape))
# print("Origin shape is {}".format(np.array(volume.origin).shape))
# print("Origin is at {}".format(volume.origin))
#
# print(sample_by_feature[0, :])

shifted = sample_by_feature - np.array(origin)
print(shifted)
print("After translation the first coordinate is at {}".format(shifted))
tilted = np.matmul(cartesian_to_lattice, shifted.T)
print("After rotation the first coordinate is at {}".format(tilted))
stretched = tilted / 0.5
print("After stretching the dist coordinate is at {}".format(stretched))

indicies = np.asarray(stretched, dtype=np.int32)

print(indicies)

# indicies[0, :] += 3
# indicies[1, :] -= 6
# indicies[2, :] += 3

#
# indicies = np.mod(indicies, np.array(volume_xmap.array.shape).reshape(3, 1))