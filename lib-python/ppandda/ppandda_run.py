import configargparse

from ppandda import PPanDDA


def parse_args():
    parser = configargparse.ArgumentParser(default_config_files=['config.ini'])
    parser.add("directory")
    parser.add("builder", default="qfit")

    args = parser.parse_args()

    return args

if __name__ == "__main__":
    args = parse_args()

    ppandda = PPanDDA(args.directory, builder=args.builder)

    summary_statistics = ppandda.run()

    summary_statistics.to_csv('statistics.csv')