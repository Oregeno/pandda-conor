import pandas as pd

run_fields = ["Settings",
              "Datasets",
              "Clusters"]


def main_loop(run_table):

    run_table.datasets = get_datasets(run_table.data_path)

    for resolution_shell in run_table.resolution_shell:

        run_table.resulution_shell.for_build = select_build(run_table.datasets)

        run_table.resolution_shell.for_analysis = select_analyse(run_table.datasets)

        run_table.aligned_datasets = align_datasets

        run_table.reference_frame = build_refrence_frame()

        run_table.shell.statistical_maps =

        for map in run_table.datasets["include" < resolution_shell]:

            z_maps["map"] = calculate_z_map(map)

            events["map"] = find_events(map)

        native_maps = generate_native(run_table.events, run_table.datasets, run_table.z_maps)
