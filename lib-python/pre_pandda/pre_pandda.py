import argparse
import pathlib as p
import os

import pandas as pd

import psutil

from gridData.CCP4 import CCP4

from iotbx.file_reader import any_file

# Custom imports
from dataset import Dataset
from build_ccp4_maps import CCP4MapBuilder
from build_np_maps import NpMapBuilder
from build_hdf5_store import Hdf5StoreBuilder
from find_outliers import OutlierFinder
from exclude_from_outliers import Excluder
from gen_pandda_run_script import GenPanDDARunScript

import fancy_printing as fp

pd.options.display.max_colwidth = 200


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d",
                        "--directory",
                        # default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model")
                        # default="/dls/labxchem/data/2017/lb18145-17/processing/analysis/initial_model_conor")
                        default="/dls/labxchem/data/2018/lb18145-71/processing/analysis/initial_model_conor")

    parser.add_argument("-o",
                        "--output",
                        # default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/pre_pandda")
                        # default="/dls/labxchem/data/2017/lb18145-17/processing/analysis/pre_pandda")
                        default = "/dls/labxchem/data/2018/lb18145-71/processing/analysis/pre_pandda")

    parser.add_argument("-t",
                        "--table",
                        default=None)

    parser.add_argument("-ap",
                        "--infer_apo",
                        default=True)

    parser.add_argument("-mn",
                        "--mtz_nomenclature",
                        default=True)

    parser.add_argument("-m",
                        "--map_scheme",
                        default=None)


    parser.add_argument("-ov",
                        "--overwrite",
                        default=False)

    parser.add_argument("-r",
                        "--reparse",
                        default=True)

    parser.add_argument("-cn",
                        "--convert_numpy",
                        default=True)

    parser.add_argument("-hdf",
                        "--hdf5",
                        default=True)

    parser.add_argument("-la",
                        "--local_analysis",
                        default=True)



    args = parser.parse_args()

    return args


class PrePanDDA:


    def __init__(self,
                 overwrite=False,
                 infer_apo=True, do_build_ccp4_maps=True, do_build_np_maps=True, do_build_hdf5_store=True,
                 num_samples="all", mtz_nomenclature="dimple.mtz", pdb_nomenclature="dimple.pdb", ccp4_map_nomenclature="2fofc.map",
                 column="FWT", resolution_cutoff_high=3.0, grid_resolution_factor=0.5/3.0,
                 ccp4_python="ccp_python", ccp4_to_np="/home/zoh22914/pandda-conor/lib-python/ppandda/investigations/numpy_to_ccp4.py", interpolate=True,
                 xyz=[5, 5, 5], num_components=3):

        # Variables
        self.infer_apo = infer_apo
        self.do_build_ccp4_maps = do_build_ccp4_maps
        self.do_build_np_maps = do_build_np_maps
        self.do_build_hdf5_store = do_build_hdf5_store
        self.do_analyse_ccp4_maps = False
        self.do_analyse_hdf5_store = True
        self.gridding=None


        # Instantiate stages
        self.dataset = Dataset(num_samples, mtz_nomenclature,
                               infer_apo=infer_apo,
                               pdb_nomenclature=pdb_nomenclature,
                               ccp4_map_nomenclature=ccp4_map_nomenclature)
        self.build_ccp4_maps = CCP4MapBuilder(resolution_cutoff_high,
                                              grid_resolution_factor,
                                              ccp4_map_nomenclature=ccp4_map_nomenclature,
                                              overwrite=overwrite)
        self.build_np_maps = NpMapBuilder(ccp4_python,
                                          ccp4_to_np,
                                          interpolate=False,
                                          overwrite=overwrite)
        self.build_hdf5_store = Hdf5StoreBuilder(overwrite=overwrite)
        self.find_outliers = OutlierFinder(xyz, num_components)
        self.gen_pandda_run_script = GenPanDDARunScript()
        self.exclude_from_outliers = Excluder()

    def estimate_memory_requirements(self):

        fp.print_task_title("Estimating memory requirements")

        ccp4_map_path = self.dataset.df["ccp4_map_path"].iloc[0]

        g = CCP4(str(ccp4_map_path))

        ccp4_map = any_file(str(ccp4_map_path)).file_object
        flex_array = ccp4_map.map_data()
        print("Flex array is of shape {}".format(flex_array.accessor().all()))

        self.gridding = g.array.shape
        print(self.gridding)


        grid_data = g.array.nbytes
        edge_data = g.edges[0].nbytes + g.edges[1].nbytes + g.edges[2].nbytes

        total_memory = edge_data + grid_data

        available_memory = psutil.virtual_memory().available

        num_datasets = available_memory/total_memory

        print(g.array.shape)

        print("The total available memory is {}, and the data per dataset is approximately {}. ".format(available_memory,
                                                                 total_memory))
        print("This indicates can handle {} datasets. Number of datasets to process is {}".format(num_datasets,
                                                                                                  self.dataset.df[
                                                                                                      self.dataset.df[
                                                                                                          "ccp4_map_path"] != "CCP4_MAP_NOT_FOUND"].count()[
                                                                                                      "ccp4_map_path"]))

        if self.dataset.df[self.dataset.df["ccp4_map_path"] != "CCP4_MAP_NOT_FOUND"].count()["ccp4_map_path"]*0.9 > num_datasets:
            print("\tLikely cannot hold all datasets in memory! Activating hdf5 chunking!")
            self.do_build_ccp4_maps = True
            self.do_build_np_maps = True
            self.do_build_hdf5_store = True
        else:
            print("\tEstimation suggests can hold all data in memory! Won't chunk unless forced.")

    def build_output_directory(self, output):
        fp.print_task_title("Making output Directory...")
        try:
            os.mkdir(str(output))
            print("\tMade output directory")
        except Exception:
            print("\tAlready created directory")

        try:
            os.mkdir(str(output / "map_cluster_plots"))
            print("\tMade plot directory")
        except:
            print("\tAlready made plot directory")



    def __call__(self, directory, output, table=None, classes=None):

        print("Beginning Pre-PanDDA dataset classification...")

        self.build_output_directory(output)

        assert not ((table != None) and (classes != None)), "Do NOT give both table and classes arguments"

        if table != None:
            print("No Table of dataset classes. ")
        if table == None and classes == None:
            print("No dataset classes provided.")
            if self.infer_apo == True:
                print("Infer_apo is True. Will infer apo datasets from absence of CIF file in crystal directory.")
            else:
                print("Will not automatically infer apo. All datasets will be assigned the same class.")


        ######## Prepare Data ########
        self.dataset.parse_initial_models(directory, output, reparse=False)
        self.estimate_memory_requirements()

        # Build CCP4 maps from MTZs if they are not available
        if self.do_build_ccp4_maps== True:
            self.build_ccp4_maps(self.dataset.df["mtz_path"],
                                 gridding=self.gridding)
            self.dataset.df["ccp4_map"] = self.build_ccp4_maps.map_paths

        # Build numpy maps if they are forced or needed for chunking
        if self.do_build_np_maps == True:
            self.build_np_maps(self.dataset.df["ccp4_map"])
            self.dataset.df["np_map"] = self.build_np_maps.map_paths

        # Store numpy maps in hf5y store for efficient chunking of larges classes for local outlier analysis
        if self.do_build_hdf5_store == True:
            self.build_hdf5_store(self.dataset.df["crystal"],
                                  self.dataset.df["np_map"],
                                  directory / "map_store.hdf5")

        if self.do_analyse_ccp4_maps == True: maps = self.dataset.df["ccp4_path"]
        elif self.do_analyse_hdf5_store == True: maps = directory / "map_store.hdf5"

        self.find_outliers(maps,
                           keys = self.dataset.df["crystal"],
                           output=output / "map_cluster_plots",
                           apos=self.dataset.df["class"],
                           crystals=self.dataset.df["crystal"])


        ### Set up PanDDA script(s) ###
        self.gen_pandda_run_script(self.find_outliers.outlier_dict,
                                   output,
                                   directory)


if __name__ == "__main__":

    args = parse_args()
    print("Instantiating Pre-pandda")
    pre_pandda = PrePanDDA(overwrite=args.overwrite)

    print("Running pre-pandda")
    pre_pandda(directory=p.Path(args.directory),
               output=p.Path(args.output),
               table=None,
               classes=None)