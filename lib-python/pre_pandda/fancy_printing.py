def print_warning(str):
    print('\033[91m' + str + '\033[0m')

def print_task_title(str):
    print('\033[92m' + "################   " + str + "   ################" + '\033[0m')