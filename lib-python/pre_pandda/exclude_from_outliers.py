import pandas as pd

class Excluder:

    def __init__(self):

        pass

    def __call__(self, labels, outliers, classes):

        # Loop over classes

        outlier_dict = {}

        for dataset_class in pd.unique(classes):

            labels_subset = labels[classes == dataset_class]

            outliers_subset = outliers[classes == dataset_class]

            class_outliers = labels_subset[outliers_subset == True]

            class_outliers[dataset_class] = class_outliers

        return outlier_dict