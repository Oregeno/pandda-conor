from subprocess import Popen, PIPE

import numpy as np
import pandas as pd

import pathlib as p

from gridData.CCP4 import CCP4
from gridData import Grid

from scipy.stats import mode
import time as t

import pathos.pools as pp

import psutil

from skimage.transform import resize


import fancy_printing as fp


def resample_maps_dep(args):
    ccp4_map = args[0]
    map_shape_mode = args[1]
    ccp4_template_map = args[2]
    if ccp4_map is None:
        return None

    if not np.array_equal(np.array(ccp4_map.array.shape), map_shape_mode):
        g = Grid(ccp4_map.array, edges=ccp4_map.edges)
        return g.resample(ccp4_template_map)
    else:
        return ccp4_map

    def resample_maps(args):
        ccp4_map = args[0]
        map_shape_mode = args[1]
        ccp4_template_map = args[2]
        if ccp4_map is None:
            return None

        if not np.array_equal(np.array(ccp4_map.array.shape), map_shape_mode):
            g = Grid(ccp4_map.array, edges=ccp4_map.edges)


            return g.resample(ccp4_template_map)
        else:
            return ccp4_map



class NpMapBuilder():

    def __init__(self, ccp4_python, ccp4_to_np, interpolate=True, force_rebuild=False, overwrite=False):

        self.ccp4_python = ccp4_python
        self.ccp4_to_np = ccp4_to_np
        self.interpolate = interpolate

        self.map_paths = None

        self.overwrite = overwrite



    def __call__(self, ccp4_map_paths):

        fp.print_task_title("Building numpy maps...")

        # for ccp4_map_path in ccp4_map_paths:
        #     print(ccp4_map_path)
        #     g = CCP4(str(ccp4_map_path))
        #     print(g.array.shape)

        # ccp4_map_paths = ccp4_map_paths[0:50]

        if self.interpolate == True:

            # Checking which maps already have numpy arrays built
            np_paths = []
            for ccp4_map_path in ccp4_map_paths:
                path = p.Path(ccp4_map_path)
                np_path = path.parent / (path.name + ".npy")
                if np_path.exists():
                    np_paths.append(np_path)
                else:
                    np_paths.append(None)


            print(np_paths.count(None) )
            if np_paths.count(None) == 0:
                self.map_paths = pd.Series(np_paths)
                return

            # Loading maps
            print("Loaidng CCP4 maps as grid data formats CCP4 Grids...")
            ccp4_maps = []
            load_start = t.time()
            for ccp4_map_path, np_path in zip(ccp4_map_paths, np_paths):
                if (p.Path(ccp4_map_path).exists()) and (np_path is None):
                    ccp4_maps.append(CCP4(str(ccp4_map_path)))
                else:
                    ccp4_maps.append(None)
            load_finish = t.time()

            print("Loaded ccp4 maps in {}".format(load_finish-load_start))

            array_shape_mode = np.array(mode([ccp4_map.array.shape for ccp4_map in ccp4_maps if ccp4_map is not None]).mode).reshape(-1)

            print("modal array shape is {}".format(array_shape_mode))
            # print(np.array(ccp4_maps[0].array.shape))
            # print(array_shape_mode)

            ccp4_maps_not_none = [ccp4_map for ccp4_map in ccp4_maps if ccp4_map is not None]
            template_maps = [ccp4_map for ccp4_map in ccp4_maps_not_none if np.array_equal(np.array(ccp4_map.array.shape),
                                                                                  array_shape_mode)]
            template_map = Grid(template_maps[0].array,
                                edges=template_maps[0].edges)
            # print([ccp4_map.array.shape for ccp4_map in ccp4_maps])

            print("Resampling maps...")



            resample_start = t.time()
            # for i, ccp4_map in enumerate(ccp4_maps):
            #     # print(np.array(ccp4_map.array.shape))
            #     if not np.array_equal(np.array(ccp4_map.array.shape), array_shape_mode):
            #         # print("resampling")
            #         g = Grid(ccp4_map.array, edges=ccp4_map.edges)
            #         # print("Resampling {} to grid of {}.".format())
            #         ccp4_maps[i] = g.resample(template_map)
            #         # print("Resampled map has shape{}".format(ccp4_maps[i].grid.shape))


            # print(cpu_count())
            print(psutil.cpu_count())
            # resample_maps_specific = lambda ccp4_map: resample_maps(ccp4_map, array_shape_mode, template_map)
            pool = pp.ProcessPool(processes=psutil.cpu_count())
            args = zip(ccp4_maps,
                       [array_shape_mode for i in range(len(ccp4_maps))],
                       [template_map for i in range(len(ccp4_maps))])
            ccp4_maps = pool.map(resample_maps, args)

            resample_finish = t.time()
            # print([ccp4_map.grid.shape for ccp4_map in ccp4_maps if type(ccp4_map)==type(template_map)])
            print("\tResampled maps in {}".format(resample_finish-resample_start))

            np_map_paths = []
            # print(type(Grid))
            # print(type(CCP4))
            for ccp4_map, ccp4_map_path in zip(ccp4_maps, ccp4_map_paths):
                path = p.Path(ccp4_map_path)
                np_path = path.parent / (path.name + ".npy")
                # print("loop start")
                try:
                    np_path.relative_to("/")
                except:
                    # print("PATH COULD NOT BE RESOLVED!")
                    np_map_paths.append(None)
                    continue

                if ccp4_map is None:
                    np_map_paths.append(None)
                elif type(ccp4_map) == type(template_map):
                    np.save(np_path, ccp4_map.grid)
                    np_map_paths.append(np_path)
                    # print("Saving grid to{}".format(np_path))
                else:
                    np.save(np_path, ccp4_map.array)
                    np_map_paths.append(np_path)
                    # print("Saving CCP4 to {}".format(np_path))

                # else:
                #     print("ERROR: wrong map type!")

            self.map_paths = pd.Series(np_map_paths)
            # print(self.map_paths)

        else:

            numpy_paths = []
            print(ccp4_map_paths)
            for ccp4_map_path in ccp4_map_paths:

                print(ccp4_map_path)

                if ccp4_map_path == "CCP4_MAP_NOT_FOUND":
                    numpy_paths.append("NUMPY_MAP_NOT_FOUND")
                    continue

                numpy_path = (ccp4_map_path.parent / (ccp4_map_path.name + ".npy"))

                if numpy_path.exists() and (self.overwrite is False):
                    print("\t{} Alrady has a Numpy map already built. Skipping.".format(ccp4_map_path.parent.name))
                    numpy_paths.append(numpy_path)
                    continue

                ccp4_map = CCP4(str(ccp4_map_path))
                print(ccp4_map_path.name + ".npy", ccp4_map.array.shape)

                np.save(numpy_path, ccp4_map.array)
                numpy_paths.append(numpy_path)

                # p = Popen("{} ".format(self.ccp4_python)
                #           + "{} ".format(self.ccp4_to_np)
                #           + "-m ".format(ccp4_map_path)
                #           + "-o ".format(numpy_path)
                #           + "-v 1",
                #           shell=True,
                #           stdout=PIPE,
                #           stderr=PIPE)

            self.map_paths = pd.Series(numpy_paths)