from gridData.CCP4 import CCP4
from gridData import Grid
import h5py
import numpy as np
from scipy.interpolate import RegularGridInterpolator

from iotbx.file_reader import any_file


import argparse



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-mp",
                        "--map_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/PDK2-x0001/dimple_2mFo-DFc.ccp4")



    args = parser.parse_args()

    return args



if __name__ == "__main__":

    args = parse_args()

    ccp4_map = CCP4(args.map_path)

    print(ccp4_map.array.shape)

    ccp4_map = any_file(str(args.map_path)).file_object
    flex_array = ccp4_map.map_data()
    print("Flex array is of shape {}".format(flex_array.accessor().all()))