

class GenPanDDARunScript:

    def __init__(self, cpus=1, pdb_style="dimple.pdb", mtz_style="dimple.mtz"):

        self.cpus = cpus
        self.pdb_style = pdb_style
        self.mtz_style = mtz_style


    def __call__(self, outlier_dict, directory=None, out_dir=None):

        print(outlier_dict)

        dataset_exclusion_string = ",".join([key for key, value in outlier_dict.items() if value[0] is True])
        print(dataset_exclusion_string)

        executable = "{} ".format("pandda.analyse")
        data_dirs = "{} ".format(directory)
        pdb_style = "{} ".format(self.pdb_style)
        mtz_style = "{} ".format(self.mtz_style)
        cpus = "{} ".format(self.cpus)
        out_dir= "{} ".format(out_dir)
        exclude_from_characterisation = "{} ".format(dataset_exclusion_string)

        run_script = executable + data_dirs + pdb_style + mtz_style + cpus + out_dir + exclude_from_characterisation

        with open("custom_pandda.sh", "w") as run_file:
            run_file.write(run_script)