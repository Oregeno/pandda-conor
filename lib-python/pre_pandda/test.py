from gridData.CCP4 import CCP4
from gridData import Grid
import h5py
import numpy as np
from scipy import interpolate

import argparse

import time as t

from skimage.transform import rescale, resize, downscale_local_mean


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-mp",
                        "--map_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/PDK2-x0001/2fofc.map")

    parser.add_argument("-tmp",
                        "--template_map_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/PDK2-x0337/2fofc.map")

    parser.add_argument("-ml",
                        "--map_label",
                        default="PDK2-x0050")

    parser.add_argument("-fsp",
                        "--file_store_path",
                        default="/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/map_store.hdf5")


    args = parser.parse_args()

    return args



if __name__ == "__main__":

    args = parse_args()

    file_store = h5py.File(args.file_store_path, "r", libver="latest", swmr=True)

    numpy_map = file_store[args.map_label]

    ccp4_map_target = CCP4(args.map_path)
    ccp4_map_template = CCP4(args.template_map_path)

    grid_target = Grid(ccp4_map_target.array, edges = ccp4_map_target.edges)
    grid_template = Grid(ccp4_map_template.array, edges=ccp4_map_template.edges)
    #
    # print(ccp4_map_target.array.shape)
    # print(ccp4_map_target.edges[0].shape)
    # print(ccp4_map_target.edges[0])


    centre_points = grid_target.centers()
    centre_template_points = grid_template.centers()
    # points = [[x[0] for x in centre_points],
    #           [x[1] for x in centre_points],
    #           [x[2] for x in centre_points]]
    # template_points = [[x[0] for x in centre_template_points],
    #           [x[1] for x in centre_template_points],
    #           [x[2] for x in centre_template_points]]

    array = ccp4_map_target.array
    array_shape = array.shape
    pad = 1
    array = array[pad:array_shape[0]- 0 - pad,
            pad:array_shape[1] - 0 - pad,
            pad:array_shape[2] - 0 - pad,]
    array = (array - np.mean(array))/np.std(array)
    print(np.count_nonzero(array))
    print(np.isnan(array).sum())
    print(array.shape)


    print(np.amin(array),
          np.amax(array))

    print(np.where(array == array.max()))
    # image = resize(array,
    #                (array.shape[0]*1.5,
    #                 array.shape[1] * 1.5,
    #                 array.shape[2] * 1.5),
    #                order=2,
    #                mode="reflect")

    image = resize(array,
                   (130,
                    130,
                    94),
                   order=2,
                   mode="reflect")

    image_dummy = np.zeros((132, 132, 96))
    array_shape = image_dummy.shape

    image_dummy[pad +1:array_shape[0]- 0 - pad +1,
            pad-1:array_shape[1] - 0 - pad -1,
            pad:array_shape[2] - 0 - pad] = image

    image = image_dummy


    image = np.swapaxes(image, 0, 1)

    np.save("/dls/labxchem/data/2018/lb19758-9/processing/analysis/initial_model/test_array.npy", image)

    print(ccp4_map_target.origin)
    print(ccp4_map_template.origin)

    exit()

    print(image.shape)
    print(np.amin(image),
          np.amax(image))

    print(np.where(image == image.max()))
    print(np.count_nonzero(image))
    print(np.isnan(image).sum())

    print(array[:,1,1])

    print(image[:,1,1])

    print(array[0:2, 0:2, 0:2])
    print(image[0:4, 0:4, 0:4])

    exit()

    points = np.array(list(centre_points))

    print(points[0])
    print(points[1])

    # exit()
    template_points = np.array(list(centre_template_points))
    print(len(points))
    # exit()
    values = ccp4_map_target.array.flatten()
    print(points.shape)
    print(values.shape)

    interpolate_start = t.time()
    interpolation_function = interpolate.LinearNDInterpolator((points[:,0], points[:, 1], points[:, 2]), values,
                                                              fill_value=0)
    interpolation_function(template_points[:, 0], template_points[:, 1], template_points[:, 2])
    interpolate_finish = t.time()

    print(interpolation_function[0])
    print("Interpolated in {}".format(interpolate_finish - interpolate_start))
    print()

    exit()

    grid_resampled = grid_target.resample(grid_template)

    print(grid_resampled.grid.shape,
          np.allclose(numpy_map,
                      grid_resampled.grid))



    print("Grid first sector")
    print(grid_resampled.grid[1, 1, :])
    print("hdf5 first sector")
    print(numpy_map[1, 1, :])
    print("Grid first column")
    print(grid_resampled.grid[1, :, 1])
    print("hdf5 first column")
    print(numpy_map[1, :, 1])
    print("Grid first row")
    print(grid_resampled.grid[:, 2, 2])
    print("hdf5 first row")
    print(numpy_map[:, 2, 2])

    print("target ccp4 range")
    print(np.amin(ccp4_map_target.array), np.amax(ccp4_map_target.array))

    print("template ccp4 range")
    print(np.amin(ccp4_map_template.array), np.amax(ccp4_map_template.array))

    print("target resampled")
    print(np.amin(grid_resampled.grid[2:-2]), np.amax(grid_resampled.grid[2:-2]))