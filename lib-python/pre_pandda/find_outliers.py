import pandas as pd

from sklearn.decomposition import PCA

from sklearn.cluster import DBSCAN
from sklearn.ensemble import IsolationForest

import time
import numpy as np

import pathlib as p

import plots

import array_split

# import hdbscan

from scipy.stats import mode

import h5py

import fancy_printing as fp

from gridData.CCP4 import CCP4


import pathos.pools as pp

import psutil

from sklearn.preprocessing import StandardScaler


class OutlierFinder:

    def __init__(self, xyz, num_components, method = "dbscan"):

        self.xyz = xyz
        self.num_components = num_components


        self.do_local_outlier_detection = True
        self.method = method

        self.outliers = None

        self.outlier_dict = None

    def count_dataset_outliers(self, outlier_array, indexes, included):

        print("\tChecking which datasets have outliers...")

        # print(indexes, len(indexes))
        # print(outlier_array[0, 0, 0, :], len(outlier_array))
        # print(included)

        outlier_list = []
        print(indexes)

        # print()

        for i, include in enumerate(included):
            outliers = np.count_nonzero(outlier_array[:, :, :, indexes[i]] == False )
            # print(outliers)
            if include == False:
                outlier_list.append(-1)
            else:
                outlier_list.append(outliers)

        print(outlier_list, len(outlier_list))
        print(np.mean(outlier_list), np.std(outlier_list))
        print(np.sum(outlier_array))
        print(np.sum(outlier_list))


        return outlier_list

    def non_outlying_datasets_in_apo_class(self, outlier_array, apo_mask):

        print("\tChecking which datasets have outliers...")

        # print(indexes, len(indexes))
        # print(outlier_array[0, 0, 0, :], len(outlier_array))
        # print(included)

        outlier_list = []

        # print()

        output = np.zeros(outlier_array.shape)

        dummy = np.zeros((output.shape[0],
                          output.shape[1],
                          output.shape[2]))

        for idx, x in np.ndenumerate(dummy):

            outlier_array = outlier_array[idx[0], idx[1], idx[2], :]
            apos = outlier_array[apo_mask]
            non_outlying = apos[apos != -1]
            modal_cluster = mode(non_outlying).mode
            print("modal apo cluster is {}".format(modal_cluster))
            outlier_array[outlier_array == modal_cluster]
            print("at idx {} there are {} datasets in apo cluster. There were {} apos".format(idx,
                                                                                              np.count_nonzero(outlier_array),
                                                                                              np.count_nonzero(apo_mask)))
            output[idx[0], idx[1], idx[2], :] = outlier_array

        return output

    def validate_input(self, maps, output=None, max_chunks=1000):

        # Validation
        assert type(max_chunks) is int
        assert max_chunks >= 1
        assert ((type(maps) == type([]))
                or (type(maps) == type(p.Path("/")))
                or (type(maps)==str)
                or (type(maps)==type(pd.Series)))

        # Type fixing
        if (type(maps) == str):
            maps = p.Path(maps)
        if (type(maps) == type([])):
            maps = pd.Series(maps)

        if type(maps) == type(p.Path("/")):
            input_type="hdf5"
        else:
            input_type = "ccp4"

        return maps, input_type

    def check_output(self, maps, keys, output):

        input_mask = [(output / "outlier_array.npy").exists()]

        return input_mask

    def global_outlier_dection(self):
        pass

    def open_tables(self, file_store_path):
        file_store = h5py.File(file_store_path, "r", libver="latest", swmr=True)

        return [file_store[crystal] for crystal in file_store.keys()]

    def local_outlier_detection_dep(self, args, max_chunks, output):
        print("Process starting")
        print([type(arg) for arg in args])

        #
        chunk_num = args[0]
        idx_chunk_indicies = args[1]
        map_chunks_list = [ed_map[0] for ed_map in args[2]]
        map_ids = [ed_map[1] for ed_map in args[2]]


        idx = idx_chunk_indicies[0]
        chunk_indicies = idx_chunk_indicies[1]

        if chunk_num >= max_chunks:
            # print("\tMax chunks reached, breaking!")
            return

        # tables = self.open_tables(file_store_path)

        # print("loaded tables")

        print("\tWorking on chunk number {} with indicies {}".format(chunk_num, idx))



        # print(chunk)
        # print(tables[0][chunk[0], chunk[1], chunk[2]])
        # print(tables)
        # print(tables[0][0,0,0])

        # load_data_start = time.time()
        # map_chunks_list = [ed_map[chunk_indicies[0],
        #                           chunk_indicies[1],
        #                           chunk_indicies[2]].flatten().reshape(1, -1) for ed_map in tables]
        # load_data_finish = time.time()
        # print("\t\tLoaded data in {}".format(load_data_finish - load_data_start))

        map_chunks_list = [ed_map.flatten().reshape(1,-1) for ed_map in map_chunks_list]

        sample_by_feature = np.vstack(map_chunks_list)
        # print("\t\tShape of sample by feature array is  {}".format(sample_by_feature.shape))



        if self.method == "dbscan":

            # print("\t\tPerforming PCA on {} components!".format(self.num_components))
            pca_start = time.time()
            pca = PCA(n_components=self.num_components)
            components = pca.fit_transform(sample_by_feature)
            pca_finish = time.time()
            scalar = StandardScaler()
            components = scalar.fit_transform(components)
            # print("\t\tFinished PCA in {}".format(pca_finish - pca_start))

            dbscan_start = time.time()
            clusterer = DBSCAN(eps=2.0, min_samples=5).fit(components)
            outlier = clusterer.labels_
            dbscan_finish = time.time()
            # print("\t\tPerformed outleir detection in {}. Found {} outliers!".format(dbscan_finish - dbscan_start,
            #                                                                          np.count_nonzero(outlier == -1)))
            count = 0
            if output != None:
                if output != None:
                    plots.save_coloured_scatter(components[:, 0],
                                                components[:, 1],
                                                outlier,
                                                str(output / "chunk_{}_{}_{}_pca_{}.png".format(idx[0], idx[1],
                                                                                                idx[2], count)),
                                                np.array(map_ids))

            outliers_array = outlier
            outlier_mask = np.array([True if value != -1 else False for value in outlier])



            # print(outlier_mask)

            # print(np.count_nonzero(clusterer.labels_ < 0))

            while np.count_nonzero(clusterer.labels_ < 0) > 0:

                count = count + 1

                # print("\t\tStill outliers! Reclustering!")

                # print("\t\tPerforming PCA on {} components!".format(self.num_components))
                pca_start = time.time()
                pca = PCA(n_components=self.num_components)
                components = pca.fit_transform(sample_by_feature[outlier_mask])
                pca_finish = time.time()
                scalar = StandardScaler()
                components = scalar.fit_transform(components)
                # print("\t\tFinished PCA in {}".format(pca_finish - pca_start))

                dbscan_start = time.time()
                clusterer = DBSCAN(eps=2.0, min_samples=5).fit(components)
                outlier = clusterer.labels_
                dbscan_finish = time.time()
                # print("\t\tPerformed outleir detection in {}. Found {} outliers!".format(dbscan_finish - dbscan_start,
                #                                                                          np.count_nonzero(
                #                                                                              outlier == -1)))
                # print(components.shape, outlier.shape)
                if output != None:
                    plots.save_coloured_scatter(components[:, 0],
                                                components[:, 1],
                                                outlier,
                                                str(output / "chunk_{}_{}_{}_pca_{}.png".format(idx[0], idx[1],
                                                                                                         idx[2], count)),
                                                np.array(map_ids)[outlier_mask])

                    # print("\t\tFinished making plots")

                # print(chunk_indicies, outlier.shape)
                # print(chunk_indicies,outlier_mask.shape)
                # print(chunk_indicies,outlier_mask[outlier_mask].shape)
                outlier_mask_copy = outlier_mask.copy()
                new_outliers = np.array([True if value != -1 else False for value in outlier]).copy()
                # print(new_outliers, new_outliers.shape)
                outliers_array[outlier_mask_copy] = outlier
                outlier_mask[outlier_mask_copy] = new_outliers
                # outlier_mask[outlier_mask] = np.array([True if value != -1 else False for value in outlier])
                # print(outlier_mask)
                # print(outlier_mask.shape)
                # print(np.count_nonzero(clusterer.labels_ < 0))

                # count = count+ 1

            # print(outlier_mask)
            print("After detecting outliers for {} there are {} left".format(idx, np.count_nonzero(outlier_mask)))
            print("The number of unique classes is {}".format(np.unique(outliers_array)))


        #
        # print("\t\tPerforming PCA on {} components!".format(self.num_components))
        # pca_start = time.time()
        # pca = PCA(n_components=self.num_components)
        # components = pca.fit_transform(sample_by_feature)
        # pca_finish = time.time()
        # scalar = StandardScaler()
        # components = scalar.fit_transform(components)
        # print("\t\tFinished PCA in {}".format(pca_finish - pca_start))
        # #
        # # print(components.shape)
        # # print(components[0,:])
        #
        # if self.method == "isolation_forest":
        #
        #     iso_start = time.time()
        #     iso = IsolationForest(behaviour="new",
        #                           # max_samples=1000,
        #                           contamination=0.03)
        #     outlier = iso.fit(components).predict(components)
        #     iso_finish = time.time()
        #     print("\t\tPerformed outleir detection in {}. Found {} outliers!".format(iso_finish - iso_start,
        #                                                                              np.count_nonzero(outlier == -1)))
        #
        # elif self.method == "dbscan":
        #     dbscan_start = time.time()
        #
        #     clusterer = DBSCAN(eps=1.5, min_samples=5).fit(components)
        #
        #     outlier = clusterer.labels_
        #     dbscan_finish = time.time()
        #     print("\t\tPerformed outleir detection in {}. Found {} outliers!".format(dbscan_finish - dbscan_start,
        #                                                                              np.count_nonzero(outlier == -1)))
        #
        # if output != None:
        #     plots.save_coloured_scatter(components[:, 0], components[:, 1], outlier,
        #                                 str(output / "chunk_{}_{}_{}_pca_no_outliers.png".format(idx[0], idx[1],
        #                                                                                          idx[2])))
        #
        #
        #
        # # plots.save_coloured_scatter(components[:, 0], components[:, 1], outlier,  # [table[1] for table in tables],
        # #                             output + "chunk_{}_{}_{}_pca.png".format(idx[0], idx[1], idx[2]))
        #
        # print("\t\tPerforming PCA WITHOUT OUTLIERS on {} components!".format(self.num_components))
        # pca_start = time.time()
        # pca = PCA(n_components=self.num_components)
        # components = pca.fit(sample_by_feature[outlier == 1]).transform(sample_by_feature[outlier == 1])
        # pca_finish = time.time()
        # print("\t\tFinished PCA in {}".format(pca_finish - pca_start))
        #
        # scalar = StandardScaler()
        # components = scalar.fit_transform(components)
        #
        #
        #
        # if output != None:
        #     plots.save_coloured_scatter(components[:, 0], components[:, 1], outlier,
        #                                 str(output / "chunk_{}_{}_{}_pca_no_outliers.png".format(idx[0], idx[1],
        #                                                                                          idx[2])))

        # detector = svm.OneClassSVM(nu=0.05, kernel="rbf", gamma="auto")
        #
        # outlier = detector.fit(components).predict(components)

        # clusterer = DBSCAN(eps=6, min_samples=5).fit(components)



        # print((idx[0], idx[1], idx[2], outlier))

        return (idx[0], idx[1], idx[2], outlier_mask, outliers_array)

        # outliers[idx[0], idx[1], idx[2], :] = outlier

    def local_outlier_detection(self, args, max_chunks, output, apo_array):
        print("Process starting")
        print([type(arg) for arg in args])

        apo_array = np.array(apo_array)

        #
        chunk_num = args[0]
        idx_chunk_indicies = args[1]
        map_chunks_list = [ed_map[0] for ed_map in args[2]]
        map_ids = [ed_map[1] for ed_map in args[2]]
        idx = idx_chunk_indicies[0]

        if chunk_num >= max_chunks:
            return

        print("\tWorking on chunk number {} with indicies {}".format(chunk_num, idx))
        map_chunks_list = [ed_map.flatten().reshape(1,-1) for ed_map in map_chunks_list]
        sample_by_feature = np.vstack(map_chunks_list)


        if self.method == "dbscan":

            pca = PCA(n_components=self.num_components)
            components = pca.fit_transform(sample_by_feature)
            scalar = StandardScaler()
            components = scalar.fit_transform(components)
            clusterer = DBSCAN(eps=1.75, min_samples=5).fit(components)
            outlier = clusterer.labels_

            apo_modal_cluster = mode(outlier[apo_array]).mode

            count = 0
            if output != None:
                if output != None:
                    plots.save_coloured_scatter(components[:, 0],
                                                components[:, 1],
                                                outlier,
                                                str(output / "chunk_{}_{}_{}_pca_{}.png".format(idx[0], idx[1],
                                                                                                idx[2], count)),
                                                np.array(map_ids))

            outliers_array = outlier
            outlier_mask = np.array([True if value == apo_modal_cluster else False for value in outlier])
            print(idx, "outleir array before loop", len(outliers_array))
            print(idx, "outlier mask before while loop", len(outlier_mask))

            count = count + 1

            if output != None:
                plots.save_coloured_scatter(components[:, 0][outlier_mask],
                                            components[:, 1][outlier_mask],
                                            outlier[outlier_mask],
                                            str(output / "chunk_{}_{}_{}_pca_{}.png".format(idx[0], idx[1],
                                                                                            idx[2], count)),
                                            np.array(map_ids)[outlier_mask])

            print("before while loop", len(np.unique(clusterer.labels_ )))

            while len(np.unique(clusterer.labels_ )) > 1:

                count = count + 1

                pca = PCA(n_components=self.num_components)
                components = pca.fit_transform(sample_by_feature[outlier_mask])
                scalar = StandardScaler()
                components = scalar.fit_transform(components)
                clusterer = DBSCAN(eps=1.75, min_samples=5).fit(components)
                outlier = clusterer.labels_
                print(idx, "getting apo modal cluster")

                print(len(outlier))
                print(len(apo_array))
                print(len(outlier_mask))
                apo_masked = apo_array[outlier_mask]
                print(apo_masked[0:10])
                apo_modal_cluster = mode(outlier[apo_masked]).mode


                outlier_mask_copy = outlier_mask.copy()
                print(idx, "outlier mask", len(outlier_mask))
                new_outliers = np.array([True if value == apo_modal_cluster else False for value in outlier]).copy()
                print(idx, "new outliers", len(new_outliers))
                print(idx, "outleir array", len(outliers_array))
                outliers_array[outlier_mask_copy] = outlier
                print(idx, "outlier mask", len(outlier_mask))

                outlier_mask[outlier_mask_copy] = new_outliers


                if output != None:
                    plots.save_coloured_scatter(components[:, 0],
                                                components[:, 1],
                                                outlier,
                                                str(output / "chunk_{}_{}_{}_pca_{}.png".format(idx[0], idx[1],
                                                                                                         idx[2], count)),
                                                np.array(map_ids)[outlier_mask])


            print("After detecting outliers for {} there are {} left".format(idx, np.count_nonzero(outlier_mask)))
            print("The number of unique classes is {}".format(np.unique(outliers_array)))



        return (idx[0], idx[1], idx[2], outlier_mask)

    def make_tables(self, maps, input_type, keys=None, apo_map=None):

        if input_type=="hdf5":
            file_store_path = maps
            file_store = h5py.File(file_store_path, "r")
            untruncated_tables = [(crystal, file_store[crystal]) for crystal in file_store.keys() if crystal in list(keys)]
            apos = [True if apo_map[crystal] is True else False for crystal in file_store.keys() if crystal in list(keys)]
            keys = file_store.keys()

        elif input_type=="ccp4":
            untruncated_tables = []
            for ccp4_map_path in maps:
                path = p.Path(ccp4_map_path)
                if path.exists():
                    try:
                        g = CCP4(path)
                        untruncated_tables.append(g)
                    except:
                        untruncated_tables.append(None)
                else:
                    untruncated_tables.append(None)

        return untruncated_tables, keys, apos

    def __call__(self, maps, output=None, keys=None, max_chunks=1000, overwrite=True, apos=None, crystals=None, comparison_class="apo"):

        fp.print_task_title("Finding outliers...")

        _, input_type = self.validate_input(maps, output, max_chunks)
        # if all(input_mask) and (overwrite == False):
        #     print("AAlready made output and not forced to overwite, exiting")

        apo_dict = {crystal: (True if (map_class ==comparison_class) else False) for crystal, map_class in zip(crystals, apos)}

        untruncated_tables, keys, apo_map = self.make_tables(maps, input_type, keys=keys, apo_map=apo_dict)



        table_shape_mode = mode([array[1].shape for array in untruncated_tables]).mode


        print("\tBefore truncation there are {} datasets. Array shape mode is {}. Truncating...".format(len(untruncated_tables),
                                                                                                        table_shape_mode))

        tables = [table for table in untruncated_tables if
                  np.array_equal(np.array(table[1].shape).reshape(-1, ), np.array(table_shape_mode).reshape(-1, ))]

        included = [True if np.array_equal(np.array(table[1].shape).reshape(-1, ), np.array(table_shape_mode).reshape(-1,))
                      else False for i, table in enumerate(untruncated_tables) ]

        included_num = 0
        indexes = []
        for i, include in enumerate(included):
            if include == True:
                indexes.append(included_num)
                included_num = included_num + 1
            else:
                indexes.append(None)

        print("\t\tAfter truncating {} datasets remain.".format(len(tables)))

        partitioning = array_split.shape_split(tables[0][1].shape,
                                               axis=[self.xyz[0],
                                                     self.xyz[1],
                                                     self.xyz[2]])

        outliers = np.zeros((partitioning.shape[0],
                            partitioning.shape[1],
                            partitioning.shape[2],
                            len(tables)))



        if self.do_local_outlier_detection is True:
            func = lambda arguments: self.local_outlier_detection(arguments, max_chunks, output, apo_map)
            print("\tsetting up arguments...")

            chunk_indicies = np.ndenumerate(partitioning).next()

            # args = [0, np.ndenumerate(partitioning).next(), [(ed_map[1][chunk_indicies[1][0],
            #                                         chunk_indicies[1][1],
            #                                         chunk_indicies[1][2]], ed_map[0]) for ed_map in tables]]
            #
            # func(args)
            #
            # exit()

            args = [[i, chunk_indicies, [(ed_map[1][chunk_indicies[1][0],
                                                    chunk_indicies[1][1],
                                                    chunk_indicies[1][2]], ed_map[0]) for ed_map in tables]]
                    for i, chunk_indicies in enumerate(np.ndenumerate(partitioning))]
            print("\tProcessing...")
            pool = pp.ProcessPool(processes=psutil.cpu_count())
            # pool = pp.ProcessPool(processes=1)
            #
            outlying = pool.map(func,
                                args)
            # print(outlying)
            for o in outlying:
                if o is not None:
                    outliers[o[0], o[1], o[2], :] = o[3]

            np.save(output / "outlier_array.npy", outliers)



            # outlier_array = self.non_outlying_datasets_in_apo_class(outliers,
            #                                                         apo_map)

            outlier_list = self.count_dataset_outliers(outliers,
                                                  indexes,
                                                  included)
            outlier_mean = np.mean(outlier_list)
            outlier_std = np.std(outlier_list)

            self.outlier_dict = {key: (True, value) if value > outlier_mean else (False, value)
                                 for key, value in zip([table[0] for table in tables], outlier_list)}

            # print("The outlier dictionary is...")
            # def sort_key(x):
            #     return x[0]
            # key_list = [(key, value) for key, value in self.outlier_dict.items()]
            # for key in sorted(self.outlier_dict):
            #     print(key, self.outlier_dict[key])


        self.outliers = pd.Series(outlier_list)
