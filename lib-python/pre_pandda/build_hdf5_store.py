import h5py
import time
import numpy as np

import fancy_printing as fp
import os

class Hdf5StoreBuilder:

    def __init__(self, overwrite=True):

        self.hdf5_file = None
        self.overwite = overwrite

    def __call__(self, crystal_labels, numpy_map_paths, hdf5_store):

        fp.print_task_title("Building hdf5 store...")


        print(hdf5_store)

        if self.overwite is True:
            try:
                os.remove(str(hdf5_store))

            except:
                print("Could not remove: probably didn't make file yet!")


        self.hdf5_file = hdf5_store

        f = h5py.File(self.hdf5_file, "a")

        print("Generating hdf5 maps...")
        gen_map_start = time.time()

        count = 0.0
        for crystal_label, numpy_map_path in zip(crystal_labels, numpy_map_paths):
            count = count + 1
            print "Added {} of {} datasets to store\r".format(count, len(crystal_label)),
            if numpy_map_path is None:
                pass
                # print("\tNo Numpy map to load!")

            elif numpy_map_path == "NUMPY_MAP_NOT_FOUND":
                pass
            elif crystal_label in f:
                pass
                # print("\tAlready added dataset. Skipping.")

            else:
                f.create_dataset("{}".format(crystal_label),
                                 data=np.load(numpy_map_path),
                                 chunks=True)

        gen_map_finish = time.time()
        print("\n\tGenerated store in {}".format(gen_map_finish - gen_map_start))