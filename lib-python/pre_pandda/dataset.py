import pandas as pd
import pathlib as p

import fancy_printing as fp


class Dataset:

    def __init__(self, num_samples, mtz_nomenclature, infer_apo, pdb_nomenclature="dimple.pdb", output=None,
                 ccp4_map_nomenclature="2fofc.map"):

        self.directory = None
        self.num_samples = num_samples
        self.df = 1
        self.mtz_nomenclature = mtz_nomenclature
        self.pdb_nomenclature = pdb_nomenclature
        self.ccp4_map_nomenclature = ccp4_map_nomenclature
        self.infer_apo = infer_apo

    def parse_pandda_directory(self, directory):

        print("\tPopulating database...")

        self.directory = directory
        self.get_crystal_paths()
        self.get_mtzs()
        self.get_apo()
        self.get_modelled()

        if self.num_samples != "all":
            self.df = self.df.head(self.num_samples)

        print("\tAfter truncations {} datasets remain".format(len(self.df)))

    def parse_initial_models(self, directory, output, reparse=True):

        fp.print_task_title("Generating dataset table")

        self.check_output(output, reparse)
        if (self.df != 1) & (reparse is False):
            return

        self.directory = directory
        self.get_initial_model_crystal_paths()
        self.get_initial_model_mtzs()
        self.get_initial_model_pdbs()
        self.get_initial_model_ccp4_maps()
        if self.infer_apo is True: self.get_initial_model_apo()
        if self.num_samples != "all": self.df = self.df.head(self.num_samples)
        print("\tAfter truncations {} datasets remain".format(len(self.df)))
        self.output(output)


    def output(self, output):
        self.df.to_csv(str(output / "dataset_table.csv"))

    def check_output(self, output, reparse):
        if reparse is True:
            print("Checking if dataaset table in output directory.")
            try:
                self.df = pd.read_csv(output / "dataset_table.csv")
                print("\tFound dataset table. Reparse is False so Exiting.")

            except:
                print("\tNo existing dataset table found. Generating one.")

    def get_initial_model_crystal_paths(self):
        print("Finding datasets...")
        processed_datasets = p.Path(self.directory)
        crystals = processed_datasets.glob("*")
        datasets = [{"crystal": crystal.name,
                     "path": crystal,
                     "class": None} for crystal in crystals if crystal.is_dir()]

        df = pd.DataFrame(datasets)
        self.df = df
        print("\t found {} datasets".format(len(datasets)))

    def get_initial_model_mtzs(self):
        print("Getting mtz paths...")
        mtz_paths = []
        for path in self.df["path"]:
            mtz_path_glob = path.glob(self.mtz_nomenclature)
            try:
                mtz_paths.append(mtz_path_glob.next())
            except Exception as e:
                # print("Could not find {} in crystal directory!".format(self.mtz_nomenclature))
                mtz_paths.append("MTZ_NOT_FOUND")

        self.df["mtz_path"] = pd.Series(mtz_paths)

        if self.df[self.df["mtz_path"] == "MTZ_NOT_FOUND"].count()["mtz_path"] != 0:
            fp.print_warning("\tWARNING: missing mtzs!")
            print("\t\tNumber of crystals in initial models is {}.".format(self.df.count()["mtz_path"]))
            print("\t\tNumber of missing mtzs is {}.".format(self.df[self.df["mtz_path"] == "MTZ_NOT_FOUND"].count()["mtz_path"]))

    def get_initial_model_pdbs(self):
        print("Getting pdb paths...")
        pdb_paths = []
        for path in self.df["path"]:
            pdb_path_glob = path.glob(self.pdb_nomenclature)
            try:
                pdb_paths.append(pdb_path_glob.next())
            except Exception as e:
                # print("Could not find {} in crystal directory!".format(self.mtz_nomenclature))
                pdb_paths.append("PDB_NOT_FOUND")

        self.df["pdb_path"] = pd.Series(pdb_paths)

        if self.df[self.df["pdb_path"] == "PDB_NOT_FOUND"].count()["pdb_path"] != 0:
            fp.print_warning("\tWARNING: missing pdbs!")
            print("\t\tNumber of crystals in initial models is {}.".format(self.df.count()["pdb_path"]))
            print("\t\tNumber of missing pdbs is {}.".format(self.df[self.df["pdb_path"] == "PDB_NOT_FOUND"].count()["pdb_path"]))

    def get_initial_model_ccp4_maps(self):
        print("Getting ccp4 map paths...")
        ccp4_map_paths = []
        for path in self.df["path"]:
            ccp4_map_path_glob = path.glob(self.ccp4_map_nomenclature)
            try:
                ccp4_map_paths.append(ccp4_map_path_glob.next())
            except Exception as e:
                # print("Could not find {} in crystal directory!".format(self.mtz_nomenclature))
                ccp4_map_paths.append("CCP4_MAP_NOT_FOUND")

        self.df["ccp4_map_path"] = pd.Series(ccp4_map_paths)

        if self.df[self.df["ccp4_map_path"] == "CCP4_MAP_NOT_FOUND"].count()["ccp4_map_path"] != 0:
            fp.print_warning("\tWARNING: missing ccp4 maps!")
            print("\t\tNumber of crystals in initial models is {}.".format(self.df.count()["ccp4_map_path"]))
            print("\t\tNumber of missing pdbs is {}.".format(self.df[self.df["ccp4_map_path"] == "CCP4_MAP_NOT_FOUND"].count()["ccp4_map_path"]))


    def get_initial_model_apo(self):
        print("Infer_apo is True. Will infer apo datasets from absence of CIF file in crystal directory.")
        apo = ["apo" if len(list((path).glob("*.cif"))) == 0 else None for path in self.df["path"]]
        self.df["class"] = self.df["class"].combine(pd.Series(apo), lambda x1, x2: x2 if x2 is not None else x1)
        # print(self.df.head(50))
        print("\tInferred apo datasets. Found {} apo datasets (datasets without cif files present).".format(self.df[self.df["class"]=="apo"].count()["class"]))


    def get_crystal_paths(self):

        print("Finding datasets...")
        processed_datasets = p.Path(self.directory) / "processed_datasets"
        crystals = processed_datasets.glob("*")
        datasets = [{"crystal": crystal.name,
                     "path": crystal,
                     "class": None} for crystal in crystals if crystal.is_dir()]

        df = pd.DataFrame(datasets)
        self.df = df
        print("\t found {} datasets".format(len(datasets)))

    def get_mtzs(self):

        mtz_paths = [path.glob("*.mtz").next() for path in self.df["path"]]

        self.df["mtz_path"] = pd.Series(mtz_paths)

    def get_apo(self):

        apo = ["apo" if len(list((path / "ligand_files").glob("*"))) != 0 else None for path in self.df["path"]]

        self.df["class"] = self.df["class"].combine(pd.Series(apo), lambda x1, x2: x2 if x2 is not None else x1)

    def get_modelled(self):

        modelled_structures = ["modelled" if len(list((path / "modelled_structures").glob("*"))) != 0 else None for path in self.df["path"]]

        self.df["class"] = self.df["class"].combine(pd.Series(modelled_structures), lambda x1, x2: x2 if x2 is not None else x1)


