from  subprocess import Popen, PIPE
import pandas as pd

import pathlib as p

import fancy_printing as fp

from iotbx.file_reader import any_file


class CCP4MapBuilder():

    def __init__(self, resolution_cutoff_high=3.0, grid_resolution_factor=0.5/3.0,
                 prebuilt_ccp4_map_nomenclature=None, ccp4_map_nomenclature="2fofc.map",
                 overwrite=False):

        self.resolution_cutoff_high = resolution_cutoff_high
        self.grid_resolution_factor = grid_resolution_factor
        self.map_paths = None

        self.overwrite=overwrite

    def make_ccp4_map(self, gridding=None, mtz_path=None):

        print("WOULD GENERATE A MAP")


        # p = Popen("phenix.mtz2map "
        #           + "mtz_file={} ".format(mtz_path)
        #           + "d_min={} ".format(self.resolution_cutoff_high)
        #           + "grid_resolution_factor={} ".format(self.grid_resolution_factor)
        #           + "directory={} ".format(mtz_path.parent),
        #           shell=True,
        #           stdout=PIPE,
        #           stderr=PIPE)
        # print(p.communicate())

        ccp4_python = "/dls/science/groups/i04-1/conor_dev/ccp4/build/bin/cctbx.python"
        program = "/home/zoh22914/pandda-conor/lib-python/pre_pandda/mtz2map.py"

        if gridding is None:
            command = "{} ".format(ccp4_python) + "{} ".format(program) + "{} ".format(
                mtz_path) + "directory={} ".format(mtz_path.parent)

            print(command)

            p = Popen(command,
                      shell=True,
                      stdout=PIPE,
                      stderr=PIPE)
            string = p.communicate()

        else:

            command = "{} ".format(ccp4_python)+ "{} ".format(program) + "{} ".format(mtz_path)+ "gridding={},{},{} ".format(gridding[0] ,
                                                        gridding[1],
                                                        gridding[2]) + "directory={} ".format(mtz_path.parent)

            print(command)

            p = Popen(command,
                      shell=True,
                      stdout=PIPE,
                      stderr=PIPE)
            string = p.communicate()



        return mtz_path.parent / (mtz_path.stem + "_2mFo-DFc.ccp4")

    def __call__(self, mtz_paths, ccp4_map_paths=None, gridding=None):

        fp.print_task_title("Building ccp4 maps...")

        # Get appropriate gridding


        template_map_path = mtz_paths[mtz_paths != "MTZ_NOT_FOUND"].iloc[0]
        output_map_path = template_map_path.parent / (template_map_path.stem + "_2mFo-DFc.ccp4")

        self.make_ccp4_map(mtz_path=template_map_path)

        ccp4_map = any_file(str(output_map_path)).file_object
        flex_array = ccp4_map.map_data()
        gridding = flex_array.accessor().all()

        print(gridding)


        if ccp4_map_paths is not None:

            no_missing_mtz = ccp4_map_paths[mtz_paths != "MTZ_NOT_FOUND"]

            num_missing_maps = no_missing_mtz[no_missing_mtz == "CCP4_MAP_NOT_FOUND"].count()

            print("\tFound {} maps with mtzs but no associated ccp4 map.".format(num_missing_maps))


            failed = 0
            new_ccp4_map_paths = []
            for mtz_path, ccp4_map_path in zip(mtz_paths, ccp4_map_paths):

                if type(ccp4_map_path) != type(p.PosixPath):
                    ccp4_map_path = p.Path(ccp4_map_path)
                if type(mtz_path) != type(p.PosixPath):
                    mtz_path = p.Path(mtz_path)

                if ccp4_map_path.exists():
                    new_ccp4_map_paths.append(ccp4_map_path)
                elif (mtz_path.parent / (mtz_path.stem + "_2mFo-DFc.ccp4")).exists():
                    new_ccp4_map_paths.append(mtz_path.parent / (mtz_path.stem + "_2mFo-DFc.ccp4"))
                elif mtz_path.exists():
                    try:
                        new_path = self.make_ccp4_map(mtz_path=mtz_path)
                        new_ccp4_map_paths.append(new_path)
                    except Exception as e:
                        failed += 1
                        print(e)
                        new_ccp4_map_paths.append("CCP4_MAP_NOT_FOUND")
                else:
                    new_ccp4_map_paths.append("CCP4_MAP_NOT_FOUND")


            print("\t{} maps failed to build".format(failed))

            self.map_paths = pd.Series(new_ccp4_map_paths)

        # If no maps are provided
        else:
            map_paths = []
            for mtz_path in mtz_paths:

                print(mtz_path)

                if str(mtz_path) == "MTZ_NOT_FOUND":
                    map_paths.append("CCP4_MAP_NOT_FOUND")
                    continue

                assert gridding is not None, "No gridding supplied!"

                map_path = (mtz_path.parent / (mtz_path.stem + "_2mFo-DFc.ccp4"))
                map_paths.append(map_path)

                if map_path.exists() and (self.overwrite is False):
                    print("\t{} already has a ccp4 map. Skipping.".format(mtz_path.parent.name))
                    continue



                self.make_ccp4_map(gridding=gridding,
                                   mtz_path=mtz_path)
                print("\tConverting map at {}...".format(mtz_path))

            self.map_paths = pd.Series(map_paths)

            print(self.map_paths)

