import argparse

import pandas as pd
from biopandas.pdb import PandasPdb


def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_pdb_path",
                        default="/dls/labxchem/data/2017/lb18145-3/processing/analysis/initial_model/NUDT7A-x0125/refine.split.bound-state.pdb")
    parser.add_argument("--output_path",
                        default="~/")

    args = parser.parse_args()

    return args


def run(input_pdb_path, output_path):


    ##############################
    # Ligand
    ###############################

    ligand_pdb = PandasPdb().read_pdb(input_pdb_path)

    ligand_pdb.df["ATOM"] ==  pd.DataFrame(columns=ligand_pdb.df["ATOM"].columns)


    ligand_pdb.to_pdb(path= output_path + 'ligand.pdb',
                      records=None,
                      gz=False,
                      append_newline=True)

    ###################################
    # Receptor
    ####################################

    receptor_pdb = PandasPdb().read_pdb(input_pdb_path)

    receptor_pdb.df["HETATOM"] == pd.DataFrame(columns=receptor_pdb.df["HETATOM"].columns)


    receptor_pdb.to_pdb(path= output_path + 'receptor.pdb',
                        records=None,
                        gz=False,
                        append_newline=True)


if __name__ == "__main__":

    args = parse_args()

    run(args.input_pdb_path, args.output_path)


