import argparse

import scitbx
import iotbx
from iotbx.file_reader import any_file
from scitbx.array_family import flex
import os

import numpy as np


def make_grid(sample_size):

    x = np.linspace(-1, 1, sample_size[0])
    y = np.linspace(-1, 1, sample_size[1])
    z = np.linspace(-1, 1, sample_size[2])

    xx, yy, zz = np.meshgrid(x, y, z)

    return [xx, yy, zz]


def make_synthetic_data(number_samples, sample_size):

    grid = make_grid(sample_size)

    distribution_a = lambda x, y, z: np.random.normal(1/(5*(1 + x**2 + y**2 + z**2)), 0.01)
    distribution_b = lambda x, y, z: np.random.normal(1/(1+50*(x-0.8)**2 + 50*(y-0.8)**2 + 50*(z-0.8)**2), 0.01)

    samples = []
    for i in range(number_samples):

        sample = distribution_a(grid[0], grid[1], grid[2])

        samples.append(sample)

    samples[0] = samples[0] + 2*distribution_b(grid[0], grid[1], grid[2])


    return samples

#/dls/labxchem/data/2017/lb18145-3/processing/analysis/panddas.2017-09-10/processed_datasets/NUDT7A-x0343/NUDT7A-x0343-ground-state-mean-map.native.ccp4

def load_ccp4_template(ccp4_map_template_path):
    test_map_file = any_file(ccp4_map_template_path)
    test_map = test_map_file.file_object

    return test_map


def make_syntheic_flex_arrays(sythentic_data_np):

    flex_arrays = []

    for sample in sythentic_data_np:
        flex_arrays.append(flex.double(sample))

    return flex_arrays


def write_ccp4_maps(ccp4_template_map, sythentic_data_ccp4, output_dir):

    for i, flex_array in enumerate(sythentic_data_ccp4):

        output_path = output_dir + "test_map_{}.ccp4"

        iotbx.ccp4_map.write_ccp4_map(space_group=ccp4_template_map.crystal_symmetry().space_group(),
                                      unit_cell=ccp4_template_map.unit_cell(),
                                      map_data=flex_array,
                                      file_name=output_path.format(i),
                                      labels=scitbx.array_family.flex.std_string(['Output map']))



def run(ccp4_map_template_path=None, number_samples=0, sample_size=(1, 1, 1), output_dir=None):

    sythentic_data_np = make_synthetic_data(number_samples, sample_size)

    print("#######Sythnetic data 1########")
    print(sythentic_data_np[0])
    print("#######Sythnetic data 2########")
    print(sythentic_data_np[1])

    ccp4_template = load_ccp4_template(ccp4_map_template_path)

    sythentic_data_ccp4 = make_syntheic_flex_arrays(sythentic_data_np)

    write_ccp4_maps(ccp4_template, sythentic_data_ccp4, output_dir)

    return 1


def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("--ccp4_map_template_path", default="")
    parser.add_argument("--number_samples", type=int, default="10")
    parser.add_argument("--x", type=int, default=20)
    parser.add_argument("--y", type=int, default=20)
    parser.add_argument("--z", type=int, default=20)
    parser.add_argument("--output_dir", default="output/")

    return parser.parse_args()


if __name__ == "__main__":

    args = parse_args()

    try:
        os.mkdir(args.output_dir)
    except Exception as e:
        print(e)

    run(ccp4_map_template_path=args.ccp4_map_template_path,
        number_samples=args.number_samples,
        sample_size=(args.x, args.y, args.z),
        output_dir=args.output_dir)
