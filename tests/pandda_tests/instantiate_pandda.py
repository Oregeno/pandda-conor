import pickle

from libtbx import easy_pickle

import glob
import os

from iotbx.file_reader import any_file


def load_map_list():

    return 1


def instantiate_single_test_map(template_analysis_directory):

    map_glob = template_analysis_directory+"*/*/*aligned-map.ccp4"

    print(map_glob)

    map_list = glob.glob(map_glob)

    # print(map_list)

    test_map_file = any_file(map_list[0])

    test_map = test_map_file.file_object

    return test_map


def instantiate_PanddaMapAnalyser(template_analysis_directory):

    PanddaMapAnalyser_path = template_analysis_directory + "pickled_data/map_analyser_1.78A.pickle"

    map_analyser = easy_pickle.load(PanddaMapAnalyser_path)

    return map_analyser

def instantiate_pandda(template_analysis_directory):

    Pandda_path = template_analysis_directory + "pickled_data/pandda.pickle"

    Pandda = easy_pickle.load(Pandda_path)

    return Pandda