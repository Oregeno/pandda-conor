import numpy as np
from hypothesis import given
import hypothesis.strategies as st
from hypothesis.extra.numpy import arrays
from hypothesis import assume
from hypothesis import note
from hypothesis import settings

from scitbx.array_family import flex

from pandda.analyse import classes

from giant.xray import maps
from giant import grid
from giant.grid import masks


import pytest

settings.register_profile("dev", max_examples=200, deadline=None)
settings.load_profile("dev")

# template_analysis_directory = "/dls/labxchem/data/2017/lb18145-3/processing/analysis/2018_11_09_pandda_final/"


def spoof_ElectronDensityMap_list(array_data):

    map_list = []

    map_size = array_data[0].shape

    # spoof a grid
    spoof_grid = grid.Grid(grid_spacing=0.5,
                           origin=flex.double([0.0, 0.0, 0.0]),
                           approx_max=flex.double(tuple([(map_size[0]-1)*0.5, (map_size[1]-1)*0.5, (map_size[2]-1)*0.5])))

    assert spoof_grid is not None

    # Spoof global mask:
    # this needs to be a list of indicies in the one d array of

    # print(type(spoof_grid.cart_origin()[0]))

    spoof_global_mask = masks.AtomicMask(parent=spoof_grid,
                                         sites_cart=flex.vec3_double([[0.0, 0.0, 0.0]]),
                                         max_dist=1.4,
                                         min_dist=0.4)

    spoof_grid.set_global_mask(spoof_global_mask)

    print(spoof_grid.summary())

    # Iterate over num maps
    for i, data in enumerate(array_data):

        # spoof an array
        array = data

        # Convert array to flex
        flex_array = flex.double(array.flatten()[spoof_grid.global_mask().outer_mask_binary()])
        # print("spoof total mask", spoof_grid.global_mask().total_mask_binary().size)
        # print("spoof total mask true", spoof_grid.global_mask().outer_mask_binary()[spoof_grid.global_mask().outer_mask_binary() == True].size)
        # print("flex array size", flex_array.size())

        # Spoof a map
        ref_map = maps.ElectronDensityMap(map_data=flex_array,
                                          unit_cell=spoof_grid.unit_cell(),
                                          map_indices=spoof_grid.global_mask().outer_mask_indices(),
                                          map_size=spoof_grid.grid_size(),
                                          map_origin=spoof_grid.cart_origin(),
                                          sparse=True)

        ref_map.meta.num = i
        ref_map.meta.tag = str(i)
        ref_map.meta.type = 'ElectronDensityMap'
        ref_map.meta.resolution = 1.0
        ref_map.meta.map_uncertainty = None
        # ref_map.meta.obs_map_mean = morphed_map_data.min_max_mean().mean
        # ref_map.meta.obs_map_rms = morphed_map_data.standard_deviation_of_the_sample()
        # ref_map.meta.scl_map_mean = scaled_map_data.min_max_mean().mean
        # ref_map.meta.scl_map_rms = scaled_map_data.standard_deviation_of_the_sample()

        map_list.append(ref_map)

    return map_list


def spoof_PanddaMapAnalyser(array_data):
    assume(len(array_data) != 0)

    dataset_maps = spoof_ElectronDensityMap_list(array_data)  # List of electron density maps

    map_holder = classes.MapHolderList()

    map_holder.add(dataset_maps)

    map_analyser = classes.PanddaMapAnalyser(dataset_maps=map_holder)  # Not including a lot of metadata

    return map_analyser


class TestPanddaMapAnalyser(object):

    @given(st.lists(arrays(np.float, (5, 5, 5), elements=st.floats(0, 1))))
    def test___init__(self, array_list):
        # dataset_maps,
        # meta = None,
        # statistical_maps = None,
        # parent = None,
        # log = None

        map_analyser = spoof_PanddaMapAnalyser(array_list)

        assert map_analyser is not None

    ####################################################################################################################

    #@pytest.mark.skip(reason="In development")
    @given(st.lists(arrays(np.float, (5, 5, 5), elements=st.floats(0, 1))))
    def test_calculate_empirical_distribution(self, map_list):
        # TODO CONOR: load function TODO
        assume(len(map_list) != 0)
        assume(np.count_nonzero(map_list[0]) > 0)

        map_analyser = spoof_PanddaMapAnalyser(map_list)

        e_dist_np = map_analyser.calculate_empirical_distribution()

        note("e_dist type: "), note(type(e_dist_np))
        note("map list length: "), note(len(map_list))
        note("shape of distribution: "), note(e_dist_np.shape)

        assert e_dist_np is not None
        # assert np.count_nonzero(e_dist) != 0
        # assert not np.array_equal(e_dist, np.array(map_list))
        if len(map_list) > 1:
            note(e_dist_np[:, 0, 0, 0])
            assert np.all(np.diff(e_dist_np[:, 0, 0, 0]) >= 0), "Array not increasing along (0,0,0)"
            note(e_dist_np[:, 0, 2, 1])
            assert np.all(np.diff(e_dist_np[:, 0, 2, 1]) >= 0), "Array not increasing along (0,2,1)"

    ####################################################################################################################

    @pytest.mark.skip(reason="In development")
    @given(st.lists(arrays(np.float, (5, 5, 5), elements=st.floats(0, 1))))
    def test__calculate_empirical_distribution(self, map_list):
        # TODO CONOR: load function TODO
        assume(len(map_list) != 0)
        assume(np.count_nonzero(map_list[0]) > 0)

        e_dist = classes.PanddaMapAnalyser._calculate_empirical_distribution(map_list)

        note(len(map_list))
        note(e_dist.shape)
        assert e_dist is not None
        # assert np.count_nonzero(e_dist) != 0
        # assert not np.array_equal(e_dist, np.array(map_list))
        if len(map_list) > 1:
            note(e_dist[:, 0, 0, 0])
            assert np.all(np.diff(e_dist[:, 0, 0, 0]) >= 0), "Array not increasing along (0,0,0)"
            note(e_dist[:, 0, 2, 1])
            assert np.all(np.diff(e_dist[:, 0, 2, 1]) >= 0), "Array not increasing along (0,2,1)"

    ####################################################################################################################

    @pytest.mark.skip(reason="In development")
    @given(st.lists(arrays(np.float, (5, 5, 5), elements=st.floats(0, 1))))
    def test__calculate_mad(self, map_list):
        # TODO CONOR: load function TODO
        assume(len(map_list) >= 2)
        assume(np.count_nonzero(map_list[0]) > 0)

        mad = classes.PanddaMapAnalyser._calculate_mad(map_list)

        note(len(map_list))
        note("mad shape:")
        note(mad.shape)
        assert mad is not None
        assert mad.shape == map_list[0].shape

    ####################################################################################################################

    # @pytest.mark.skip(reason="In development")
    @given(map_list=st.lists(arrays(np.float, (7, 5, 3), elements=st.floats(0, 1))),
           test_map=arrays(np.float, (7, 5, 3), elements=st.floats(0, 1)))
    def test__calculate_q_map(self, map_list, test_map):
        # TODO CONOR: load function TODO
        assume(len(map_list) >= 3)
        assume(np.count_nonzero(map_list[0]) > 0)

        e_dist = classes.PanddaMapAnalyser._calculate_empirical_distribution(map_list)

        map_dimension = e_dist.shape

        note(e_dist.shape)
        note(test_map.shape)

        q_map = classes.PanddaMapAnalyser._calculate_q_map(e_dist, test_map, map_dimension)

        note(q_map)

        assert q_map is not None

        assert np.all(q_map <= 1)
        assert np.all(q_map >= 0)

    ####################################################################################################################

    @given(map_list=st.lists(arrays(np.float, (7, 5, 3), elements=st.floats(0, 1))),
           test_map=arrays(np.float, (7, 5, 3), elements=st.floats(0, 1)))
    def test_calculate_q_map(self, map_list, test_map):
        # TODO CONOR: load function TODO
        assume(len(map_list) >= 7)
        assume(np.count_nonzero(map_list[0]) > 0)

        map_analyser = spoof_PanddaMapAnalyser(map_list)

        map_analyser.calculate_empirical_distribution()

        note("map list shape"), note(map_list[0].shape)
        note("map shape"), note(test_map.shape)

        # Spoof a map
        test_map_ccp4 = spoof_ElectronDensityMap_list([test_map])[0]

        q_map = map_analyser.calculate_q_map(test_map_ccp4)

        q_map_np = np.array(q_map.get_map_data(sparse=False)).reshape(q_map._map_size)

        note(q_map_np)

        assert q_map_np is not None

        assert np.all(q_map_np <= 1)
        assert np.all(q_map_np >= 0)

    ####################################################################################################################

    @pytest.mark.skip(reason="In development")
    @given(map_list=st.lists(arrays(np.float, (5, 5, 5), elements=st.floats(0, 1))),
           test_map=arrays(np.float, (5, 5, 5), elements=st.floats(0, 1)))
    def test__calculate_mad_map(self, map_list, test_map):
        # TODO CONOR: load function TODO
        assume(len(map_list) >= 3)
        assume(np.count_nonzero(map_list[0]) > 0)

        med = np.median(map_list)
        mad = classes.PanddaMapAnalyser._calculate_mad(map_list)

        map_dimension = mad.shape

        note(mad.shape)
        note(med.shape)
        note(test_map.shape)

        mad_map = classes.PanddaMapAnalyser._calculate_mad_map(mad, med, test_map, map_dimension)

        note(mad_map.shape)
        note(mad_map)

        assert mad_map is not None
        assert mad_map.shape == test_map.shape


