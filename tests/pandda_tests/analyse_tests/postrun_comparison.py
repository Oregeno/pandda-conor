import argparse

import numpy as np
import pandas as pd

import seaborn as sns; sns.set()
import matplotlib.pyplot as plt


def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("--truth")
    parser.add_argument("--control")
    parser.add_argument("--test")
    parser.add_argument("--output")

    parser.add_argument("--check_qaulity", default=False)

    args = parser.parse_args()

    return args


def ROC():

    pass


def num_events():

    pass


def num_models():

    pass


# Calculate metrics
def calculate_accuracy(table):

    pd.count(table[""])


def calculate_precission():
    pass


def calculate_recall():
    pass


def calculate_specificity():
    pass


# Dtermine event truth
def true_positive():
    pass

def true_negative():
    pass

def false_positive():
    pass

def false_negative():
    pass


def determine_event_truth(event, true_model_table=None, cutoff=None):
    """
    Check if any models from the true_model_table lie within cutoff
    :param event:
    :param true_model_table:
    :param cutoff:
    :return:
    """

    calculate_distance = lambda row: np.lingalg.norm([row.x - event.x, row.y - event.y, row.z - event.z]
    distances = true_model_table.apply(calculate_distance, axis=0)

    hits = true_model_table[distances < cutoff]

    return hits


# Graph
def line_plot_and_save(dataframe, y, x, z):

    lineplot = sns.lineplot(y=y,
                 x=x,
                 hue=z,
                 data=dataframe)

    fig = lineplot.get_figure()

    fig.savefig('{}_vs_{}_with_{}.png'.format(y, x, z))



if __name__== "__main__":

    # Parse args
    args = parse_args()

    # Load tables
    true_model_table = pd.load_csv(args.truth)
    run_tables = [pd.load_csv(csv_path) for csv_path in args.run_tables]

    # Instantiate statistics table
    statistics_columns = ["run",
                          "metric",
                          "parameters",
                          "score"]

    statistics = pd.DataFrame(columns=statistics_columns)

    # loop over cutoffs
    truth_table_columns = ["run",
                           "event",
                           "annotation"]
    for cutoff in args.cutoffs:

        for run_table in run_tables:

            # Analyse events
            truth_table = pd.DataFrame(truth_table_columns)

            for event in run_table:
                truth_value = determine_event_truth(event, cutoff=cutoff)
                truth_table.append({"run": run_table.name,
                                    "event": event,
                                    "annotation": truth_value})

            # Analyse accuracy
            accuracy = calculate_accuracy(truth_table)
            statistics.append({"run": run_table.name,
                               "metric": "accuracy",
                               "parameters": {"cutoff": cutoff},
                               "score": accuracy})

            # Analyse precision
            precission = calculate_precission(truth_table)
            statistics.append({"run": run_table.name,
                               "metric": "precission",
                               "parameters": {"cutoff": cutoff},
                               "score": precission})

            # Analyse recall
            recall = calculate_recall(truth_table)
            statistics.append({"run": run_table.name,
                               "metric": "recall",
                               "parameters": {"cutoff": cutoff},
                               "score": recall})

            # Analyse specificity
            specificity = calculate_specificity(truth_table)
            statistics.append({"run": run_table.name,
                               "metric": "specificity",
                               "parameters": {"cutoff": cutoff},
                               "score": accuracy})


    # Plot graphs
    # plot accuracy vs cutoff with runs
    line_plot_and_save(statistics,
                       "accuracy",
                       "cutoff",
                       "runs")
    # plot precission vs cutoff with runs
    line_plot_and_save(statistics,
                       "precission",
                       "cutoff",
                       "runs")
    # plot recall vs cutoff with runs
    line_plot_and_save(statistics,
                       "recall",
                       "cutoff",
                       "runs")
    # plot specificity vs cutoff with runs
    line_plot_and_save(statistics,
                       "specificity",
                       "cutoff",
                       "runs")

    # plot_precission_recall: precision vs recall with runs
    for run in run_tables:
        line_plot_and_save(statistics,
                           "precission",
                           "recall",
                           "cutoff")

    # plot_ROC(parameter="cutoff"): true positive vs false positive with runs
    for run in run_tables:
        tpr = true_positive_rate()
        fpr = false_positive_rate()

        line_plot_and_save(statistics,
                           tpr,
                           fpr,
                           "cutoff")

    # Plot tables

    table_ROC_auc()



    # Analyse qaulity: Model RSCC's to nearest event

    if args.check_qaulity is True:

        pass



