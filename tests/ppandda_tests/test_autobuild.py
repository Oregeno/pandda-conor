
from hypothesis import given
import hypothesis.strategies as st
from hypothesis.extra.numpy import arrays
from hypothesis import assume
from hypothesis import note
from hypothesis import settings
import pytest

import numpy as np

from ppandda.autobuild import Autobuild

from .structure import Ligand, Structure
from .volume import Volume


settings.register_profile("dev", max_examples=200, deadline=None)
settings.load_profile("dev")


def spoof_receptor():

    spoof_receptor = Structure.fromfile(ligand_path)

    return spoof_receptor


def spoof_ligand():

    spoof_ligand = Ligand.fromfile(ligand_path)

    return spoof_ligand


def spoof_xmap():

    spoof_volume = Volume.fromfile()

    return spoof_volume


class TestAutobuild:

    @pytest.mark.skip(reason="In development")
    @given(st.lists(arrays(np.float, (5, 5, 5), elements=st.floats(0, 1))))
    def test_Autobuild(self, map_list):
        # TODO CONOR: load function TODO
        assume(len(map_list) != 0)
        assume(np.count_nonzero(map_list[0]) > 0)

        receptor = spoof_receptor()
        ligand = spoof_ligand()
        xmap = spoof_xmap()

        resolution = 2.0

        test_class = Autobuild(resolution,
                               ligand,
                               receptor,
                               xmap=xmap)

        assert test_class is not None


        @pytest.mark.slow
        def test_run_real_data():

            return 1

